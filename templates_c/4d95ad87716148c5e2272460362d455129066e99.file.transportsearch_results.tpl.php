<?php /* Smarty version Smarty-3.1.13, created on 2015-02-01 22:53:58
         compiled from "./templates/transportsearch_results.tpl" */ ?>
<?php /*%%SmartyHeaderCode:157944020254ce5511daa3b8-58315002%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4d95ad87716148c5e2272460362d455129066e99' => 
    array (
      0 => './templates/transportsearch_results.tpl',
      1 => 1422809629,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '157944020254ce5511daa3b8-58315002',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_54ce55121447a9_03661308',
  'variables' => 
  array (
    'transports' => 0,
    'my_id' => 0,
    'id' => 0,
    'transport' => 0,
    'origin' => 0,
    'destination' => 0,
    'phone' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54ce55121447a9_03661308')) {function content_54ce55121447a9_03661308($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/v-8075/data/www/gruzy.kz/libs/plugins/modifier.date_format.php';
?><?php if (isset($_smarty_tpl->tpl_vars['transports']->value)){?>
			<table id="cargo-table">
				<tr>
											<th>
												<strong>Даты погрузки и прибытия<br/>
													Откуда<img src="templates/images/arrow-right.png" style="width:10px; height:10px">Куда
												</strong>
											</th>
											<th><strong>Стоимость</strong></th>
											<th><strong>Тип транспорта<br/>Кол-во транспорта</strong></th>
											<th><strong>Грузоподъемность</strong></th>
											<th><strong>Объем и габариты</strong></th>
											<th><strong>Управление</strong></th>
										</tr>
				<?php  $_smarty_tpl->tpl_vars["transport"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["transport"]->_loop = false;
 $_smarty_tpl->tpl_vars["id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['transports']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["transport"]->key => $_smarty_tpl->tpl_vars["transport"]->value){
$_smarty_tpl->tpl_vars["transport"]->_loop = true;
 $_smarty_tpl->tpl_vars["id"]->value = $_smarty_tpl->tpl_vars["transport"]->key;
?>
					<tr class="cargo-container">
						<!--<?php if (isset($_smarty_tpl->tpl_vars['my_id']->value)){?>
						<td class="favorite"><img class="add-to-favorites" cargoid="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" src="templates/images/favorite_no.png" alt=""/></td>
						<?php }?>-->
						<td class="delivery-dates" style="width:250px;">
							<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['transport']->value['date_from'],"%d.%m.%G");?>
 &mdash; <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['transport']->value['date_due'],"%d.%m.%G");?>

							<div class="origin-destination">
								<?php  $_smarty_tpl->tpl_vars["origin"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["origin"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['transport']->value['origins']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["origin"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["origin"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["origin"]->key => $_smarty_tpl->tpl_vars["origin"]->value){
$_smarty_tpl->tpl_vars["origin"]->_loop = true;
 $_smarty_tpl->tpl_vars["origin"]->iteration++;
 $_smarty_tpl->tpl_vars["origin"]->last = $_smarty_tpl->tpl_vars["origin"]->iteration === $_smarty_tpl->tpl_vars["origin"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["originsloop"]['last'] = $_smarty_tpl->tpl_vars["origin"]->last;
?>
									<b><?php echo $_smarty_tpl->tpl_vars['origin']->value;?>
</b><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['originsloop']['last']){?><?php }else{ ?>, <?php }?>
								<?php } ?>
								<img src="templates/images/arrow-right.png" style="width:10px; height:10px">
								<?php  $_smarty_tpl->tpl_vars["destination"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["destination"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['transport']->value['destinations']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["destination"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["destination"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["destination"]->key => $_smarty_tpl->tpl_vars["destination"]->value){
$_smarty_tpl->tpl_vars["destination"]->_loop = true;
 $_smarty_tpl->tpl_vars["destination"]->iteration++;
 $_smarty_tpl->tpl_vars["destination"]->last = $_smarty_tpl->tpl_vars["destination"]->iteration === $_smarty_tpl->tpl_vars["destination"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["destinationsloop"]['last'] = $_smarty_tpl->tpl_vars["destination"]->last;
?>
									<b><?php echo $_smarty_tpl->tpl_vars['destination']->value;?>
</b><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['destinationsloop']['last']){?><?php }else{ ?>, <?php }?>
								<?php } ?>
							</div>
							<!--<?php if (isset($_smarty_tpl->tpl_vars['my_id']->value)&&$_smarty_tpl->tpl_vars['my_id']->value==$_smarty_tpl->tpl_vars['transport']->value['account_id']){?><div><a href="index.php?page=mycargo&action=delete&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="delete">Удалить</a></div><?php }?>-->
						</td>
						<td>
							<!--div class="cargo-name"><?php echo $_smarty_tpl->tpl_vars['transport']->value['cargo_name'];?>
</div-->
							<?php if (isset($_smarty_tpl->tpl_vars['transport']->value['price'])){?><div class="cargo-price">Цена:<?php echo $_smarty_tpl->tpl_vars['transport']->value['price'];?>
 тенге</div><?php }?>
						</td>
						<td style="text-align:center">
							<!--div><?php echo $_smarty_tpl->tpl_vars['transport']->value['transport_type'];?>
</div-->
							<table style="text-align:left;">
								<!--tr>
									<td><b>Марка: </b></td>
									<td>
										<?php if (strlen($_smarty_tpl->tpl_vars['transport']->value['brand'])==0){?>
										0<br/>
										<?php }else{ ?>
										<?php echo $_smarty_tpl->tpl_vars['transport']->value['brand'];?>
<br/>
										<?php }?>
									</td>
								</tr>
								<tr>
									<td><b>Вид транспорта: </b></td>
									
									<td>
									<?php if (strlen($_smarty_tpl->tpl_vars['transport']->value['transports_mode'])==0){?>
									0<br/>
									<?php }else{ ?>
									<?php echo $_smarty_tpl->tpl_vars['transport']->value['transports_modes'];?>
<br/>
									<?php }?>
									</td>
								</tr>
								<tr>
									<td><b>Тип транспорта: </b></td>
									<td>
									<?php if (strlen($_smarty_tpl->tpl_vars['transport']->value['transports_type'])==0){?>
									0<br/>
									<?php }else{ ?>
									<?php echo $_smarty_tpl->tpl_vars['transport']->value['transports_type'];?>
<br/>
									<?php }?>
									</td>
									
								</tr>
								<tr>
									<td><b>Вместимость, машин: </b></td>
									<td>
									<?php if (strlen($_smarty_tpl->tpl_vars['transport']->value['capacity'])==0){?>
									0<br/>
									<?php }else{ ?>
									<?php echo $_smarty_tpl->tpl_vars['transport']->value['capacity'];?>
<br/>
									<?php }?>
									</td>
									
								</tr>
								<tr>
									<td><b>Посадочных мест, шт.: </b></td>
									<td>
									<?php if (strlen($_smarty_tpl->tpl_vars['transport']->value['seats'])==0){?>
									0<br/>
									<?php }else{ ?>
									<?php echo $_smarty_tpl->tpl_vars['transport']->value['seats'];?>
<br/>
									<?php }?>
									</td>
								</tr-->
								<tr>
									<td style='width:350px;'><b>Вид транспорта: </b></td>
									
									<td style='width:350px;'>
									<?php if (strlen($_smarty_tpl->tpl_vars['transport']->value['transports_mode'])==0){?>
										Не указано<br/>
									<?php }else{ ?>
										<?php echo $_smarty_tpl->tpl_vars['transport']->value['transports_mode'];?>
<br/>
									<?php }?>
									</td>
								</tr>
								<tr>
									<td style='width:350px;'><b>Тип транспорта: </b></td>
									<td style='width:350px;'>
									<?php if (strlen($_smarty_tpl->tpl_vars['transport']->value['transports_type'])==0){?>
										Не указано<br/>
									<?php }else{ ?>
										<?php echo $_smarty_tpl->tpl_vars['transport']->value['transports_type'];?>
<br/>
									<?php }?>
									</td>
									
								</tr>
								<tr>
									<td style='width:350px;'><b>Доп.инфо: </b></td>
									<td style='width:350px;'>
									<?php if (strlen($_smarty_tpl->tpl_vars['transport']->value['information'])==0){?>
										Не указано<br/>
									<?php }else{ ?>
										<?php echo $_smarty_tpl->tpl_vars['transport']->value['information'];?>
<br/>
									<?php }?>
									</td>
									
								</tr>
								<tr>
									<td style='width:350px;'><b>Единиц транспорта в наличии:</b></td>
									<td style='width:350px;'>
									<?php if (strlen($_smarty_tpl->tpl_vars['transport']->value['car_amount'])==0){?>
										Не указано<br/>
									<?php }else{ ?>
										<?php echo $_smarty_tpl->tpl_vars['transport']->value['car_amount'];?>
<br/>
									<?php }?>
									</td>
								<tr>
									<td style='width:350px;'><b>Перевозка негабаритного груза:<b></td>
									<td style='width:350px;'>
									<?php if ($_smarty_tpl->tpl_vars['transport']->value['notgabarite']=="0"){?>Нет<?php }else{ ?>Да<?php }?>
									</td>
								</tr>
							</table>
						</td>
						<td style="text-align:center">
							<?php if ($_smarty_tpl->tpl_vars['transport']->value['weight']>0){?><?php echo $_smarty_tpl->tpl_vars['transport']->value['weight'];?>
т.<?php }else{ ?><div class="note">тоннаж<br/>неизвестен</div><?php }?>
						</td>
						<td style="text-align:center">
							<?php if ($_smarty_tpl->tpl_vars['transport']->value['volume']>0){?><?php echo $_smarty_tpl->tpl_vars['transport']->value['volume'];?>
м<sup>3</sup>.<?php }else{ ?><div class="note">обьем<br/>не задан</div><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['transport']->value['length']>0&&$_smarty_tpl->tpl_vars['transport']->value['width']>0&&$_smarty_tpl->tpl_vars['transport']->value['height']>0){?><?php echo $_smarty_tpl->tpl_vars['transport']->value['length'];?>
м x <?php echo $_smarty_tpl->tpl_vars['transport']->value['width'];?>
м x <?php echo $_smarty_tpl->tpl_vars['transport']->value['height'];?>
м м<?php }?>
						</td>
						<td style="width:180px;">
							<div style="text-align:right;">
									<div id="cargo-contacts-<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" style="text-align:right;">
                                                                  <?php if ((strlen($_smarty_tpl->tpl_vars['transport']->value['contact_email'])==0)){?><?php $_smarty_tpl->createLocalArrayVariable('transport', null, 0);
$_smarty_tpl->tpl_vars['transport']->value['contact_email'] = "не указан";?><?php }?>
                                                                  <a href="#" class="tooltip yellow-tooltip edit" data-html="true" data-title="Имя: <?php echo $_smarty_tpl->tpl_vars['transport']->value['contact_name'];?>

                                                                  E-mail: <?php echo $_smarty_tpl->tpl_vars['transport']->value['contact_email'];?>
; 
                                                                  <?php  $_smarty_tpl->tpl_vars["phone"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["phone"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['transport']->value['contact_phones']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["phone"]->key => $_smarty_tpl->tpl_vars["phone"]->value){
$_smarty_tpl->tpl_vars["phone"]->_loop = true;
?>
                                                                  Телефон: <?php echo $_smarty_tpl->tpl_vars['phone']->value;?>

                                                                  <?php } ?>
                                                                  Нажмите для подробной информации"><img src="templates/images/phone.png" style="width:24px; height:24px;"></a>
                                                                  
                                                                  <a href="index.php?page=contacts&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="tooltip yellow-tooltip edit" data-html="true" data-title="Подробная информация
                                                                  Контактная информация
                                                                  Маршрут и расстояние
                                                                  Нажмите для подробной информации"><img src="templates/images/info.png" style="width:24px; height:24px;"></a>

									<?php if (isset($_smarty_tpl->tpl_vars['my_id']->value)&&$_smarty_tpl->tpl_vars['my_id']->value==$_smarty_tpl->tpl_vars['transport']->value['account_id']){?>
										<a href="index.php?page=mycargo&action=delete&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="tooltip yellow-tooltip delete" data-html="true" data-title="Удалить данный транспорт"><img src="templates/images/bin.png" style="width:24px; height:24px"></a>
										<a href="index.php?page=edittransport&action=edit&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="tooltip yellow-tooltip edit" data-html="true" data-title="Изменить данный транспорт"><img src="templates/images/pencil.png" style="width:24px; height:24px"></a>
									<?php }?>
									</div>
						</td>
					</tr>
				<?php } ?>
			</table>
		<?php }else{ ?>
			<div style="text-align:center">К сожалению, на данном направлении свободный транспорт отсутствует. Попробуйте, пожалуйста, повторить поиск немного позднее.</div>
		<?php }?>
<?php }} ?>