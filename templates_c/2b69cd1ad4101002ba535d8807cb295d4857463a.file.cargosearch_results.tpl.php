<?php /* Smarty version Smarty-3.1.13, created on 2015-02-01 22:29:48
         compiled from "./templates/cargosearch_results.tpl" */ ?>
<?php /*%%SmartyHeaderCode:89152473554ce547c45b444-13675222%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2b69cd1ad4101002ba535d8807cb295d4857463a' => 
    array (
      0 => './templates/cargosearch_results.tpl',
      1 => 1422808101,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '89152473554ce547c45b444-13675222',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cargos' => 0,
    'cargo' => 0,
    'origin' => 0,
    'destination' => 0,
    'id' => 0,
    'my_id' => 0,
    'phone' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_54ce547c56d2a2_73496016',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54ce547c56d2a2_73496016')) {function content_54ce547c56d2a2_73496016($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/v-8075/data/www/gruzy.kz/libs/plugins/modifier.date_format.php';
?><?php if (isset($_smarty_tpl->tpl_vars['cargos']->value)){?>
	<table id="cargo-table">
					<tr>
											<th>
												<strong>Даты погрузки и прибытия<br/>
													Откуда<img src="templates/images/arrow-right.png" style="width:10px; height:10px">Куда
												</strong>
											</th>
											<th><strong>Наименование<br/>Стоимость</strong></th>
											<th><strong>Тип транспорта<br/>кол-во транспорта</strong></th>
											<th><strong>Грузоподъемность</strong></th>
											<th><strong>Объем и габариты</strong></th>
											<th><strong>Управление</strong></th>
										</tr>
		<?php  $_smarty_tpl->tpl_vars["cargo"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["cargo"]->_loop = false;
 $_smarty_tpl->tpl_vars["id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cargos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["cargo"]->key => $_smarty_tpl->tpl_vars["cargo"]->value){
$_smarty_tpl->tpl_vars["cargo"]->_loop = true;
 $_smarty_tpl->tpl_vars["id"]->value = $_smarty_tpl->tpl_vars["cargo"]->key;
?>
			<!--tr>
				<td colspan="6" class="origin-destination">
					<?php  $_smarty_tpl->tpl_vars["origin"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["origin"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cargo']->value['origins']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["origin"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["origin"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["origin"]->key => $_smarty_tpl->tpl_vars["origin"]->value){
$_smarty_tpl->tpl_vars["origin"]->_loop = true;
 $_smarty_tpl->tpl_vars["origin"]->iteration++;
 $_smarty_tpl->tpl_vars["origin"]->last = $_smarty_tpl->tpl_vars["origin"]->iteration === $_smarty_tpl->tpl_vars["origin"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["originsloop"]['last'] = $_smarty_tpl->tpl_vars["origin"]->last;
?>
						<b><?php echo $_smarty_tpl->tpl_vars['origin']->value;?>
</b><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['originsloop']['last']){?><?php }else{ ?>, <?php }?>
					<?php } ?>
					&rarr;
					<?php  $_smarty_tpl->tpl_vars["destination"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["destination"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cargo']->value['destinations']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["destination"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["destination"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["destination"]->key => $_smarty_tpl->tpl_vars["destination"]->value){
$_smarty_tpl->tpl_vars["destination"]->_loop = true;
 $_smarty_tpl->tpl_vars["destination"]->iteration++;
 $_smarty_tpl->tpl_vars["destination"]->last = $_smarty_tpl->tpl_vars["destination"]->iteration === $_smarty_tpl->tpl_vars["destination"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["destinationsloop"]['last'] = $_smarty_tpl->tpl_vars["destination"]->last;
?>
						<b><?php echo $_smarty_tpl->tpl_vars['destination']->value;?>
</b><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['destinationsloop']['last']){?><?php }else{ ?>, <?php }?>
					<?php } ?>
					<div style="float:right"><a href="javascript:void(0)" class="display-contacts-link" cargoid="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">Отобразить контакты</a></div>
				</td>
			</tr-->
			<tr class="cargo-container">
				<!--td class="favorite"><?php if (isset($_smarty_tpl->tpl_vars['my_id']->value)){?><img class="add-to-favorites" cargoid="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" src="templates/img/favorite_no.png" alt=""/><?php }?></td-->
				<td class="delivery-dates">
					<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['cargo']->value['load_from'],"%d.%m.%G");?>
 &mdash; <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['cargo']->value['load_due'],"%d.%m.%G");?>

							<div class="origin-destination">
								<?php  $_smarty_tpl->tpl_vars["origin"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["origin"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cargo']->value['origins']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["origin"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["origin"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["origin"]->key => $_smarty_tpl->tpl_vars["origin"]->value){
$_smarty_tpl->tpl_vars["origin"]->_loop = true;
 $_smarty_tpl->tpl_vars["origin"]->iteration++;
 $_smarty_tpl->tpl_vars["origin"]->last = $_smarty_tpl->tpl_vars["origin"]->iteration === $_smarty_tpl->tpl_vars["origin"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["originsloop"]['last'] = $_smarty_tpl->tpl_vars["origin"]->last;
?>
									<b><?php echo $_smarty_tpl->tpl_vars['origin']->value;?>
</b><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['originsloop']['last']){?><?php }else{ ?>, <?php }?>
								<?php } ?>
								<img src="templates/images/arrow-right.png" style="width:10px; height:10px">
								<?php  $_smarty_tpl->tpl_vars["destination"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["destination"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cargo']->value['destinations']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["destination"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["destination"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["destination"]->key => $_smarty_tpl->tpl_vars["destination"]->value){
$_smarty_tpl->tpl_vars["destination"]->_loop = true;
 $_smarty_tpl->tpl_vars["destination"]->iteration++;
 $_smarty_tpl->tpl_vars["destination"]->last = $_smarty_tpl->tpl_vars["destination"]->iteration === $_smarty_tpl->tpl_vars["destination"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["destinationsloop"]['last'] = $_smarty_tpl->tpl_vars["destination"]->last;
?>
									<b><?php echo $_smarty_tpl->tpl_vars['destination']->value;?>
</b><?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['destinationsloop']['last']){?><?php }else{ ?>, <?php }?>
								<?php } ?>
							</div>
				</td>
				<td>
					<div class=""><strong>Наименование:</strong> <?php echo $_smarty_tpl->tpl_vars['cargo']->value['cargo_name'];?>
</div>
									<div class="cargo-name"><strong>Доп.инфо:</strong> <?php echo $_smarty_tpl->tpl_vars['cargo']->value['info'];?>
</div>
									<div class="cargo-name"><strong>Возможность консолидации:</strong> <?php if ($_smarty_tpl->tpl_vars['cargo']->value['canconsolidate']=="on"){?>Да<?php }else{ ?>Нет<?php }?></div>
									<?php if (isset($_smarty_tpl->tpl_vars['cargo']->value['price'])){?><div class="cargo-price"><?php echo $_smarty_tpl->tpl_vars['cargo']->value['price'];?>
 тенге</div><?php }?>
				</td>
				<td style="text-align:center">
					<div class="car-amount"><strong>Вид транспорта:</strong> <?php if (strlen($_smarty_tpl->tpl_vars['cargo']->value['transport_modes'])==0){?> Не указано<?php }else{ ?> <?php echo $_smarty_tpl->tpl_vars['cargo']->value['transport_modes'];?>
 <?php }?> </div>
							<div class="car-amount"><strong>Тип транспорта:</strong> <?php echo $_smarty_tpl->tpl_vars['cargo']->value['transport_type'];?>
 </div>
							<div class="car-amount"><strong>Требуется единиц транспорта:</strong><?php if (strlen($_smarty_tpl->tpl_vars['cargo']->value['vehicles_number'])==0){?>  Не указано<?php }else{ ?> <?php echo $_smarty_tpl->tpl_vars['cargo']->value['vehicles_number'];?>
 <?php }?> </div>
				</td>
				<td style="text-align:center">
					<?php if ($_smarty_tpl->tpl_vars['cargo']->value['weight']>0){?><?php echo $_smarty_tpl->tpl_vars['cargo']->value['weight'];?>
т.<?php }else{ ?><div class="note">тоннаж<br/>неизвестен</div><?php }?>
				</td>
				<td style="text-align:center">
					<?php if ($_smarty_tpl->tpl_vars['cargo']->value['volume']>0){?><?php echo $_smarty_tpl->tpl_vars['cargo']->value['volume'];?>
м<sup>3</sup>.<?php }else{ ?><div class="note">обьем<br/>не задан</div><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['cargo']->value['length']>0&&$_smarty_tpl->tpl_vars['cargo']->value['width']>0&&$_smarty_tpl->tpl_vars['cargo']->value['height']>0){?><?php echo $_smarty_tpl->tpl_vars['cargo']->value['length'];?>
м x <?php echo $_smarty_tpl->tpl_vars['cargo']->value['width'];?>
м x <?php echo $_smarty_tpl->tpl_vars['cargo']->value['height'];?>
м м<?php }?>
				</td>
				<td style="width:180px;">
							<div style="text-align:right;">
									<div id="cargo-contacts-<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" style="text-align:right;">
                                                                  <?php if ((strlen($_smarty_tpl->tpl_vars['cargo']->value['contact_email'])==0)){?><?php $_smarty_tpl->createLocalArrayVariable('cargo', null, 0);
$_smarty_tpl->tpl_vars['cargo']->value['contact_email'] = "не указан";?><?php }?>
                                                                  <a href="#" class="tooltip yellow-tooltip edit" data-html="true" data-title="Имя: <?php echo $_smarty_tpl->tpl_vars['cargo']->value['contact_name'];?>

                                                                  E-mail: <?php echo $_smarty_tpl->tpl_vars['cargo']->value['contact_email'];?>
; 
                                                                  <?php  $_smarty_tpl->tpl_vars["phone"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["phone"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cargo']->value['contact_phones']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["phone"]->key => $_smarty_tpl->tpl_vars["phone"]->value){
$_smarty_tpl->tpl_vars["phone"]->_loop = true;
?>
                                                                  Телефон: <?php echo $_smarty_tpl->tpl_vars['phone']->value;?>

                                                                  <?php } ?>
                                                                  Нажмите для подробной информации"><img src="templates/images/phone.png" style="width:24px; height:24px;"></a>
                                                                  
                                                                  <a href="index.php?page=contacts&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="tooltip yellow-tooltip edit" data-html="true" data-title="Подробная информация
                                                                  Контактная информация
                                                                  Маршрут и расстояние
                                                                  Нажмите для подробной информации"><img src="templates/images/info.png" style="width:24px; height:24px;"></a>

									<?php if (isset($_smarty_tpl->tpl_vars['my_id']->value)&&$_smarty_tpl->tpl_vars['my_id']->value==$_smarty_tpl->tpl_vars['cargo']->value['account_id']){?>
										<a href="index.php?page=mycargo&action=delete&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="tooltip yellow-tooltip delete" data-html="true" data-title="Удалить данный груз"><img src="templates/images/bin.png" style="width:24px; height:24px"></a>
										<a href="index.php?page=editfreight&action=edit&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="tooltip yellow-tooltip edit" data-html="true" data-title="Изменить данный груз"><img src="templates/images/pencil.png" style="width:24px; height:24px"></a>
									<?php }?>
									</div>
						</td>
			</tr>
		<?php } ?>
	</table>
<?php }else{ ?>
	<div style="text-align:center">По указанным вами критериям, ничего не найдено.</div>
<?php }?>
<?php }} ?>