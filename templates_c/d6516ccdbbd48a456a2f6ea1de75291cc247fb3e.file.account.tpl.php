<?php /* Smarty version Smarty-3.1.13, created on 2015-02-02 01:41:33
         compiled from "./templates/account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13390086554ce816d9493a8-91877057%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd6516ccdbbd48a456a2f6ea1de75291cc247fb3e' => 
    array (
      0 => './templates/account.tpl',
      1 => 1422107352,
      2 => 'file',
    ),
    'c0360d049dff10f364dfc53ba2cc3958abf6ee6d' => 
    array (
      0 => './templates/index.tpl',
      1 => 1422808325,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13390086554ce816d9493a8-91877057',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'prices_chart' => 0,
    'load_date' => 0,
    'chart' => 0,
    'my_id' => 0,
    'error' => 0,
    'authorized' => 0,
    'my_company_name' => 0,
    'cargos' => 0,
    'cargo' => 0,
    'origin' => 0,
    'destination' => 0,
    'id' => 0,
    'phone' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_54ce816dc04731_97302880',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54ce816dc04731_97302880')) {function content_54ce816dc04731_97302880($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/v-8075/data/www/gruzy.kz/libs/plugins/modifier.date_format.php';
if (!is_callable('smarty_function_html_options')) include '/var/www/v-8075/data/www/gruzy.kz/libs/plugins/function.html_options.php';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="keywords" content="найти перевозчика, найти груз, найти транспорт,  автоперевозки, avtoperevozki, доставка грузов, перевозки грузов, перевезти груз, рузоперевозки, попутный груз, попутный транспорт, грузовик, найти фуру, грузы в Казахстан, грузы из Казахстана, грузы по Казахстану, грузы в Россию, грузы в Москву, догруз, рефрижератор, тент, контейнеровоз, шаланда, дальнобойщик" />
		<meta name="description" content="" />
		<meta name="author" content="Mikhailenko Alexandr (AlexMAK)">
		<meta property="og:image" content="http://localhost/avtoperevozki.kz/templates/images/logo.png" />
		<meta property="og:title" content="AVTOPEREVOZKI.KZ - Логистика и транспорт для требовательных клиентов. У вас есть груз? У нас есть транспорт!!!" />
		<meta property="og:description" content="автоперевозки по Казахстану, международные автоперевозки, офисные переезды,офисные квартирные переезды," />
		<title>AVTOPEREVOZKI.KZ</title>
		<link rel="stylesheet" href="templates/css/style.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="templates/js/jquery-ui-1.10.2/themes/base/jquery-ui.css" />
		<script type="text/javascript" src="templates/js/jquery-1.9.1.min.js"></script>
		<script src="templates/js/jquery-ui-1.10.2/ui/jquery-ui.js"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
                <!-- MAPS -->
                <script src="http://api-maps.yandex.ru/2.1-stable/?load=package.full&lang=ru-RU"></script>
                <script type="text/javascript" src="templates/marsh.js"></script>
		<script type="text/javascript">
			
				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback(drawChart);
				function drawChart() {
				var data = google.visualization.arrayToDataTable([
				  ['Месяц/год', 'Запрашиваемые цены', 'Предлагаемые цены'],
			
				  /*
				  ['2004',  1000,      400],
				  ['2005',  1170,      460],
				  ['2006',  660,       1120],
				  ['2007',  1030,      540]
				  */
				  <?php  $_smarty_tpl->tpl_vars["chart"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["chart"]->_loop = false;
 $_smarty_tpl->tpl_vars["load_date"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['prices_chart']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["chart"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["chart"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["chart"]->key => $_smarty_tpl->tpl_vars["chart"]->value){
$_smarty_tpl->tpl_vars["chart"]->_loop = true;
 $_smarty_tpl->tpl_vars["load_date"]->value = $_smarty_tpl->tpl_vars["chart"]->key;
 $_smarty_tpl->tpl_vars["chart"]->iteration++;
 $_smarty_tpl->tpl_vars["chart"]->last = $_smarty_tpl->tpl_vars["chart"]->iteration === $_smarty_tpl->tpl_vars["chart"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["chartloop"]['last'] = $_smarty_tpl->tpl_vars["chart"]->last;
?>
					['<?php echo $_smarty_tpl->tpl_vars['load_date']->value;?>
',<?php echo $_smarty_tpl->tpl_vars['chart']->value['cargo'];?>
,<?php echo $_smarty_tpl->tpl_vars['chart']->value['transport'];?>
]<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['chartloop']['last']){?><?php }else{ ?>, <?php }?>
				  <?php } ?>
			
				]);

				var options = {
				  title: 'Предлагаемые и запрашиваемые цены за перевозку грузов'
				};

				var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
				chart.draw(data, options);
				}
			
		</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/js/select2.min.js"></script>



		<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
<script>
   $(document).ready(function() {
      $(".js-example-basic-single").select2();
   });
</script>
		<script type="text/javascript">
		$(document).ready(function(){
			$("select[name='from_region']").change(function(){
				//alert("here");
				$("select[name='from_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[name='from_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='from_city']").attr("disabled",false);
			});
			$("select[name='from_country']").change(function(){
				//alert("here");
				$("select[name='from_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getregionoptions.php?id='+countryid, function(data) {
					$("select[name='from_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('getcityoptions.php?id='+countryid, function(data) {
					$("select[name='from_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='from_city']").attr("disabled",false);
			});
			
			$("select[name='to_region']").change(function(){
				//alert("here");
				$("select[name='to_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[name='to_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='to_city']").attr("disabled",false);
			});
			$("select[name='to_country']").change(function(){
				//alert("here");
				$("select[name='to_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getregionoptions.php?id='+countryid, function(data) {
					$("select[name='to_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('getcityoptions.php?id='+countryid, function(data) {
					$("select[name='to_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='to_city']").attr("disabled",false);
			});
			
				$( "input[name='date_from']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
				$( "input[name='date_to']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
			
			$("#cargo-search-filter").submit(function(){
				url="index.php?"+$(this).serialize()+"&ajax";
				//alert(url);
				$("input[type='submit']").val("Ищем, подождите...");
				$.get(url,function(data){
					$("#results").html(data);
					$(".display-contacts-link").click(function(){ $("#cargo-contacts-"+$(this).attr("cargoid")).toggle(); });
					$(".add-to-favorites").click(function(){
						//do ajax action to add to favorites
						$(this).attr("src","templates/images/favorite_yes.png");
						item_id=$(this).attr("cargoid");
						$.get("mods/addtofavorites.php?myid=<?php echo $_smarty_tpl->tpl_vars['my_id']->value;?>
&itemid="+item_id+"&type=cargo",function(data){
							// do nothing
						});
					});
				});
				$("input[type='submit']").val("Еще");
				return false;
			});
		});
	</script>
		<script type="text/javascript">
		$(document).ready(function(){
			var temp_obj=null;
			function ajaxGetCities()
			{
				temp_obj=$(this).next();
				temp_obj.attr("disabled",true);
				countryid=$(this).val();
				$.get('getcityoptions.php?id='+countryid, function(data) {
					temp_obj.html(data);
					$("#delete").val(data);
				});
				temp_obj.attr("disabled",false);
			}
			$("#more-origin-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-from").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			$("#more-destination-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-to").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			
			$(".country-select").change(ajaxGetCities);
			function recalculateVolume()
			{
				l=$("input[name='length']").val();
				w=$("input[name='width']").val();
				h=$("input[name='height']").val();
				if(l.length==0 || w.length==0 || h.length==0) return;
				ll=parseFloat(l);
				ww=parseFloat(w);
				hh=parseFloat(h);
				$("input[name='volume']").val(ll*ww*hh);
			}
			$("input[name='length']").blur(recalculateVolume);
			$("input[name='width']").blur(recalculateVolume);
			$("input[name='height']").blur(recalculateVolume);
			
				$( "input[name='date_from']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
				$( "input[name='date_due']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
			
			$("input[name='date_due']").change(function(){
				var today = new Date();
				var plus2months = new Date(new Date(today).setMonth(today.getMonth()+3));
				var dtarr=$(this).val().split(".");
				var date2compare=new Date(dtarr[2],dtarr[1],dtarr[0]);
				if(date2compare>plus2months)
				{
					alert("Дата не должна превышать 2-х месяцев");
					$(this).val("");  
				}
			});
		});
	</script>
	<style>
		/* mark with differing style */
		.mark {
		    font-weight: 12px;
		}
		/* nav */
		.nav {
		    list-style: none;
		}
		.nav li {
		    float: left;
		    position: relative;
		}
		.navv li {
		    float: left;
		    position: relative;
		}
		/* круглые края */
		.nav > li:first-of-type {
		    border-radius: 5px 0 0 5px;
		}
		.nav > li:last-of-type {
		    border-radius: 0 5px 5px 0;
		}
		/* блоки ссылок */
		.nav li a {
		    display: block;
		    text-decoration: none;
		    color: #fff;
		    padding: 12px;
		    border-radius: 5px;
		}
		/* выпадающее меню */
		.sub-nav {
			top:23px;
			width:205px;
		    position: absolute;
		    border-top: 15px solid transparent;
		    left: -9999px;
		    list-style:none;
		    padding:10;
		    background-color: #666;
		    margin-left:0px;
		    border-radius: 10px;
		}
		/* make submenu reappear */
		.nav li:hover .sub-nav {
		    left: 0;
		}
		/* triangle */
		.triangle {
			top:25px;
		    width: 0;
		    height: 0;
		    border-left: 10px solid transparent;
		    border-right: 10px solid transparent;
		    border-bottom: 10px solid #34495e;
		    position: absolute;
		    margin-top: 5px;
		    left: -9999px;
		}
		/* reappear */
		.nav li:hover .triangle {
		    left: 15px;
		}
		/* prevent multi line links */
		.sub-nav li {
		    white-space: nowrap;
		}
		/* закругленные края */
		.sub-nav > li:first-of-type {
		    border-radius: 5px 5px 0 0;
		}
		.sub-nav > li:last-of-type {
		    border-radius: 0 0 5px 5px;
		}
		/* верхний уровень при наведении */
		.nav > li:hover > a {
		    background: #2C3E50;
		    transition: background ease .5s;
		}
		/* ссылки подменю */
		.sub-nav a {
		    font-size: 90%;
		    margin: 3px;
		    transition: background ease .3s;
		}
		.sub-nav a:hover {
			width:175px;
		    background: #E74C3C;
		}
	</style>
	<script type="text/javascript">
		$(document).ready(function(){
			$("select[name='from_region']").change(function($){
				//alert("here");
				$("select[name='origin']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[id='origin']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[id='origin']").attr("disabled",false);
			});
			$("select[name='from_country']").change(function(){
				//alert("here");
				$("select[id='origin']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getregionoptions.php?id='+countryid, function(data) {
					$("select[name='from_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('getcityoptions.php?id='+countryid, function(data) {
					$("select[id='origin']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[id='origin']").attr("disabled",false);
			});
			
			$("select[name='to_region']").change(function(){
				//alert("here");
				$("select[name='destination']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[name='destination']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='destination']").attr("disabled",false);
			});
			$("select[name='to_country']").change(function(){
				//alert("here");
				$("select[name='destination']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getregionoptions.php?id='+countryid, function(data) {
					$("select[name='to_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('getcityoptions.php?id='+countryid, function(data) {
					$("select[name='destination']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='destination']").attr("disabled",false);
			});
	</script>
	<style>
	#map {
	        width: 100%;
	        height: 480px;
	      }
	#route-length {
		margin-top: 1em;
		right:0;
	}
.navv ul {
  margin: 0;
  padding: 0;
  list-style: none;
  position: relative;
  float: right;
  background: #eee;
  border-bottom: 1px solid #fff;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;    
}

.navv li {
  float: left;          
}

.navv #login {
  border-right: 1px solid #ddd;
  -moz-box-shadow: 1px 0 0 #fff;
  -webkit-box-shadow: 1px 0 0 #fff;
  box-shadow: 1px 0 0 #fff;  
}

.navv #login-trigger,
.navv #signup a {
  display: inline-block;
  *display: inline;
  *zoom: 1;
  height: 25px;
  line-height: 25px;
  font-weight: bold;
  padding: 0 8px;
  text-decoration: none;
  color: #444;
  text-shadow: 0 1px 0 #fff; 
}

.navv #signup a {
  -moz-border-radius: 0 3px 3px 0;
  -webkit-border-radius: 0 3px 3px 0;
  border-radius: 0 3px 3px 0;
}

.navv #login-trigger {
  -moz-border-radius: 3px 0 0 3px;
  -webkit-border-radius: 3px 0 0 3px;
  border-radius: 3px 0 0 3px;
}

.navv #login-trigger:hover,
.navv #login .active,
.navv #signup a:hover {
  background: #fff;
}

.navv #login-content {
  display: none;
  position: absolute;
  top: 24px;
  right: 0;
  z-index: 999;    
  background: #fff;
  background-image: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee));
  background-image: -webkit-linear-gradient(top, #fff, #eee);
  background-image: -moz-linear-gradient(top, #fff, #eee);
  background-image: -ms-linear-gradient(top, #fff, #eee);
  background-image: -o-linear-gradient(top, #fff, #eee);
  background-image: linear-gradient(top, #fff, #eee);  
  padding: 15px;
  -moz-box-shadow: 0 2px 2px -1px rgba(0,0,0,.9);
  -webkit-box-shadow: 0 2px 2px -1px rgba(0,0,0,.9);
  box-shadow: 0 2px 2px -1px rgba(0,0,0,.9);
  -moz-border-radius: 3px 0 3px 3px;
  -webkit-border-radius: 3px 0 3px 3px;
  border-radius: 3px 0 3px 3px;
}

.navv li #login-content {
  right: 0;
  width: 250px;  
}

/*--------------------*/

#inputs input {
  background: #f1f1f1;
  padding: 6px 5px;
  margin: 0 0 5px 0;
  width: 238px;
  border: 1px solid #ccc;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  -moz-box-shadow: 0 1px 1px #ccc inset;
  -webkit-box-shadow: 0 1px 1px #ccc inset;
  box-shadow: 0 1px 1px #ccc inset;
}

#inputs input:focus {
  background-color: #fff;
  border-color: #e8c291;
  outline: none;
  -moz-box-shadow: 0 0 0 1px #e8c291 inset;
  -webkit-box-shadow: 0 0 0 1px #e8c291 inset;
  box-shadow: 0 0 0 1px #e8c291 inset;
}

/*--------------------*/

#login #actions {
  margin: 10px 0 0 0;
}

#login #submit {		
  background-color: #d14545;
  background-image: -webkit-gradient(linear, left top, left bottom, from(#e97171), to(#d14545));
  background-image: -webkit-linear-gradient(top, #e97171, #d14545);
  background-image: -moz-linear-gradient(top, #e97171, #d14545);
  background-image: -ms-linear-gradient(top, #e97171, #d14545);
  background-image: -o-linear-gradient(top, #e97171, #d14545);
  background-image: linear-gradient(top, #e97171, #d14545);
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  text-shadow: 0 1px 0 rgba(0,0,0,.5);
  -moz-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
  -webkit-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
  box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;    
  border: 1px solid #7e1515;
  float: left;
  height: 30px;
  padding: 0;
  width: 100px;
  cursor: pointer;
  font: bold 14px Arial, Helvetica;
  color: #fff;
}

#login #submit:hover,
#login #submit:focus {		
  background-color: #e97171;
  background-image: -webkit-gradient(linear, left top, left bottom, from(#d14545), to(#e97171));
  background-image: -webkit-linear-gradient(top, #d14545, #e97171);
  background-image: -moz-linear-gradient(top, #d14545, #e97171);
  background-image: -ms-linear-gradient(top, #d14545, #e97171);
  background-image: -o-linear-gradient(top, #d14545, #e97171);
  background-image: linear-gradient(top, #d14545, #e97171);
}	

#login #submit:active {		
  outline: none;
  -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
  -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;		
}

#login #submit::-moz-focus-inner {
  border: none;
}

#login label {
  float: right;
  line-height: 30px;
}

#login label input {
  position: relative;
  top: 2px;
  right: 2px;
}

	</style>
<script>
$(document).ready(function(){
$('#login-trigger').click(function(){
	$(this).next('#login-content').slideToggle();
	$(this).toggleClass('active');					
	
	if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
		else $(this).find('span').html('&#x25BC;')
	})
});
</script>
	</head>
	<body onload="javascript:createRoute();">
		<script type="text/javascript">
			$(document).ready(function(){
				$(".display-contacts-link").click(function(){
					$("#cargo-contacts-"+$(this).attr("cargoid")).toggle();
				});
				$.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
				$(".hide-on-click").focus(function(){
					$(this).val("");
				});
			});
		</script>
		<?php if ($_smarty_tpl->tpl_vars['error']->value=='db'){?>
			<div class="warning">Ошибка БД на сайте. Разбираемся.</div>
		<?php }?>
		<div id="header">
			<div class="col w5 bottomlast">
				<a href="" class="logo">
					<img src="templates/images/logo.png" alt="Logo" style="border-radius: 20px;"/>
				</a>
			</div>

			<div class="col w5 last right bottomlast">

				<div style="margin-bottom:0px; text-align:center;"><img align="left" height="200px" src="http://lp.bigbiceps.kz/images/map.png"></div>
				
						<?php if (isset($_smarty_tpl->tpl_vars['authorized']->value)){?>
						<nav class="navv">
							<ul>
								<li id="login">
									<a id="login-trigger" href="#">
										Вы вошли как <span><?php echo $_smarty_tpl->tpl_vars['my_company_name']->value;?>
 &#x25BC;</span>
									</a>
									<div id="login-content">
										<a href="index.php?page=account">Мой профиль</a>
									</div>                     
								</li>
								<li id="signup">
									<a href="auth.php?signout">Выйти</a>
								</li>
							</ul>
						</nav>
						<?php }else{ ?>
						<nav class="navv">
							<ul>
								<li id="login">
									<a id="login-trigger" href="#">
										Войти <span>&#x25BC;</span>
									</a>
									<div id="login-content">
										<form id="login-form" action="auth.php" method="post">
											<fieldset id="inputs">
												<input type="text" name="email" placeholder="Введите E-mail" class="text w_20" /><br />
														<input type="password" name="password" placeholder="Введите свой пароль" class="text w_20" /><br />
											</fieldset>
											<fieldset id="actions">
												<input type="submit" id="submit" value="Войти">
												<label><input type="checkbox" checked="checked"> Запомнить меня</label>
											</fieldset>
										</form>
									</div>                     
								</li>
								<li id="signup">
									<a href="index.php?page=signup">Регистрация</a>
								</li>
							</ul>
						</nav>

						<?php }?>
					
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div id="wrapper">
			<div id="minwidth">
				<div id="holder">
					<div id="menu">
						<div id="left"></div>
						<div id="right"></div>
						<ul>
							<li>
								<a href="index.php"><span>Главная страница</span></a>
							</li>
							<li>
								<a href="index.php?page=cargosearch"><span>Поиск грузов</span></a>
							</li>
							<li>
								<a href="index.php?page=transportsearch"><span>Поиск транспорта</span></a>
							</li>
							<li>
								<a href="index.php?page=page&name=contacts"><span>Обратная связь и контакты</span></a>
							</li>
							<li>
								<a href="index.php?page=page&name=usefullinfo"><span>Полезная информация</span></a>
							</li>
						</ul>

						<div class="clear"></div>
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['authorized']->value)){?>
					<div id="submenu">
						<div class="modules_left">
							<div class="module buttons">
							
							<nav>
							    <ul class="nav">
							        <li><a href="" class="dropdown_button"><small class="icon clipboard"></small><span>Меню пользователя</span></a>
							            <!--i class="triangle"></i-->
							            <ul class="sub-nav">
							                <li>
							                	<a href="index.php?page=newfreight">
													Добавить новый груз
												</a>
											</li>
											<li>
												<a href="index.php?page=mycargo">
													Мой груз
												</a>
												
											</li>
											<li>
												<a href="index.php?page=newtransport">
													Добавить новый транспорт
												</a>
												
											</li>
											<li>
												<a href="index.php?page=mytransport">
													Мой транспорт
												</a>
											</li>
							            </ul>
							        </li>
							    </ul>
							</nav>
							
								<!--a href="" class="dropdown_button"><small class="icon clipboard"></small><span>Меню пользователя</span></a>
								<div class="dropdown help_index">
									<div class="arrow"></div>
									<div class="">
										<div class="col w11">									
										<a href="index.php?page=newfreight">
											Добавить новый груз
											<span>Добавьте свой груз для отправки</span>
										</a>
										<hr/>
										<a href="index.php?page=mycargo">
											Мои грузы
											<span>Грузы которые вы добавили</span>
										</a>
										<hr/>
										<a href="index.php?page=newtransport">
											Добавить новый транспорт
											<span>Добавьте свой транспорт и заработайте денег</span>
										</a>
										<hr/>
										<a href="index.php?page=mytransport">
											Мой транспорт
											<span>Транспорт который вы добавляли</span>
										</a>
										<hr/>
										</div>
										<div class="clear"></div>
										<a class="button red right close"><small class="icon cross"></small><span>Закрыть</span></a>
										<div class="clear"></div>
									</div>
								</div-->
							</div>
						</div>
						
						<div class="title">
							Последние грузы
						</div>
						
						<div class="modules_right">
						</div>
					</div>
					<?php }?>
					<div style="text-align:center">
					<script language="javascript" type="text/javascript" src="../js.php?id=1"></script>		
				       </div>
							<div id="table" class="help">
							<div class="col w10 last">
								
	<script type="text/javascript">
		$(document).ready(function(){
			$("#change_password_link").click(function(){
				$("#change-password-form").toggle();
			});
			$(".phone-input").blur(function(){
				if($(this).val().replace(/ /g,'').length<11 || $(this).val().replace(/ /g,'').length>12) $(this).after('<div class="warning">Убедитесь, что вы ввели телефон корректно.</div>');
			});
			$("#add-phone-link").click(function(){
				$(this).before('<div><input class="phone-input" type="text" name="phone[]" value="+7"/></div>');
			});
		});
	</script>
	<div class="title">Ваши данные</div>
		<?php if ($_smarty_tpl->tpl_vars['error']->value=="input"){?>
			<div class="warning">К сожалению, вы допустили ошибку при заполнении формы. Пожалуйста, попробуйте заполнить ее еще раз.</div>
		<?php }?>
		<table>
			<tr>
				<td class="form-label">Электронный адрес</td>
				<td>
					<div><?php echo $_smarty_tpl->tpl_vars['my_email']->value;?>
 <a id="change_password_link" href="javascript:void(0)">Сменить пароль</a></div>
					<?php if (isset($_smarty_tpl->tpl_vars['success']->value)){?><div class="success">Пароль успешно изменен.</div><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['error']->value=="donotmatch"){?><div class="warning">К сожалению, вы допустили ошибку. Введенные пароли не совпадают.</div><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['error']->value=="incorrectoldpass"){?><div class="warning">К сожалению, вы допустили ошибку. Текущий пароль не верен.</div><?php }?>
					<form id="change-password-form" action="index.php?page=account&action=newpassword" method="post" style="display:none">
						<input type="password" name="old_password"/> Текущий пароль<br/>
						<input type="password" name="new_password"/> Желаемый новый пароль<br/>
						<input type="password" name="new_password_again"/> Новый пароль еще раз<br/>
						<input type="submit" value="Сменить"/>
					</form>
				</td>
			</tr>
			<form action="index.php?page=account&action=udpate" method="post">
				<tr>
					<td class="form-label">
						<label>E-mail:</label>
					</td>
					<td class="form-input">
						<input type="text" name="email" value="<?php echo $_smarty_tpl->tpl_vars['my_email']->value;?>
" class="text w_20" />
					</td>
				</tr>
				<tr>
					<td class="form-label">Страна</td>
					<td class="form-input" style="white-space:nowrap"><?php echo smarty_function_html_options(array('name'=>"country",'options'=>$_smarty_tpl->tpl_vars['countries_list']->value,'selected'=>$_smarty_tpl->tpl_vars['my_country_id']->value,'class'=>"text w_20"),$_smarty_tpl);?>
</td>
				</tr>
				<tr>
					<td class="form-label">Город или селение</td>
					<td class="form-input"><input type="text" name="city" value="<?php echo $_smarty_tpl->tpl_vars['my_country']->value;?>
"/></td>
				</tr>
				<tr class="separator">
					<td class="form-label">Юридический статус</td>
					<td class="form-input" style="white-space:nowrap"><?php echo smarty_function_html_options(array('name'=>"legal_statuses_list",'options'=>$_smarty_tpl->tpl_vars['legal_statuses_list']->value,'selected'=>$_smarty_tpl->tpl_vars['my_status_id']->value,'class'=>"text w_20"),$_smarty_tpl);?>
</td>
				</tr>
				<tr>
					<td class="form-label">Ваше имя или наименование вашей организации</td>
					<td class="form-input"><input type="text" name="name" value="<?php echo $_smarty_tpl->tpl_vars['my_name']->value;?>
"/></td>
				</tr>
				<tr>
					<td class="form-label"></td>
					<td class="form-input"><input type="submit" value="Обновить профиль "/></td>
				</tr>
			</form>
			<tr>
				<td class="form-label">Телефон(ы)</td>
				<td class="form-input">
					<form action="index.php?page=account&action=changecontacts" method="post">
						<?php  $_smarty_tpl->tpl_vars['phone'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['phone']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['my_phones']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['phone']->key => $_smarty_tpl->tpl_vars['phone']->value){
$_smarty_tpl->tpl_vars['phone']->_loop = true;
?>
							<div><input type="text" class="phone-input" name="phone[]" value="<?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
"/></div>
						<?php } ?>
						<div><a id="add-phone-link" href="javascript:void(0)">Добавить еще телефон</a></div>
						<input type="submit" value="Обновить телефоны"/>
					</form>
				</td>
			</tr>
					
		</table>
			
							</div>
							<div class="clear"></div>
							</div>
							
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
						<div id="body_footer">
							<div id="bottom_left"><div id="bottom_right"></div></div>
						</div>
					</div>
				</div>
			</div>
			<div class="clear-both"></div>
		</div>
		<div id="footer">
		    <div class="center-wrapper">
		 
		        <div class="one-fifth text-align-center">
		            <a class="" href="#"><img width="135px" src="templates/images/logo.png" alt="" style=" border-radius: 10px;"></a>
		            <hr>
		            <p><em> Логистика и транспорт для требовательных клиентов.<br />У вас есть груз? У нас есть транспорт!</em></p>
		            <hr>
		            <ul class="social-icons align-center">
		                <li><a href="#"><img src="templates/images/facebook.png" alt=""></a></li>
		                <li><a href="#"><img src="templates/images/twitter.png" alt=""></a></li>
		                <li><a href="#"><img src="templates/images/rss.png" alt=""></a></li>
		            </ul><!-- end .social-icons -->
		 
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Навигация сайта</h3>
		            <ul class="links-list">
		                <li><a href="index.php">Главная</a></li>
		                <li><a href="index.php?page=signup">Регистрация</a></li>
		                <li><a href="index.php?page=cargosearch">Поиск грузов</a></li>
		                <li><a href="index.php?page=transportsearch">Поиск транспорта</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Партнеры</h3>
		            <ul class="links-list">
		                <li><a href="http://enlab.su/">ENLAB.SU</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Реклама</h3>
		            <ul class="links-list">
		                <li><a href="http://enlab.su/">Автор CMS Cargo<br/>(avtoperevozki.kz)</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <!--div class="one-fifth-last">
		            <a href="#"><h3>Trailers</h3></a>
		            <a href="#"><h3>Photos</h3></a>
		            <a href="#"><h3>Contacts</h3></a>
		        </div--><!-- end .one-fifth-last -->
		        
			</div><!-- end .center-wrapper -->
		</div><!-- end #footer -->
		<!--<div style="font-size:12px">Copyright &copy; 2013 Avtoperevozki.kz. All rights reserved. Create CMS <a href="mailto:flashhacker1988@gmail.com">Alex M.A.K.</a></div-->
	</body>
</html>
<?php }} ?>