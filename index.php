<?php

require_once "config.php";

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

if(isset($_SESSION['user']))
{
	$smarty->assign("authorized","true");
	$smarty->assign("my_company_name",$_SESSION['user']['name']);
	$smarty->assign("my_id",$_SESSION['user']['id']);
}

if(!isset($_GET['page']))
{
	$db2=new DataBase($db);
	$db->query("SELECT `id`,`name` FROM `transport_types`");
	while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
	
	$db->query("SELECT `id`,`name` FROM `transport_modes`");
	while(list($id,$name)=$db->fetchRow()) $transportss[$id]=$name;
	
	// Get cargoes from DB
	$db->query("SELECT `id`, `account_id`, `load_from`, `load_due`, `cargo_name`, `transport_required`, `transport_modes`, `canconsolidate`, `additional_info`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `cargo` ORDER BY `load_due` DESC LIMIT 30");
	if($db->numRows()==0)
	{
		$smarty->assign("nocargo","true");
	}
	$cargo_arr=array();
	while(list($id, $account_id, $load_from, $load_due, $cargo_name, $transport_type, $transport_modes, $canconsolidate, $info, $weight, $volume, $vehicles_number, $length, $width, $height, $price)=$db->fetchRow())
	{
		$cargo_arr[$id]['account_id']=$account_id;
		$cargo_arr[$id]['load_from']=$load_from;
		$cargo_arr[$id]['load_due']=$load_due;
		$cargo_arr[$id]['cargo_name']=$cargo_name;
		$cargo_arr[$id]['transport_type']=$transports[$transport_type];
		$cargo_arr[$id]['transport_modes']=$transportss[$transport_modes];
		$cargo_arr[$id]['canconsolidate']=($canconsolidate==1)?'on':'off';
		$cargo_arr[$id]['info']=$info;
		$cargo_arr[$id]['weight']=$weight;
		$cargo_arr[$id]['volume']=$volume;
		$cargo_arr[$id]['vehicles_number']=$vehicles_number;
		$cargo_arr[$id]['length']=$length;
		$cargo_arr[$id]['width']=$width;
		$cargo_arr[$id]['height']=$height;
		if($price) $cargo_arr[$id]['price']=number_format($price, 0, ',', " ");
		$db2->query("SELECT `city_name_ru` FROM `cargo_origins`,`city_` WHERE `cargo_origins`.`cargo_id`='$id' AND `cargo_origins`.`origin`=`city_`.`id` ORDER BY `cargo_origins`.`id`");
		while(list($o)=$db2->fetchRow()) $cargo_arr[$id]['origins'][]=$o;
		$db2->query("SELECT `city_name_ru` FROM `cargo_destinations`,`city_` WHERE `cargo_destinations`.`cargo_id`='$id' AND `cargo_destinations`.`destination`=`city_`.`id` ORDER BY `cargo_destinations`.`id`");
		while(list($d)=$db2->fetchRow()) $cargo_arr[$id]['destinations'][]=$d;
		$db2->query("SELECT `name`,`email`,`contact_text` FROM `accounts`,`contacts` WHERE `accounts`.`id`=`contacts`.`account_id` AND `accounts`.`id`='$account_id'");
		while(list($name,$email,$contact)=$db2->fetchRow())
		{
			$cargo_arr[$id]['contact_name']=$name;
			$cargo_arr[$id]['contact_email']=$email;
			$cargo_arr[$id]['contact_phones'][]=$contact;
		}
	}
	$smarty->assign("cargos",$cargo_arr);
	/*
	unset($cargo_arr);
	// Get transports from DB
	$db->query("SELECT `id`, `account_id`, `load_from`, `load_due`, `transport_required`, `transport_modes`, `canconsolidate`, `additional_info`,  `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `transport` ORDER BY `load_due` DESC LIMIT 30");
	$cargo_arr=array();
	while(list($id, $account_id, $load_from, $load_due, $cargo_name, $transport_type, $transport_modes, $canconsolidate, $info, $weight, $volume, $vehicles_number, $length, $width, $height, $price)=$db->fetchRow())
	{
		$cargo_arr[$id]['account_id']=$account_id;
		$cargo_arr[$id]['load_from']=$load_from;
		$cargo_arr[$id]['load_due']=$load_due;
		$cargo_arr[$id]['cargo_name']=$cargo_name;
		$cargo_arr[$id]['transport_type']=$transports[$transport_type];
		$cargo_arr[$id]['transport_modes']=$transportss[$transport_modes];
		$cargo_arr[$id]['canconsolidate']=($canconsolidate==1)?'on':'off';
		$cargo_arr[$id]['info']=$info;
		$cargo_arr[$id]['weight']=$weight;
		$cargo_arr[$id]['volume']=$volume;
		$cargo_arr[$id]['vehicles_number']=$vehicles_number;
		$cargo_arr[$id]['length']=$length;
		$cargo_arr[$id]['width']=$width;
		$cargo_arr[$id]['height']=$height;
		if($price) $cargo_arr[$id]['price']=number_format($price, 0, ',', " ");
	$db2->query("SELECT `city_name_ru` FROM `cargo_origins`,`city_` WHERE `cargo_origins`.`cargo_id`='$id' AND `cargo_origins`.`origin`=`city_`.`id` ORDER BY `cargo_origins`.`id`");
	while(list($o)=$db2->fetchRow()) $cargo_arr[$id]['origins'][]=$o;
	$db2->query("SELECT `city_name_ru` FROM `cargo_destinations`,`city_` WHERE `cargo_destinations`.`cargo_id`='$id' AND `cargo_destinations`.`destination`=`city_`.`id` ORDER BY `cargo_destinations`.`id`");
	while(list($d)=$db2->fetchRow()) $cargo_arr[$id]['destinations'][]=$d;
		$db2->query("SELECT `name`,`email`,`contact_text` FROM `accounts`,`contacts` WHERE `accounts`.`id`=`contacts`.`account_id` AND `accounts`.`id`='$account_id'");
		while(list($name,$email,$contact)=$db2->fetchRow())
		{
			$cargo_arr[$id]['contact_name']=$name;
			$cargo_arr[$id]['contact_email']=$email;
			$cargo_arr[$id]['contact_phones'][]=$contact;
		}
	}
	$smarty->assign("cargos",$cargo_arr);
	*/
	//Draw chart 
	//TODO: correct the sql to take into account huge database. add some way to cut data by using not more than one year data also make sure prices are from the same category, so there wont be huge fluctuations
	$db->query("SELECT `load_from`,`price` FROM `cargo` WHERE `price`>0 ORDER BY `load_from`");
	while(list($load_from,$price)=$db->fetchRow())
	{
		list($y,$m,$d)=explode("-",$load_from);
		$prices_chart["$m/$y"]["cargo"]=$price;
	}
	
	$db->query("SELECT `load_from`,`price` FROM `transport` WHERE `price`>0 ORDER BY `load_from`");
	while(list($load_from,$price)=$db->fetchRow())
	{
		list($y,$m,$d)=explode("-",$load_from);
		$prices_chart["$m/$y"]["transport"]=$price;
	}
	//print_r($prices_chart);
	
	//find average for each category
	$sum_cargo=0; $sum_transport=0; $count_cargo=0; $count_transport=0;
	foreach($prices_chart as $key=>$value)
	{
		if(isset($value["cargo"])){ $sum_cargo+=$value["cargo"]; $count_cargo++; }
		if(isset($value["transport"])){ $sum_transport+=$value["transport"]; $count_transport++; }
	}
	$cargo_average_price=$sum_cargo/$count_cargo;
	$transport_average_price=$sum_transport/$count_transport;
	
	foreach($prices_chart as $key=>$value)
	{
		//print_r($value); echo "<br/>";
		if(!isset($value["cargo"])) $prices_chart[$key]["cargo"]=$cargo_average_price;
		if(!isset($value["transport"])) $prices_chart[$key]["transport"]=$transport_average_price;
	}
	
	//print_r($prices_chart);
	
	$smarty->assign("prices_chart",$prices_chart);
	
	$smarty->display("index.tpl");
}
elseif(in_array($_GET['page'],$allowed_pages))
{
	$page=$_GET['page'];
	include_once("mods/$page.php");
	$smarty->display("$page.tpl");
}
else
{
	$smarty->display('404.tpl');
}


?>