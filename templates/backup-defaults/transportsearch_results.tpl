{if isset($transports)}
	<table id="cargo-table">
		{foreach item="transport" key="id" from=$transports}
			<tr>
				<td colspan="5" class="origin-destination">
					{foreach name="originsloop" item="origin" from=$transport.origins}
						<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
					{/foreach}
					&rarr;
					{foreach name="destinationsloop" item="destination" from=$transport.destinations}
						<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
					{/foreach}
					<div style="float:right"><a href="javascript:void(0)" class="display-contacts-link" cargoid="{$id}">Отобразить контакты</a></div>
				</td>
			</tr>
			<tr class="cargo-container">
				<td class="favorite">{if isset($my_id)}<img class="add-to-favorites" cargoid="{$id}" src="templates/img/favorite_no.png" alt=""/>{/if}</td>
				<td class="delivery-dates">
					{$transport.load_from|date_format:"%d.%m"} &mdash; {$transport.load_due|date_format:"%d.%m"}
					{if isset($my_id) && $my_id==$transport.account_id}<div><a href="index.php?page=mytransport&action=delete&id={$id}" class="delete">Удалить</a></div>{/if}
				</td>
				<td style="text-align:center">
					<div>{$transport.transport_required}</div>
					{if $transport.vehicles_number>0}<div class="car-amount">Нужно {$transport.vehicles_number} машин</div>{/if}
				</td>
				<td style="text-align:center">
					{if $transport.weight>0}{$transport.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
				</td>
				<td style="text-align:center">
					{if $transport.volume>0}{$transport.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
					{if $transport.length>0 && $transport.width>0 && $transport.height>0}{$transport.length}м x {$transport.width}м x {$transport.height}м м{/if}
				</td>
			</tr>
			<tr id="cargo-contacts-{$id}" class="cargo-contacts" style="display:none">
				<td colspan="5">
						<div class="cargo-contact-name">{$transport.contact_name}</div>
						<div>{$transport.contact_email}</div>
						<div class="cargo-phones">
							{foreach item="phone" from=$transport.contact_phones}
								{$phone}
							{/foreach}
							<div><a href="javascript:void(0)" class="display-contacts-link" cargoid="{$id}" style="text-decoration:none; margin-right:10px; font-weight:bold;">X</a></div>
						</div>
						
				</td>
			</tr>
		{/foreach}
	</table>
{else}
	<div style="text-align:center">По указанным вами критериям, ничего не найдено.</div>
{/if}
