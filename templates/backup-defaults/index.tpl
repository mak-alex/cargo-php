<html>
	<head>
		<link href="templates/style.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="templates/js/jquery-ui-1.10.2/themes/base/jquery-ui.css" />
		<script type="text/javascript" src="templates/js/jquery-1.9.1.min.js"></script>
		<script src="templates/js/jquery-ui-1.10.2/ui/jquery-ui.js"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
		<script type="text/javascript" src="jquery.notification.js"></script>
		<link type="text/css" rel="stylesheet" href="jquery.notification.css" />
		<script type="text/javascript">
			$(document).ready( function() {
				$('.notify').click( function() {
					$.notification($('INPUT[name=message]').val(), {
						duration: parseInt($('INPUT[name=duration]').val()),
						freezeOnHover: $('INPUT[name=freezeOnHover]').prop('checked'),
						hideSpeed: parseInt($('INPUT[name=hideSpeed]').val()),
						position: $(this).attr('id'),
						showSpeed: parseInt($('INPUT[name=showSpeed]').val())
					});
				});
			});
		</script>
		<script type="text/javascript">
			{literal}
				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback(drawChart);
				function drawChart() {
				var data = google.visualization.arrayToDataTable([
				  ['Месяц/год', 'Запрашиваемые цены', 'Предлагаемые цены'],
			{/literal}
				  /*
				  ['2004',  1000,      400],
				  ['2005',  1170,      460],
				  ['2006',  660,       1120],
				  ['2007',  1030,      540]
				  */
				  {foreach name="chartloop" item="chart" key="load_date" from=$prices_chart}
					['{$load_date}',{$chart.cargo},{$chart.transport}]{if $smarty.foreach.chartloop.last}{else}{literal}, {/literal}{/if}
				  {/foreach}
			{literal}
				]);

				var options = {
				  title: 'Предлагаемые и запрашиваемые цены за перевозку грузов'
				};

				var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
				chart.draw(data, options);
				}
			{/literal}
		</script>
		<meta name="Keywords" content="найти перевозчика, найти груз, найти транспорт,  автоперевозки, avtoperevozki, доставка грузов, перевозки грузов, перевезти груз, рузоперевозки, попутный груз, попутный транспорт, грузовик, найти фуру, грузы в Казахстан, грузы из Казахстана, грузы по Казахстану, грузы в Россию, грузы в Москву, догруз, рефрижератор, тент, контейнеровоз, шаланда, дальнобойщик"/>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/> 
	</head>
	<body>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".display-contacts-link").click(function(){
					$("#cargo-contacts-"+$(this).attr("cargoid")).toggle();
				});
				$.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
				$(".hide-on-click").focus(function(){
					$(this).val("");
				});
			});
		</script>
		{if $error=='db'}
			<div class="warning">Ошибка БД на сайте. Разбираемся.</div>
		{/if}
		<table id="header">
			<tr>
				<td style="width:100%"><a href="index.php"><img id="logo" src="templates/img/logo.png" alt="logo" title="logo"/></a></td>
				<td id="login" style="white-space:nowrap">
					{block name="login-box"}
						{if isset($authorized)}
							<div>{$my_company_name} <a href="index.php?page=account">Мой профиль</a> <a href="auth.php?signout">Выйти из системы</a></div>
						{else}
							<form id="login-form" action="auth.php" method="post">
								<input type="text" name="email" value="E-mail" class="hide-on-click"/>
								<input type="password" name="password" value="password" class="hide-on-click"/>
								<input type="submit" value="Войти" class="button"/>
								<a href="index.php?page=signup">Регистрация</a>
							</form>
						{/if}
					{/block}
				</td>
			</tr>
		</table>
		<div id="outer-navigation">
			<a href="index.php">Главная страница</a>
			<a href="index.php?page=cargosearch">Поиск грузов</a>
			<a href="index.php?page=transportsearch">Поиск транспорта</a>
			<a href="index.php?page=page&name=contacts">Обратная связь и контакты</a>
			<a href="index.php?page=page&name=usefullinfo">Полезная информация</a>
		</div>
		{if isset($authorized)}
			<div id="inner-navigation">
				<a href="index.php?page=newfreight">Новый груз</a>
				<a href="index.php?page=mycargo">Ваши грузы</a>
				<a href="index.php?page=newtransport">Новый транспорт</a>
				<a href="index.php?page=mytransport">Ваш транспорт</a>
			</div>
		{/if}
		<div style="text-align:center; margin:10px;"><img src="templates/img/footer-image.jpg" alt=""/></div>
		<div id="main-div">
			{block name="main"}
				<div class="title">Последние грузы</div>
				<table id="cargo-table">
					{foreach item="cargo" key="id" from=$cargos}
					<div id="wrap">
						<div id="left_col">
						<tr>
							<td colspan="5" class="origin-destination">
								{foreach name="originsloop" item="origin" from=$cargo.origins}
									<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
								{/foreach}
								&rarr;
								{foreach name="destinationsloop" item="destination" from=$cargo.destinations}
									<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
								{/foreach}
								<div style="float:right"><a href="javascript:void(0)" class="display-contacts-link" cargoid="{$id}">Отобразить контакты</a></div>
							</td>
						</tr>
						<tr class="cargo-container">
							<td class="delivery-dates">
								{$cargo.load_from|date_format:"%d.%m.%g"} &mdash; {$cargo.load_due|date_format:"%d.%m.%g"}
								{if isset($my_id) && $my_id==$cargo.account_id}<div><a href="index.php?page=mycargo&action=delete&id={$id}" class="delete">Удалить</a> | <a href="index.php?page=editfreight&action=edit&id={$id}" class="edit">Изменить</a></div>{/if}
							</td>
							<td>
								<div class="cargo-name">{$cargo.cargo_name}</div>
								{if isset($cargo.price)}<div class="cargo-price">Цена:{$cargo.price} тенге</div>{/if}
							</td>
							<td style="text-align:center">
								<div>{$cargo.transport_required}</div>
								{if $cargo.vehicles_number>0}<div class="car-amount">Нужно {$cargo.vehicles_number} машин</div>{/if}
							</td>
							<td style="text-align:center">
								{if $cargo.weight>0}{$cargo.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
							</td>
							<td style="text-align:center">
								{if $cargo.volume>0}{$cargo.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
								{if $cargo.length>0 && $cargo.width>0 && $cargo.height>0}{$cargo.length}м x {$cargo.width}м x {$cargo.height}м м{/if}
							</td>
						</tr>
					</div>
					<div id="right_col">
						<tr id="cargo-contacts-{$id}" class="cargo-contacts" style="display:none">
							<td colspan="5">
									<div class="cargo-contact-name">{$cargo.contact_name}</div>
									<div>{$cargo.contact_email}</div>
									<div class="cargo-phones">
										{foreach item="phone" from=$cargo.contact_phones}
											{$phone}
										{/foreach}
										<div><a href="javascript:void(0)" class="display-contacts-link" cargoid="{$id}" style="text-decoration:none; margin-right:10px; font-weight:bold;">X</a></div>
									</div>
									
							</td>
						</tr>
					</div>
					</div>
					{/foreach}
				</table>
				
				<table style="width:100%">
					<tr>
						<td style="width:70%">
							<div id="chart_div"></div>
						</td>
						<td style="width:30%"><img src="ads/ad1.jpg" alt=""/></td>
					</tr>
				</table>
				
				<div style="color:#555555; font-size:14px; padding:50px; text-align:justify">
					<p>
					Сайт avtoperevozki.kz призван помочь участникам рынка автомобильных грузоперевозок своевременно и качественно решать вопросы по доставке грузов или их поиску. Всего лишь разместив заявку на сайте, Вы мгновенно сообщите всем остальным пользователям о своей потребности отправить Ваш груз или, наоборот, о Вашей готовности перевезти груз, а, грамотно указав основные параметры перевозки, существенно сократите потери времени на бесконечный пересказ этих параметров по телефону.
					<br/>
					Надеемся, что теперь Ваши задачи по перевозке грузов по Казахстану, России и СНГ или Европе, поиску рефрижератора, тентованного грузовика или контейнеровоза и другие будут решаться гораздо проще и быстрее.
					</p>
					<p>
					AVTOPEREVOZKI.KZ Все права защищены. Любое использование содержимого сайта без письменного согласия собственника – запрещено.
					</p>
				</div>

			{/block}
		</div>
		<div id="footer">
			<div style="text-align:center"><img style="width:98%" src="templates/img/footer-image.jpg" alt=""/></div>
			<div id="footer-note">
				<div><a href="index.php">Главная</a> | <a href="index.php?page=signup">Регистрация</a> | <a href="index.php?page=cargosearch">Поиск грузов</a></div>
				<br/>
				<div style="font-size:12px">Copyright &copy; 2013 Avtoperevozki.kz. All rights reserved.</div>
			</div>
		</div>
	</body>
</html>