{extends file="index.tpl"}
{block name="main"}
	<div class="title">Связаться с нами</div>
	<form action="mods/contactus.php" method="post" style="padding:30px">
		<div><input type="" name="name" style="width:100%" value="Ваше имя" class="hide-on-click"/></div>
		<div><input type="" name="email" style="width:100%" value="Ваше E-mail" class="hide-on-click"/></div>
		<div><textarea style="width:100%" name="" class="hide-on-click">Ваше сообщение</textarea></div>
		<div><input type="submit" value="Отправить"/></div>
	</form>
	<br/>
	<br/>
	<div class="title">Наши контакты</div>
	<div style="padding:30px">
		Если у Вас возникли сложности с поиском грузов или с тем, как найти перевозчика, Вы можете связаться с нами:<br/>
		<ul>
			<li>e-mail: info@avtoperevozki.kz</li>
			<li>skype: avtoperevozki kz</li>
			<li>тел.: +7 727 227 72 67</li>
		</ul>
	</div>
{/block}
