{extends file="index.tpl"}
{block name="main"}
	<script type="text/javascript">
		$(document).ready(function(){
			$("select[name='from_region']").change(function(){
				//alert("here");
				$("select[name='from_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[name='from_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='from_city']").attr("disabled",false);
			});
			$("select[name='from_country']").change(function(){
				//alert("here");
				$("select[name='from_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getregionoptions.php?id='+countryid, function(data) {
					$("select[name='from_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('getcityoptions.php?id='+countryid, function(data) {
					$("select[name='from_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='from_city']").attr("disabled",false);
			});
			
			$("select[name='to_region']").change(function(){
				//alert("here");
				$("select[name='to_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[name='to_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='to_city']").attr("disabled",false);
			});
			$("select[name='to_country']").change(function(){
				//alert("here");
				$("select[name='to_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getregionoptions.php?id='+countryid, function(data) {
					$("select[name='to_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('getcityoptions.php?id='+countryid, function(data) {
					$("select[name='to_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='to_city']").attr("disabled",false);
			});
			{literal}
				$( "input[name='date_from']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
				$( "input[name='date_to']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
			{/literal}
			$("#cargo-search-filter").submit(function(){
				url="index.php?"+$(this).serialize()+"&ajax";
				//alert(url);
				$("input[type='submit']").val("Ищем, подождите...");
				$.get(url,function(data){
					$("#results").html(data);
					$(".display-contacts-link").click(function(){ $("#cargo-contacts-"+$(this).attr("cargoid")).toggle(); });
					$(".add-to-favorites").click(function(){
						//do ajax action to add to favorites
						$(this).attr("src","templates/img/favorite_yes.png");
						item_id=$(this).attr("cargoid");
						$.get("mods/addtofavorites.php?myid={$my_id}&itemid="+item_id+"&type=cargo",function(data){
							// do nothing
						});
					});
				});
				$("input[type='submit']").val("Еще поиск");
				return false;
			});
		});
	</script>
	<div class="title alternative">Поиск транспорта</div>
	<form action="index.php?page=transportsearch" method="get" id="cargo-search-filter">
		<input type="hidden" name="page" value="transportsearch"/>
		<div style="margin:10px; overflow:hidden; padding:0px;background-color:#f0f0f0">
			<div style="width:50%; float:left">
				<div class="title">Откуда</div>
				<p style="padding:10px">
					Страна<br/>
					{html_options name="from_country" options=$from_country}<br/>
					Область<br/>
					{html_options name="from_region" options=$from_region}<br/>
					Город<br/>
					{html_options name="from_city" options=$from_city}
				</p>
			</div>
			<div style="width:50%; float:right">
				<div class="title">Куда</div>
				<p style="padding:10px">
					Страна<br/>
					{html_options name="to_country" options=$from_country}<br/>
					Область<br/>
					{html_options name="to_region" options=$from_region}<br/>
					Город<br/>
					{html_options name="to_city" options=$from_city}
				</p>
			</div>
			<div style="clear:both"></div>
			<div style="text-align:center;">
				<div class="title">Даты погрузки</div>
				<p style="padding:10px">
					С <input type="text" name="date_from" value="{$current_date}"/> по <input type="text" name="date_to" value="{$due_date}"/>
					<!-- Вес (тонна) {html_options name="weight" options=$possible_weights selected=0 style="width:50px"} Обьем (м<sup>3</sup>) {html_options name="volume" options=$possible_volumes selected=0  style="width:50px"} -->
					Вес (тонна) от <input type="text" name="weight_from"/> до <input type="text" name="weight_to"/>
					Объем (м<sup>3</sup>) от <input type="text" name="volume_from"/> до <input type="text" name="volume_to"/>
				</p>
			</div>
		</div>
		<div style="text-align:center"><input type="submit" value="Поиск" class="button"/></div>
	</form>
	<div id="results">
		{if isset($transports)}
			<table id="cargo-table">
				{foreach item="cargo" key="id" from=$transports}
					<tr class="cargo-container">
						{if isset($my_id)}<td class="favorite"><img class="add-to-favorites" cargoid="{$id}" src="templates/img/favorite_no.png" alt=""/></td>{/if}
						<td class="delivery-dates">
							{$cargo.load_from|date_format:"%d.%m"} &mdash; {$cargo.load_due|date_format:"%d.%m"}
							{if isset($my_id) && $my_id==$cargo.account_id}<div><a href="index.php?page=mycargo&action=delete&id={$id}" class="delete">Удалить</a></div>{/if}
						</td>
						<td>
							<div class="origin-destination">
								{foreach name="originsloop" item="origin" from=$cargo.origins}
									<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
								{/foreach}
								&rarr;
								{foreach name="destinationsloop" item="destination" from=$cargo.destinations}
									<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
								{/foreach}
							</div>
							<div class="cargo-name">{$cargo.cargo_name}</div>
							{if isset($cargo.price)}<div class="cargo-price">Цена:{$cargo.price} тенге</div>{/if}
							<a href="javascript:void(0)" class="display-contacts-link" cargoid="{$id}">Отобразить контакты</a>
							<div id="cargo-contacts-{$id}" class="cargo-contacts" style="display:none">
								<div class="cargo-contact-name">{$cargo.contact_name}</div>
								<div>{$cargo.contact_email}</div>
								<div class="cargo-phones">
									{foreach item="phone" from=$cargo.contact_phones}
										{$phone}<br/>
									{/foreach}
								</div>
							</div>
						</td>
						<td style="text-align:center">
							<div>{$cargo.transport_required}</div>
							{if $cargo.vehicles_number>0}<div class="car-amount">Нужно {$cargo.vehicles_number} машин</div>{/if}
						</td>
						<td style="text-align:center">
							{if $cargo.weight>0}{$cargo.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
						</td>
						<td style="text-align:center">
							{if $cargo.volume>0}{$cargo.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
							{if $cargo.length>0 && $cargo.width>0 && $cargo.height>0}{$cargo.length}м x {$cargo.width}м x {$cargo.height}м м{/if}
						</td>
					</tr>
				{/foreach}
			</table>
		{else}
			<div style="text-align:center">По указанным вами критериям, ничего не найдено.</div>
		{/if}
	</div>	
{/block}
