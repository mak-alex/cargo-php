var map, mapRoute;
ymaps.ready(function() {
    map = new ymaps.Map('map', {
        center: [71.435742, 51.174298], 
        zoom: 14
    });
});



function createRoute() {
    // Удаление старого маршрута
    if (mapRoute) {
        map.geoObjects.remove(mapRoute);
    }

    var routeFrom = document.getElementById('route-from').value;
    var routeTo = document.getElementById('route-to').value;

    // Создание маршрута
    ymaps.route([routeFrom, routeTo], {mapStateAutoApply:true}).then(
        function(route) {
            map.geoObjects.add(route);
            map.controls.add('zoomControl');
            map.controls.add('miniMap');
            map.controls.add('trafficControl');

            document.getElementById('route-length').innerHTML = '<strong>'+route.getHumanLength()+'</strong>';
            document.getElementById('route-time').innerHTML = '<strong>'+route.getHumanJamsTime()+'</strong>';
            document.getElementById('route-speed').innerHTML = '<strong>'+Math.round(parseInt(route.getHumanLength())/parseInt(route.getJamsTime()/60/60))+' км/ч</strong>';
            mapRoute = route;
        },
        function(error) {
            alert('Невозможно построить маршрут');
        }
    );
}
