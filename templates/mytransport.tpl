{extends file="index.tpl"}
{block name="title"}
	<div class="title">
		Мой транспорт
	</div>
{/block}
{block name="main"}
	<script type="text/javascript"></script>
	<table id="cargo-table">
		<tr>
			<th><strong>Направление и даты</strong></th>
			<th><strong>Цена</strong></th>
			<th><strong>Тип и кол-во транспорта</strong></th>
			<th><strong>Грузоподъемность</strong></th>
			<th><strong>Объем и габариты</strong></th>
			<th><strong>Управление</strong></th>
		</tr>
		{foreach item="transport" key="id" from=$transports}
			<tr class="cargo-container">
				<td class="delivery-dates">
					{$transport.date_from|date_format:"%d.%m.%g"} &mdash; {$transport.date_due|date_format:"%d.%m.%g"}<br/>
					{foreach name="originsloop" item="origin" from=$transport.origins}
							<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
						{/foreach}
						&rarr;
						{foreach name="destinationsloop" item="destination" from=$transport.destinations}
							<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
						{/foreach}
				</td>
				<td>
					<div class="origin-destination">
						
					</div>
					{if isset($transport.price)}<div class="cargo-price">Цена:{$transport.price} тенге</div>{/if}
				</td>
				<td style="text-align:center">
					<!--div>{$transport.transport_required}</div-->
					<table style="text-align:left;">
			
								<!--tr>
									<td><b>Марка: </b></td>
									<td>
										{if strlen($transport.brand)==0}
										0<br/>
										{else}
										{$transport.brand}<br/>
										{/if}
									</td>
								</tr-->
								<tr>
									<td><b>Вид транспорта: </b></td>
									
									<td>
									{if strlen($transport.transports_modes)==0}
									0<br/>
									{else}
									{$transport.transports_modes}<br/>
									{/if}
									</td>
								</tr>
								<tr>
									<td><b>Тип транспорта: </b></td>
									<td>
									{if strlen($transport.transports_type)==0}
									Не указано<br/>
									{else}
									{$transport.transports_type}<br/>
									{/if}
									</td>
									
								</tr>
								<!--tr>
									<td><b>Вместимость, машин: </b></td>
									<td>
									{if strlen($transport.capacity)==0}
									0<br/>
									{else}
									{$transport.capacity}<br/>
									{/if}
									</td>
									
								</tr>
								<tr>
									<td><b>Посадочных мест, шт.: </b></td>
									<td>
									{if strlen($transport.seats)==0}
									0<br/>
									{else}
									{$transport.seats}<br/>
									{/if}
									</td>
								</tr-->
								<!--tr>
									<td><b>Доп.инфо: </b></td>
									<td>
									{if strlen($transport.information)==0}
									0<br/>
									{else}
									{$transport.information}<br/>
									{/if}
									</td>
									
								</tr-->
								<tr>
									<td><b>Единиц транспорта в наличии:</b></td>
									<td>
									{if strlen($transport.car_amount)==0}
									Не указано<br/>
									{else}
									{$transport.car_amount}<br/>
									{/if}
									</td>
								<tr>
									<td><b>Перевозка негабаритного груза:<b></td>
									<td>
									{if $transport.notgabarite=="0"}Нет{else}Да{/if}
									</td>
					</tr>
					</table>
					
				</td>
				<td style="text-align:center">
					{if $transport.weight>0}{$transport.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
				</td>
				<td style="text-align:center">
					{if $transport.volume>0}{$transport.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
					{if $transport.length>0 && $transport.width>0 && $transport.height>0}{$transport.length}м x {$transport.width}м x {$transport.height}м м{/if}
				</td>
				<td>
					<div id="cargo-contacts-{$id}" style="text-align:right;">
                        <a href="#" class="tooltip yellow-tooltip edit" data-html="true" data-title="Имя: {$transport.contact_name}
                            E-mail: {$transport.contact_email}; 
                            {foreach item="phone" from=$transport.contact_phones}
                            		Телефон: {$phone}
                            {/foreach}"><img src="templates/images/phone.png" style="width:24px; height:24px;"></a>
                    
					<a href="index.php?page=mytransport&action=delete&id={$id}" class="delete" title="Удалить"><img src="templates/images/bin.png" style="width:24px; height:24px"></a>
					<a href="index.php?page=edittransport&action=edit&id={$id}" class="edit" title="Изменить"><img src="templates/images/pencil.png" style="width:24px; height:24px"></a>
					</div>
				</td>
			</tr>
		{/foreach}
	</table>					
{/block}
