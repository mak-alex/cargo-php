{extends file="index.tpl"}
{block name="title"}
	<div class="title">
		Добавить транспорт
	</div>
{/block}
{block name="main"}

	{if $error=="input"}
		<div class="warning">К сожалению, вы допустили ошибку при заполнении формы. Пожалуйста, попробуйте заполнить ее еще раз.</div>
	{/if}
	<form id="cargo-form" action="index.php?page=newtransport&action=do" method="post">
		<table><tr><td>
		<div style="text-align:right">Все, что выделено <b>жирным</b> - обязательно для заполнения. Остальные поля, при желании, можете пропустить.</div>
		</tr></td></table>
		<table>
			<tr>
				<td style="70%"><label>Желаемая дата погрузки</label></td><td class="form-input">с <input type="text" name="date_from" value="{$current_date}" class="text w_20" /> по <input type="text" name="date_due" value="{$due_date}" class="text w_20" />
				{if $error=="date"}<div class="warning">Даты были введены не правильно</div>{/if}
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Предпочтительное направление</label></td>
				<td class="form-input">
					<div id="country-city-from" class="location-selection">
						<!--{html_options name="blah" class="country-select" options=$from_country selected=222 style="width:250px"}
						{html_options name="origin[]" options=$from_city selected=21332 style="width:250px"}-->
						Страна<br/>
						{html_options name="from_country" options=$from_country style="width:250px;"}<br/>
						Область<br/>
						{html_options name="from_region" options=$from_region style="width:250px;"}<br/>
						Город<br/>
						{html_options name="from_city" options=$from_city style="width:250px;"}
					</div>
					<!--a id="more-origin-link" href="javascript:void(0)">Дополнительный пункт погрузки</a-->
					{if $error=="noorigin"}<div class="warning">Введите хотя бы один пункт отправки</div>{/if}
				</td>
			</tr>
			<tr class="separator">
				<td style="70%"><label>Предпочтительное направление</label></td>
				<td class="form-input">
					<div id="country-city-to" class="location-selection">
						{html_options name="blah" class="country-select" options=$from_country selected=222 style="width:250px"}
						{html_options name="destination[]" options=$from_city selected=21332 style="width:250px"}
					</div>
					<!--a id="more-destination-link" href="javascript:void(0)">Дополнительный пункт выгрузки</a-->
					{if $error=="nodestination"}<div class="warning">Введите хотя бы один пункт выгрузки</div>{/if}
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Необходимый тип транспорта</label></td>
				<td class="form-input">{html_options name="transports" options=$transports selected=0 style="width:250px"}<br />
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Необходимый вид транспорта</label></td>
				<td class="form-input">{html_options id="transports_modes" name="transports_modes" options=$transport_modes selected=0 style="width:250px" onchange="moreopt()"}<br />
					<input type="checkbox" class="checkbox" name="notgabarite">Перевозка негабаритного груза<br/>
					<script type="text/javascript">
						function moreopt(){
						    var e = document.getElementById("transports_modes");
							var strUser = e.options[e.selectedIndex].value;
						    if (strUser>1){
						        document.getElementById('more-opt').innerHTML = ('\
				<h2>Характеристики транспортного средства:</h2>\
				<label>Марка</label>\
				<td class="form-input">\
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">\
						<input type="text" name="brand" placeholder="Не указано" class="text w_20" />\
					</span>\
				</td>\
				<label>Вместимость, машин.</label>\
				<td class="form-input">\
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">\
						<input type="text" name="capacity" placeholder="Не указано" class="text w_20" />\
					</span>\
				</td>\
				<label>Посадочных мест, шт.</label>\
				<td class="form-input">\
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">\
						<input type="text" name="seats" placeholder="Не указано" class="text w_20" />\
					</span>\
				</td>\
				<label>Дополнительная информация.</label>\
				<td class="form-input">\
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">\
						<input type="text" name="information" placeholder="Не указано" class="text w_20" />\
					</span>\
				</td>\
						        	') ;
						    } else if(strUser==2) {
						    	document.getElementById('more-opt').innerHTML = ('\
				<h2>Характеристики транспортного средства:</h2>\
				<label>Посадочных мест, шт.</label>\
				<td class="form-input">\
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">\
						<input type="text" name="seats" placeholder="Не указано" class="text w_20" />\
					</span>\
				</td>\
				<label>Дополнительная информация.</label>\
				<td class="form-input">\
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">\
						<input type="text" name="information" placeholder="Не указано" class="text w_20" />\
					</span>\
				</td>\
						        	') ;
						    }
						}
						</script>
						<div id="more-opt"></div>
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Единиц транспорта в наличии</label></td>
				<td class="form-input">
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<input type="text" name="car_amount" placeholder="Не указано" class="text w_20" />
					</span>
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Грузоподъемность в тоннах</label></td>
				<td class="form-input">
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<input type="text" name="weight" class="text w_20" />
					</span>
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Обьем в кубических метрах</label></td>
				<td class="form-input">
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<input type="text" name="volume" class="text w_20" />
					</span>
					</td>
			</tr>
			<tr>
				<td style="70%"><label>Или укажите размеры</label></td>
				<td class="form-input">
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<input type="text" name="length" class="text w_20" />
					</span> длина(метры)<br /> 
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<input type="text" name="width" class="text w_20" />
					</span> ширина(метры)<br />
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<input type="text" name="height" class="text w_20" />
					</span> высота(метры) 
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Цена за перевозку в тенге</label></td>
				<td class="form-input">
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<input type="text" name="price" class="text w_20" />
					</span>
				</td>
			</tr>
		</table>
		<input type="submit" value="Добавить обьявление о транспорте"/>
	</form>
{/block}
