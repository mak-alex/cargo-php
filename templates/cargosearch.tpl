{extends file="index.tpl"}
{block name="title"}
	<div class="title">
		Поиск грузов
	</div>
{/block}
{block name="main"}
	<form action="index.php?page=cargosearch" method="get" id="cargo-search-filter">
		<input type="hidden" name="page" value="cargosearch"/>
		<div style="margin:10px; overflow:hidden; padding:0px;background-color:#f0f0f0">
			<div style="width:50%; float:left">
				<table><tr><th><label>Откуда</label></th></tr></table>
				<p style="padding:10px">
					Страна<br/>
					{html_options name="from_country" options=$from_country style="width:250px;"}<br/>
					Область<br/>
					{html_options name="from_region" options=$from_region style="width:250px;"}<br/>
					Город<br/>
					{html_options name="from_city" options=$from_city style="width:250px;"}
				</p>
			</div>
			<div style="width:50%; float:right">
				<table><tr><th><label>Куда</label></th></tr></table>
				<p style="padding:10px">
					Страна<br/>
					{html_options name="to_country" options=$from_country style="width:250px;"}<br/>
					Область<br/>
					{html_options name="to_region" options=$from_region style="width:250px;"}<br/>
					Город<br/>
					{html_options name="to_city" options=$from_city style="width:250px;"}
				</p>
			</div>
			<div style="clear:both"></div>
			<div style="text-align:center;">
				<p style="padding:10px">
					<table>
						<tr>
							<th><strong>Даты погрузки с:</strong></th>
							<th><strong>Даты погрузки по:</strong></th>
							<th><strong>Вес (тонна) от:</strong></th>
							<th><strong>Вес (тонна) до:</strong></th>
							<th><strong>Объем (м<sup>3</sup>) от:</strong></th>
							<th><strong>Объем (м<sup>3</sup>) до:</strong></th>
							<th><strong>Управление</strong></th>
						</tr>
						<tr id="id_1">
							<td>
								<input type="text" name="date_from" value="{$current_date}" style="width:100px;"/> 
							</td>
							<td>
								<input type="text" name="date_to" value="{$due_date}" style="width:100px;"/>
							</td>
							<td>
								<input type="text" name="weight_from" style="width:100px;"/>
							</td>
							<td>
								<input type="text" name="weight_to" style="width:100px;"/>
							</td>
							<td>
								<input type="text" name="volume_from" style="width:100px;"/>
							</td>
							<td>
								<input type="text" name="volume_to" style="width:100px;"/>
							</td>
							<td>
								<input type="submit" value="Поиск" class="button"/>
							</td>
						</tr>
					</table>
				</p>
			</div>
		</div>
	</form>
	<div id="results">
		{if isset($cargos)}
			<table id="cargo-table">
				<tr>
											<th>
												<strong>Даты погрузки и прибытия<br/>
													Откуда<img src="templates/images/arrow-right.png" style="width:10px; height:10px">Куда
												</strong>
											</th>
											<th><strong>Наименование<br/>Стоимость</strong></th>
											<th><strong>Тип транспорта<br/>кол-во транспорта</strong></th>
											<th><strong>Грузоподъемность</strong></th>
											<th><strong>Объем и габариты</strong></th>
											<th><strong>Управление</strong></th>
										</tr>
				{foreach item="cargo" key="id" from=$cargos}
					<tr class="cargo-container">
						<!--{if isset($my_id)}
							<td class="favorite"><img class="add-to-favorites" cargoid="{$id}" src="templates/img/favorite_no.png" alt=""/></td>
						{/if}-->
						<td class="delivery-dates" style="width:190px;">
							{$cargo.load_from|date_format:"%d.%m.%g"} &mdash; {$cargo.load_due|date_format:"%d.%m.%g"}
							<div class="origin-destination">
								{foreach name="originsloop" item="origin" from=$cargo.origins}
									<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
								{/foreach}
								<img src="templates/images/arrow-right.png" style="width:10px; height:10px">
								{foreach name="destinationsloop" item="destination" from=$cargo.destinations}
									<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
								{/foreach}
							</div>
							<!--{if isset($my_id) && $my_id==$cargo.account_id}<div><a href="index.php?page=mycargo&action=delete&id={$id}" class="delete">Удалить</a></div>{/if}-->
						</td>
						<td>
							
							<div class="cargo-name">Наименование: {$cargo.cargo_name}</div>
							{if isset($cargo.price)}<div class="cargo-price">Цена:{$cargo.price} тенге</div>{/if}
							<div class="cargo-name"><strong>Возможность консолидации:</strong> {if $cargo.canconsolidate=="on"}Да{else}Нет{/if}</div>
						</td>
						<td style="text-align:center">
							<div><strong>Тип транспорта:</strong>{$cargo.transport_required}</div>
							{if $cargo.vehicles_number>0}<div class="car-amount"><strong>Требуется единиц транспорта:</strong> {$cargo.vehicles_number}</div>{/if}
						</td>
						<td style="text-align:center">
							{if $cargo.weight>0}{$cargo.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
						</td>
						<td style="text-align:center">
							{if $cargo.volume>0}{$cargo.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
							{if $cargo.length>0 && $cargo.width>0 && $cargo.height>0}{$cargo.length}м x {$cargo.width}м x {$cargo.height}м м{/if}
						</td>
						<td style="width:180px;">
							<div style="text-align:right;">
									<div id="cargo-contacts-{$id}" style="text-align:right;">
                                                                  {if (strlen($cargo.contact_email)==0)}{$cargo.contact_email="не указан" }{/if}
                                                                  <a href="#" class="tooltip yellow-tooltip edit" data-html="true" data-title="Имя: {$cargo.contact_name}
                                                                  E-mail: {$cargo.contact_email}; 
                                                                  {foreach item="phone" from=$cargo.contact_phones}
                                                                  Телефон: {$phone}
                                                                  {/foreach}
                                                                  Нажмите для подробной информации"><img src="templates/images/phone.png" style="width:24px; height:24px;"></a>
                                                                  
                                                                  <a href="index.php?page=contacts&id={$id}" class="tooltip yellow-tooltip edit" data-html="true" data-title="Подробная информация
                                                                  Контактная информация
                                                                  Маршрут и расстояние
                                                                  Нажмите для подробной информации"><img src="templates/images/info.png" style="width:24px; height:24px;"></a>

									{if isset($my_id) && $my_id==$cargo.account_id}
										<a href="index.php?page=mycargo&action=delete&id={$id}" class="tooltip yellow-tooltip delete" data-html="true" data-title="Удалить данный груз"><img src="templates/images/bin.png" style="width:24px; height:24px"></a>
										<a href="index.php?page=editfreight&action=edit&id={$id}" class="tooltip yellow-tooltip edit" data-html="true" data-title="Изменить данный груз"><img src="templates/images/pencil.png" style="width:24px; height:24px"></a>
									{/if}
									</div>
						</td>
					</tr>
				{/foreach}
			</table>
		{else}
			<div style="text-align:center">К сожалению, на данном направлении свободные грузы отсутствуют. Попробуйте, пожалуйста, повторить поиск немного позднее.</div>
		{/if}
	</div>	
{/block}
