{extends file="index.tpl"}
{block name="title"}
	<div class="title">
		Поиск транспорта
	</div>
{/block}
{block name="main"}
	<script type="text/javascript">
		$(document).ready(function(){
			$("select[name='from_region']").change(function(){
				//alert("here");
				$("select[name='from_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('../getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[name='from_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='from_city']").attr("disabled",false);
			});
			$("select[name='from_country']").change(function(){
				//alert("here");
				$("select[name='from_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('../getregionoptions.php?id='+countryid, function(data) {
					$("select[name='from_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('../getcityoptions.php?id='+countryid, function(data) {
					$("select[name='from_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='from_city']").attr("disabled",false);
			});
			
			$("select[name='to_region']").change(function(){
				//alert("here");
				$("select[name='to_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('../getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[name='to_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='to_city']").attr("disabled",false);
			});
			$("select[name='to_country']").change(function(){
				//alert("here");
				$("select[name='to_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('../getregionoptions.php?id='+countryid, function(data) {
					$("select[name='to_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('../getcityoptions.php?id='+countryid, function(data) {
					$("select[name='to_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='to_city']").attr("disabled",false);
			});
			{literal}
				$( "input[name='date_from']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
				$( "input[name='date_to']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
			{/literal}
			$("#cargo-search-filter").submit(function(){
				url="index.php?"+$(this).serialize()+"&ajax";
				//alert(url);
				$("input[type='submit']").val("Ищем, подождите...");
				$.get(url,function(data){
					$("#results").html(data);
					$(".display-contacts-link").click(function(){ $("#cargo-contacts-"+$(this).attr("cargoid")).toggle(); });
					$(".add-to-favorites").click(function(){
						//do ajax action to add to favorites
						$(this).attr("src","templates/images/favorite_yes.png");
						item_id=$(this).attr("cargoid");
						$.get("mods/addtofavorites.php?myid={$my_id}&itemid="+item_id+"&type=cargo",function(data){
							// do nothing
						});
					});
				});
				$("input[type='submit']").val("Еще поиск");
				return false;
			});
		});
	</script>
	<form action="index.php?page=transportsearch" method="get" id="cargo-search-filter">
		<input type="hidden" name="page" value="transportsearch"/>
		<div style="margin:10px; overflow:hidden; padding:0px;background-color:#f0f0f0">
			<div style="width:50%; float:left">
				<table><tr><th><label>Откуда</label></th></tr></table>
				<p style="padding:10px">
					Страна<br/>
					{html_options name="from_country" options=$from_country style="width:250px;"}<br/>
					Область<br/>
					{html_options name="from_region" options=$from_region style="width:250px;"}<br/>
					Город<br/>
					{html_options name="from_city" options=$from_city style="width:250px;"}
				</p>
			</div>
			<div style="width:50%; float:right">
				<table><tr><th><label>Куда</label></th></tr></table>
				<p style="padding:10px">
					Страна<br/>
					{html_options name="to_country" options=$from_country style="width:250px;"}<br/>
					Область<br/>
					{html_options name="to_region" options=$from_region style="width:250px;"}<br/>
					Город<br/>
					{html_options name="to_city" options=$from_city style="width:250px;"}
				</p>
			</div>
			<div style="clear:both"></div>
			<div style="text-align:center;">
				<div class="title">Даты погрузки</div>
				<p style="padding:10px">
					<table>
						<tr>
							<th><strong>Даты погрузки с:</strong></th>
							<th><strong>Даты погрузки по:</strong></th>
							<th><strong>Вес (тонна) от:</strong></th>
							<th><strong>Вес (тонна) до:</strong></th>
							<th><strong>Объем (м<sup>3</sup>) от:</strong></th>
							<th><strong>Объем (м<sup>3</sup>) до:</strong></th>
							<th><strong>Управление</strong></th>
						</tr>
						<tr id="id_1">
							<td>
								<input type="text" name="date_from" value="{$current_date}" style="width:100px;"/> 
							</td>
							<td>
								<input type="text" name="date_to" value="{$due_date}" style="width:100px;"/>
							</td>
							<td>
								<input type="text" name="weight_from" style="width:100px;"/>
							</td>
							<td>
								<input type="text" name="weight_to" style="width:100px;"/>
							</td>
							<td>
								<input type="text" name="volume_from" style="width:100px;"/>
							</td>
							<td>
								<input type="text" name="volume_to" style="width:100px;"/>
							</td>
							<td>
								<input type="submit" value="Поиск" class="button"/>
							</td>
						</tr>
					</table>
				</p>
			</div>
		</div>
		
	</form>
	<div id="results">
		{if isset($transports)}
			<table id="cargo-table">
				<tr>
											<th>
												<strong>Даты погрузки и прибытия<br/>
													Откуда<img src="templates/images/arrow-right.png" style="width:10px; height:10px">Куда
												</strong>
											</th>
											<th><strong>Стоимость</strong></th>
											<th><strong>Тип транспорта<br/>Кол-во транспорта</strong></th>
											<th><strong>Грузоподъемность</strong></th>
											<th><strong>Объем и габариты</strong></th>
											<th><strong>Управление</strong></th>
										</tr>
				{foreach item="transport" key="id" from=$transports}
					<tr class="cargo-container">
						<!--{if isset($my_id)}
						<td class="favorite"><img class="add-to-favorites" cargoid="{$id}" src="templates/images/favorite_no.png" alt=""/></td>
						{/if}-->
						<td class="delivery-dates" style="width:250px;">
							{$transport.load_from|date_format:"%d.%m.%g"} &mdash; {$transport.load_due|date_format:"%d.%m.%g"}
							<div class="origin-destination">
								{foreach name="originsloop" item="origin" from=$transport.origins}
									<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
								{/foreach}
								<img src="templates/images/arrow-right.png" style="width:10px; height:10px">
								{foreach name="destinationsloop" item="destination" from=$transport.destinations}
									<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
								{/foreach}
							</div>
							<!--{if isset($my_id) && $my_id==$transport.account_id}<div><a href="index.php?page=mycargo&action=delete&id={$id}" class="delete">Удалить</a></div>{/if}-->
						</td>
						<td>
							<div class="cargo-name">{$transport.cargo_name}</div>
							{if isset($transport.price)}<div class="cargo-price">Цена:{$transport.price} тенге</div>{/if}
						</td>
						<td style="text-align:center">
							<div>{$transport.transport_required}</div>
							<table style="text-align:left;">
								<tr>
									<td><b>Марка: </b></td>
									<td>
										{if strlen($transport.brand)==0}
										0<br/>
										{else}
										{$transport.brand}<br/>
										{/if}
									</td>
								</tr>
								<tr>
									<td><b>Вид транспорта: </b></td>
									
									<td>
									{if strlen($transport.transports_modes)==0}
									0<br/>
									{else}
									{$transport.transports_modes}<br/>
									{/if}
									</td>
								</tr>
								<tr>
									<td><b>Тип транспорта: </b></td>
									<td>
									{if strlen($transport.transports_type)==0}
									0<br/>
									{else}
									{$transport.transports_type}<br/>
									{/if}
									</td>
									
								</tr>
								<tr>
									<td><b>Вместимость, машин: </b></td>
									<td>
									{if strlen($transport.capacity)==0}
									0<br/>
									{else}
									{$transport.capacity}<br/>
									{/if}
									</td>
									
								</tr>
								<tr>
									<td><b>Посадочных мест, шт.: </b></td>
									<td>
									{if strlen($transport.seats)==0}
									0<br/>
									{else}
									{$transport.seats}<br/>
									{/if}
									</td>
									
								<tr>
									<td><b>Доп.инфо: </b></td>
									<td>
									{if strlen($transport.information)==0}
									0<br/>
									{else}
									{$transport.information}<br/>
									{/if}
									</td>
									
								</tr>
								<tr>
									<td><b>Единиц транспорта в наличии:</b></td>
									<td>
									{if strlen($transport.car_amount)==0}
									0<br/>
									{else}
									{$transport.car_amount}<br/>
									{/if}
									</td>
								<tr>
									<td><b>Перевозка негабаритного груза:<b></td>
									<td>
									{if $transport.notgabarite=="0"}Нет{else}Да{/if}
									</td>
								</tr>
							</table>
						</td>
						<td style="text-align:center">
							{if $transport.weight>0}{$transport.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
						</td>
						<td style="text-align:center">
							{if $transport.volume>0}{$transport.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
							{if $transport.length>0 && $transport.width>0 && $transport.height>0}{$transport.length}м x {$transport.width}м x {$transport.height}м м{/if}
						</td>
						<td style="width:180px;">
							<div style="text-align:right;">
								<div id="cargo-contacts-{$id}" style="text-align:right;">
                                                                  {if (strlen($transport.contact_email)==0)}{$transport.contact_email="не указан" }{/if}
                                                                  <a href="#" class="tooltip yellow-tooltip edit" data-html="true" data-title="Имя: {$transport.contact_name}
                                                                  E-mail: {$transport.contact_email}; 
                                                                  {foreach item="phone" from=$transport.contact_phones}
                                                                  Телефон: {$phone}
                                                                  {/foreach}
                                                                  Нажмите для подробной информации"><img src="templates/images/phone.png" style="width:24px; height:24px;"></a>
                                                                  
                                                                  <a href="index.php?page=contacts&id={$id}" class="tooltip yellow-tooltip edit" data-html="true" data-title="Подробная информация
                                                                  Контактная информация
                                                                  Маршрут и расстояние
                                                                  Нажмите для подробной информации"><img src="templates/images/info.png" style="width:24px; height:24px;"></a>
                                                                  </div>
								{if isset($my_id) && $my_id==$transport.account_id}
									<a href="index.php?page=mycargo&action=delete&id={$id}" class="tooltip yellow-tooltip delete" data-html="true" data-title="Удалить данный груз"><img src="templates/images/bin.png" style="width:24px; height:24px"></a>
									<a href="index.php?page=editfreight&action=edit&id={$id}" class="tooltip yellow-tooltip edit" data-html="true" data-title="Изменить данный груз"><img src="templates/images/pencil.png" style="width:24px; height:24px"></a>
								{/if}
							</div>
						</td>
					</tr>
				{/foreach}
			</table>
		{else}
			<div style="text-align:center">К сожалению, на данном направлении свободный транспорт отсутствует. Попробуйте, пожалуйста, повторить поиск немного позднее.</div>
		{/if}
	</div>	
{/block}
