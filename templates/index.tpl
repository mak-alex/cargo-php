<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="keywords" content="найти перевозчика, найти груз, найти транспорт,  автоперевозки, avtoperevozki, доставка грузов, перевозки грузов, перевезти груз, рузоперевозки, попутный груз, попутный транспорт, грузовик, найти фуру, грузы в Казахстан, грузы из Казахстана, грузы по Казахстану, грузы в Россию, грузы в Москву, догруз, рефрижератор, тент, контейнеровоз, шаланда, дальнобойщик" />
		<meta name="description" content="" />
		<meta name="author" content="Mikhailenko Alexandr (AlexMAK)">
		<meta property="og:image" content="http://localhost/avtoperevozki.kz/templates/images/logo.png" />
		<meta property="og:title" content="AVTOPEREVOZKI.KZ - Логистика и транспорт для требовательных клиентов. У вас есть груз? У нас есть транспорт!!!" />
		<meta property="og:description" content="автоперевозки по Казахстану, международные автоперевозки, офисные переезды,офисные квартирные переезды," />
		<title>AVTOPEREVOZKI.KZ</title>
		<link rel="stylesheet" href="templates/css/style.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="templates/js/jquery-ui-1.10.2/themes/base/jquery-ui.css" />
		<script type="text/javascript" src="templates/js/jquery-1.9.1.min.js"></script>
		<script src="templates/js/jquery-ui-1.10.2/ui/jquery-ui.js"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
                <!-- MAPS -->
                <script src="http://api-maps.yandex.ru/2.1-stable/?load=package.full&lang=ru-RU"></script>
                <script type="text/javascript" src="templates/marsh.js"></script>
		<script type="text/javascript">
			{literal}
				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback(drawChart);
				function drawChart() {
				var data = google.visualization.arrayToDataTable([
				  ['Месяц/год', 'Запрашиваемые цены', 'Предлагаемые цены'],
			{/literal}
				  /*
				  ['2004',  1000,      400],
				  ['2005',  1170,      460],
				  ['2006',  660,       1120],
				  ['2007',  1030,      540]
				  */
				  {foreach name="chartloop" item="chart" key="load_date" from=$prices_chart}
					['{$load_date}',{$chart.cargo},{$chart.transport}]{if $smarty.foreach.chartloop.last}{else}{literal}, {/literal}{/if}
				  {/foreach}
			{literal}
				]);

				var options = {
				  title: 'Предлагаемые и запрашиваемые цены за перевозку грузов'
				};

				var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
				chart.draw(data, options);
				}
			{/literal}
		</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/js/select2.min.js"></script>



		<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
<script>
   $(document).ready(function() {
      $(".js-example-basic-single").select2();
   });
</script>
		<script type="text/javascript">
		$(document).ready(function(){
			$("select[name='from_region']").change(function(){
				//alert("here");
				$("select[name='from_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[name='from_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='from_city']").attr("disabled",false);
			});
			$("select[name='from_country']").change(function(){
				//alert("here");
				$("select[name='from_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getregionoptions.php?id='+countryid, function(data) {
					$("select[name='from_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('getcityoptions.php?id='+countryid, function(data) {
					$("select[name='from_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='from_city']").attr("disabled",false);
			});
			
			$("select[name='to_region']").change(function(){
				//alert("here");
				$("select[name='to_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[name='to_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='to_city']").attr("disabled",false);
			});
			$("select[name='to_country']").change(function(){
				//alert("here");
				$("select[name='to_city']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getregionoptions.php?id='+countryid, function(data) {
					$("select[name='to_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('getcityoptions.php?id='+countryid, function(data) {
					$("select[name='to_city']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='to_city']").attr("disabled",false);
			});
			{literal}
				$( "input[name='date_from']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
				$( "input[name='date_to']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
			{/literal}
			$("#cargo-search-filter").submit(function(){
				url="index.php?"+$(this).serialize()+"&ajax";
				//alert(url);
				$("input[type='submit']").val("Ищем, подождите...");
				$.get(url,function(data){
					$("#results").html(data);
					$(".display-contacts-link").click(function(){ $("#cargo-contacts-"+$(this).attr("cargoid")).toggle(); });
					$(".add-to-favorites").click(function(){
						//do ajax action to add to favorites
						$(this).attr("src","templates/images/favorite_yes.png");
						item_id=$(this).attr("cargoid");
						$.get("mods/addtofavorites.php?myid={$my_id}&itemid="+item_id+"&type=cargo",function(data){
							// do nothing
						});
					});
				});
				$("input[type='submit']").val("Еще");
				return false;
			});
		});
	</script>
		<script type="text/javascript">
		$(document).ready(function(){
			var temp_obj=null;
			function ajaxGetCities()
			{
				temp_obj=$(this).next();
				temp_obj.attr("disabled",true);
				countryid=$(this).val();
				$.get('getcityoptions.php?id='+countryid, function(data) {
					temp_obj.html(data);
					$("#delete").val(data);
				});
				temp_obj.attr("disabled",false);
			}
			$("#more-origin-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-from").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			$("#more-destination-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-to").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			
			$(".country-select").change(ajaxGetCities);
			function recalculateVolume()
			{
				l=$("input[name='length']").val();
				w=$("input[name='width']").val();
				h=$("input[name='height']").val();
				if(l.length==0 || w.length==0 || h.length==0) return;
				ll=parseFloat(l);
				ww=parseFloat(w);
				hh=parseFloat(h);
				$("input[name='volume']").val(ll*ww*hh);
			}
			$("input[name='length']").blur(recalculateVolume);
			$("input[name='width']").blur(recalculateVolume);
			$("input[name='height']").blur(recalculateVolume);
			{literal}
				$( "input[name='date_from']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
				$( "input[name='date_due']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
			{/literal}
			$("input[name='date_due']").change(function(){
				var today = new Date();
				var plus2months = new Date(new Date(today).setMonth(today.getMonth()+3));
				var dtarr=$(this).val().split(".");
				var date2compare=new Date(dtarr[2],dtarr[1],dtarr[0]);
				if(date2compare>plus2months)
				{
					alert("Дата не должна превышать 2-х месяцев");
					$(this).val("");  
				}
			});
		});
	</script>
	<style>
		/* mark with differing style */
		.mark {
		    font-weight: 12px;
		}
		/* nav */
		.nav {
		    list-style: none;
		}
		.nav li {
		    float: left;
		    position: relative;
		}
		.navv li {
		    float: left;
		    position: relative;
		}
		/* круглые края */
		.nav > li:first-of-type {
		    border-radius: 5px 0 0 5px;
		}
		.nav > li:last-of-type {
		    border-radius: 0 5px 5px 0;
		}
		/* блоки ссылок */
		.nav li a {
		    display: block;
		    text-decoration: none;
		    color: #fff;
		    padding: 12px;
		    border-radius: 5px;
		}
		/* выпадающее меню */
		.sub-nav {
			top:23px;
			width:205px;
		    position: absolute;
		    border-top: 15px solid transparent;
		    left: -9999px;
		    list-style:none;
		    padding:10;
		    background-color: #666;
		    margin-left:0px;
		    border-radius: 10px;
		}
		/* make submenu reappear */
		.nav li:hover .sub-nav {
		    left: 0;
		}
		/* triangle */
		.triangle {
			top:25px;
		    width: 0;
		    height: 0;
		    border-left: 10px solid transparent;
		    border-right: 10px solid transparent;
		    border-bottom: 10px solid #34495e;
		    position: absolute;
		    margin-top: 5px;
		    left: -9999px;
		}
		/* reappear */
		.nav li:hover .triangle {
		    left: 15px;
		}
		/* prevent multi line links */
		.sub-nav li {
		    white-space: nowrap;
		}
		/* закругленные края */
		.sub-nav > li:first-of-type {
		    border-radius: 5px 5px 0 0;
		}
		.sub-nav > li:last-of-type {
		    border-radius: 0 0 5px 5px;
		}
		/* верхний уровень при наведении */
		.nav > li:hover > a {
		    background: #2C3E50;
		    transition: background ease .5s;
		}
		/* ссылки подменю */
		.sub-nav a {
		    font-size: 90%;
		    margin: 3px;
		    transition: background ease .3s;
		}
		.sub-nav a:hover {
			width:175px;
		    background: #E74C3C;
		}
	</style>
	<script type="text/javascript">
		$(document).ready(function(){
			$("select[name='from_region']").change(function($){
				//alert("here");
				$("select[name='origin']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[id='origin']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[id='origin']").attr("disabled",false);
			});
			$("select[name='from_country']").change(function(){
				//alert("here");
				$("select[id='origin']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getregionoptions.php?id='+countryid, function(data) {
					$("select[name='from_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('getcityoptions.php?id='+countryid, function(data) {
					$("select[id='origin']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[id='origin']").attr("disabled",false);
			});
			
			$("select[name='to_region']").change(function(){
				//alert("here");
				$("select[name='destination']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getcityoptions.php?byregion&id='+countryid, function(data) {
					$("select[name='destination']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='destination']").attr("disabled",false);
			});
			$("select[name='to_country']").change(function(){
				//alert("here");
				$("select[name='destination']").attr("disabled",true);
				countryid=$(this).val();
				if(countryid==-1) return; // выбрано "не важно"
				//alert(countryid);
				$.get('getregionoptions.php?id='+countryid, function(data) {
					$("select[name='to_region']").html("<option value='-1' selected>любая</option>"+data);
				});
				$.get('getcityoptions.php?id='+countryid, function(data) {
					$("select[name='destination']").html("<option value='-1' selected>любой</option>"+data);
				});
				$("select[name='destination']").attr("disabled",false);
			});
	</script>
	<style>
	#map {
	        width: 100%;
	        height: 480px;
	      }
	#route-length {
		margin-top: 1em;
		right:0;
	}
.navv ul {
  margin: 0;
  padding: 0;
  list-style: none;
  position: relative;
  float: right;
  background: #eee;
  border-bottom: 1px solid #fff;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;    
}

.navv li {
  float: left;          
}

.navv #login {
  border-right: 1px solid #ddd;
  -moz-box-shadow: 1px 0 0 #fff;
  -webkit-box-shadow: 1px 0 0 #fff;
  box-shadow: 1px 0 0 #fff;  
}

.navv #login-trigger,
.navv #signup a {
  display: inline-block;
  *display: inline;
  *zoom: 1;
  height: 25px;
  line-height: 25px;
  font-weight: bold;
  padding: 0 8px;
  text-decoration: none;
  color: #444;
  text-shadow: 0 1px 0 #fff; 
}

.navv #signup a {
  -moz-border-radius: 0 3px 3px 0;
  -webkit-border-radius: 0 3px 3px 0;
  border-radius: 0 3px 3px 0;
}

.navv #login-trigger {
  -moz-border-radius: 3px 0 0 3px;
  -webkit-border-radius: 3px 0 0 3px;
  border-radius: 3px 0 0 3px;
}

.navv #login-trigger:hover,
.navv #login .active,
.navv #signup a:hover {
  background: #fff;
}

.navv #login-content {
  display: none;
  position: absolute;
  top: 24px;
  right: 0;
  z-index: 999;    
  background: #fff;
  background-image: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee));
  background-image: -webkit-linear-gradient(top, #fff, #eee);
  background-image: -moz-linear-gradient(top, #fff, #eee);
  background-image: -ms-linear-gradient(top, #fff, #eee);
  background-image: -o-linear-gradient(top, #fff, #eee);
  background-image: linear-gradient(top, #fff, #eee);  
  padding: 15px;
  -moz-box-shadow: 0 2px 2px -1px rgba(0,0,0,.9);
  -webkit-box-shadow: 0 2px 2px -1px rgba(0,0,0,.9);
  box-shadow: 0 2px 2px -1px rgba(0,0,0,.9);
  -moz-border-radius: 3px 0 3px 3px;
  -webkit-border-radius: 3px 0 3px 3px;
  border-radius: 3px 0 3px 3px;
}

.navv li #login-content {
  right: 0;
  width: 250px;  
}

/*--------------------*/

#inputs input {
  background: #f1f1f1;
  padding: 6px 5px;
  margin: 0 0 5px 0;
  width: 238px;
  border: 1px solid #ccc;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  -moz-box-shadow: 0 1px 1px #ccc inset;
  -webkit-box-shadow: 0 1px 1px #ccc inset;
  box-shadow: 0 1px 1px #ccc inset;
}

#inputs input:focus {
  background-color: #fff;
  border-color: #e8c291;
  outline: none;
  -moz-box-shadow: 0 0 0 1px #e8c291 inset;
  -webkit-box-shadow: 0 0 0 1px #e8c291 inset;
  box-shadow: 0 0 0 1px #e8c291 inset;
}

/*--------------------*/

#login #actions {
  margin: 10px 0 0 0;
}

#login #submit {		
  background-color: #d14545;
  background-image: -webkit-gradient(linear, left top, left bottom, from(#e97171), to(#d14545));
  background-image: -webkit-linear-gradient(top, #e97171, #d14545);
  background-image: -moz-linear-gradient(top, #e97171, #d14545);
  background-image: -ms-linear-gradient(top, #e97171, #d14545);
  background-image: -o-linear-gradient(top, #e97171, #d14545);
  background-image: linear-gradient(top, #e97171, #d14545);
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  text-shadow: 0 1px 0 rgba(0,0,0,.5);
  -moz-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
  -webkit-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
  box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;    
  border: 1px solid #7e1515;
  float: left;
  height: 30px;
  padding: 0;
  width: 100px;
  cursor: pointer;
  font: bold 14px Arial, Helvetica;
  color: #fff;
}

#login #submit:hover,
#login #submit:focus {		
  background-color: #e97171;
  background-image: -webkit-gradient(linear, left top, left bottom, from(#d14545), to(#e97171));
  background-image: -webkit-linear-gradient(top, #d14545, #e97171);
  background-image: -moz-linear-gradient(top, #d14545, #e97171);
  background-image: -ms-linear-gradient(top, #d14545, #e97171);
  background-image: -o-linear-gradient(top, #d14545, #e97171);
  background-image: linear-gradient(top, #d14545, #e97171);
}	

#login #submit:active {		
  outline: none;
  -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
  -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;		
}

#login #submit::-moz-focus-inner {
  border: none;
}

#login label {
  float: right;
  line-height: 30px;
}

#login label input {
  position: relative;
  top: 2px;
  right: 2px;
}

	</style>
<script>
$(document).ready(function(){
$('#login-trigger').click(function(){
	$(this).next('#login-content').slideToggle();
	$(this).toggleClass('active');					
	
	if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
		else $(this).find('span').html('&#x25BC;')
	})
});
</script>
	</head>
	<body onload="javascript:createRoute();">
		<script type="text/javascript">
			$(document).ready(function(){
				$(".display-contacts-link").click(function(){
					$("#cargo-contacts-"+$(this).attr("cargoid")).toggle();
				});
				$.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
				$(".hide-on-click").focus(function(){
					$(this).val("");
				});
			});
		</script>
		{if $error=='db'}
			<div class="warning">Ошибка БД на сайте. Разбираемся.</div>
		{/if}
		<div id="header">
			<div class="col w5 bottomlast">
				<a href="" class="logo">
					<img src="templates/images/logo.png" alt="Logo" style="border-radius: 20px;"/>
				</a>
			</div>

			<div class="col w5 last right bottomlast">

				<div style="margin-bottom:0px; text-align:center;"><img align="left" height="200px" src="http://lp.bigbiceps.kz/images/map.png"></div>
				{block name="login-box"}
						{if isset($authorized)}
						<nav class="navv">
							<ul>
								<li id="login">
									<a id="login-trigger" href="#">
										Вы вошли как <span>{$my_company_name} &#x25BC;</span>
									</a>
									<div id="login-content">
										<a href="index.php?page=account">Мой профиль</a>
									</div>                     
								</li>
								<li id="signup">
									<a href="auth.php?signout">Выйти</a>
								</li>
							</ul>
						</nav>
						{else}
						<nav class="navv">
							<ul>
								<li id="login">
									<a id="login-trigger" href="#">
										Войти <span>&#x25BC;</span>
									</a>
									<div id="login-content">
										<form id="login-form" action="auth.php" method="post">
											<fieldset id="inputs">
												<input type="text" name="email" placeholder="Введите E-mail" class="text w_20" /><br />
														<input type="password" name="password" placeholder="Введите свой пароль" class="text w_20" /><br />
											</fieldset>
											<fieldset id="actions">
												<input type="submit" id="submit" value="Войти">
												<label><input type="checkbox" checked="checked"> Запомнить меня</label>
											</fieldset>
										</form>
									</div>                     
								</li>
								<li id="signup">
									<a href="index.php?page=signup">Регистрация</a>
								</li>
							</ul>
						</nav>

						{/if}
					{/block}
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div id="wrapper">
			<div id="minwidth">
				<div id="holder">
					<div id="menu">
						<div id="left"></div>
						<div id="right"></div>
						<ul>
							<li>
								<a href="index.php"><span>Главная страница</span></a>
							</li>
							<li>
								<a href="index.php?page=cargosearch"><span>Поиск грузов</span></a>
							</li>
							<li>
								<a href="index.php?page=transportsearch"><span>Поиск транспорта</span></a>
							</li>
							<li>
								<a href="index.php?page=page&name=contacts"><span>Обратная связь и контакты</span></a>
							</li>
							<li>
								<a href="index.php?page=page&name=usefullinfo"><span>Полезная информация</span></a>
							</li>
						</ul>

						<div class="clear"></div>
					</div>
					{if isset($authorized)}
					<div id="submenu">
						<div class="modules_left">
							<div class="module buttons">
							{block name="navigation"}
							<nav>
							    <ul class="nav">
							        <li><a href="" class="dropdown_button"><small class="icon clipboard"></small><span>Меню пользователя</span></a>
							            <!--i class="triangle"></i-->
							            <ul class="sub-nav">
							                <li>
							                	<a href="index.php?page=newfreight">
													Добавить новый груз
												</a>
											</li>
											<li>
												<a href="index.php?page=mycargo">
													Мой груз
												</a>
												
											</li>
											<li>
												<a href="index.php?page=newtransport">
													Добавить новый транспорт
												</a>
												
											</li>
											<li>
												<a href="index.php?page=mytransport">
													Мой транспорт
												</a>
											</li>
							            </ul>
							        </li>
							    </ul>
							</nav>
							{/block}
								<!--a href="" class="dropdown_button"><small class="icon clipboard"></small><span>Меню пользователя</span></a>
								<div class="dropdown help_index">
									<div class="arrow"></div>
									<div class="">
										<div class="col w11">									
										<a href="index.php?page=newfreight">
											Добавить новый груз
											<span>Добавьте свой груз для отправки</span>
										</a>
										<hr/>
										<a href="index.php?page=mycargo">
											Мои грузы
											<span>Грузы которые вы добавили</span>
										</a>
										<hr/>
										<a href="index.php?page=newtransport">
											Добавить новый транспорт
											<span>Добавьте свой транспорт и заработайте денег</span>
										</a>
										<hr/>
										<a href="index.php?page=mytransport">
											Мой транспорт
											<span>Транспорт который вы добавляли</span>
										</a>
										<hr/>
										</div>
										<div class="clear"></div>
										<a class="button red right close"><small class="icon cross"></small><span>Закрыть</span></a>
										<div class="clear"></div>
									</div>
								</div-->
							</div>
						</div>
						{block name="title"}
						<div class="title">
							Последние грузы
						</div>
						{/block}
						<div class="modules_right">
						</div>
					</div>
					{/if}
					<div style="text-align:center">
					<script language="javascript" type="text/javascript" src="../js.php?id=1"></script>		
				       </div>
							<div id="table" class="help">
							<div class="col w10 last">
								{block name="main"}
								<div class="content">
									<table>
										<tr>
											<th>
												<strong>Даты погрузки и прибытия<br/>
													Откуда<img src="templates/images/arrow-right.png" style="width:10px; height:10px">Куда
												</strong>
											</th>
											<th><strong>Наименование<br/>Стоимость</strong></th>
											<th><strong>Тип транспорта<br/>кол-во транспорта</strong></th>
											<th><strong>Грузоподъемность</strong></th>
											<th><strong>Объем и габариты</strong></th>
											<th><strong>Управление</strong></th>
										</tr>
							{foreach item="cargo" key="id" from=$cargos}

							<tr id="id_2">
								<!--td class="checkbox"><input type="checkbox" name="checkbox" /></td-->
								<td style="width:380px;">
									{$cargo.load_from|date_format:"%d.%m.%G"} &mdash; {$cargo.load_due|date_format:"%d.%m.%G"}<br/>
									{foreach name="originsloop" item="origin" from=$cargo.origins}
										<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
									{/foreach}
									<img src="templates/images/arrow-right.png" style="width:10px; height:10px">
									{foreach name="destinationsloop" item="destination" from=$cargo.destinations}
										<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
									{/foreach}
								</td>
								<td>
									<div class=""><strong>Наименование:</strong> {$cargo.cargo_name}</div>
									<div class="cargo-name"><strong>Доп.инфо:</strong> {$cargo.info}</div>
									<div class="cargo-name"><strong>Возможность консолидации:</strong> {if $cargo.canconsolidate=="on"}Да{else}Нет{/if}</div>
									{if isset($cargo.price)}<div class="cargo-price">{$cargo.price} тенге</div>{/if}
								</td>
								<td>
								<div class="car-amount"><strong>Вид транспорта:</strong> {if strlen($cargo.transport_modes)==0} Не указано{else} {$cargo.transport_modes} {/if} </div>
									<div class="car-amount"><strong>Тип транспорта:</strong> {$cargo.transport_type} </div>
									<div class="car-amount"><strong>Требуется единиц транспорта:</strong>{if strlen($cargo.vehicles_number)==0}  Не указано{else} {$cargo.vehicles_number} {/if} </div>
								</td>
								<td>
									{if $cargo.weight>0}{$cargo.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
								</td>
								<td>
									{if $cargo.volume>0}{$cargo.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
									{if $cargo.length>0 && $cargo.width>0 && $cargo.height>0}{$cargo.length}м x {$cargo.width}м x {$cargo.height}м м{/if}
								</td>
								<td style="width:140px;">
                                    <div id="cargo-contacts-{$id}" style="text-align:right;">
                          
                                        <a href="#" class="tooltip yellow-tooltip edit" data-html="true" data-title="Имя: {$cargo.contact_name}
                                            E-mail: {$cargo.contact_email}
                                            {foreach item="phone" from=$cargo.contact_phones}
	                            				Телефон: {$phone}
                                            {/foreach}"><img src="templates/images/phone.png" style="width:24px; height:24px;">
                                        </a>
                                            {if isset($authorized)}                      
                                            <a href="index.php?page=contacts&id={$id}" class="tooltip yellow-tooltip edit" data-html="true" data-title="Подробная информация
                                            	Контактная информация
                                                Маршрут и расстояние
                                                Нажмите для подробной информации"><img src="templates/images/info.png" style="width:24px; height:24px;">
                                            </a>
                                            {else}
                                            <a href="index.php?page=signup" class="tooltip yellow-tooltip edit" data-html="true" data-title="Подробная информация о: 
                                            	Контактной информации
                                                Маршрутах и расстоянии
                                                Доступно только для зарегистрированных пользователей!
                                                
                                                Нажмите для регистрации"><img src="templates/images/info.png" style="width:24px; height:24px;">
                                            </a>
                                            {/if}
                                        {if isset($my_id) && $my_id==$cargo.account_id}
                                            <a href="index.php?page=mycargo&action=delete&id={$id}" class="tooltip yellow-tooltip delete" data-html="true" data-title="Удалить данный груз"><img src="templates/images/bin.png" style="width:24px; height:24px"></a>
											<a href="index.php?page=editfreight&action=edit&id={$id}" class="tooltip yellow-tooltip edit" data-html="true" data-title="Изменить данный груз"><img src="templates/images/pencil.png" style="width:24px; height:24px"></a>
                                        {/if}
                                    </div>
								</td>
							</tr>
							
							<div id="cargo-contacts-{$id}" class="cargo-contacts" style="display:none">
								<div class="cargo-contact-name">{$cargo.contact_name}</div>
								<div>{$cargo.contact_email}</div>
								<div class="cargo-phones">
									{foreach item="phone" from=$cargo.contact_phones}
										{$phone}<br/>
									{/foreach}
								</div>
							{/foreach}
						</table></div>
						<table style="width:100%">
							<tr>
								<td style="width:70%;">
									<div id="chart_div"></div>
								</td>
								<td style="width:20%"><script language="javascript" type="text/javascript" src="../js.php?id=3"></script></td>
							</tr>
						</table>
				
				<div style="color:#555555; font-size:14px; padding:50px; text-align:justify">
					<p>
					Сайт avtoperevozki.kz призван помочь участникам рынка автомобильных грузоперевозок своевременно и качественно решать вопросы по доставке грузов или их поиску. Всего лишь разместив заявку на сайте, Вы мгновенно сообщите всем остальным пользователям о своей потребности отправить Ваш груз или, наоборот, о Вашей готовности перевезти груз, а, грамотно указав основные параметры перевозки, существенно сократите потери времени на бесконечный пересказ этих параметров по телефону.
					<br/>
					Надеемся, что теперь Ваши задачи по перевозке грузов по Казахстану, России и СНГ или Европе, поиску рефрижератора, тентованного грузовика или контейнеровоза и другие будут решаться гораздо проще и быстрее.
					</p>
					<p>
					AVTOPEREVOZKI.KZ Все права защищены. Любое использование содержимого сайта без письменного согласия собственника – запрещено.
					</p>
				</div>
				<div style="text-align:center">
					<script language="javascript" type="text/javascript" src="../js.php?id=2"></script>
				</div>
						{/block}			
							</div>
							<div class="clear"></div>
							</div>
							
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
						<div id="body_footer">
							<div id="bottom_left"><div id="bottom_right"></div></div>
						</div>
					</div>
				</div>
			</div>
			<div class="clear-both"></div>
		</div>
		<div id="footer">
		    <div class="center-wrapper">
		 
		        <div class="one-fifth text-align-center">
		            <a class="" href="#"><img width="135px" src="templates/images/logo.png" alt="" style=" border-radius: 10px;"></a>
		            <hr>
		            <p><em> Логистика и транспорт для требовательных клиентов.<br />У вас есть груз? У нас есть транспорт!</em></p>
		            <hr>
		            <ul class="social-icons align-center">
		                <li><a href="#"><img src="templates/images/facebook.png" alt=""></a></li>
		                <li><a href="#"><img src="templates/images/twitter.png" alt=""></a></li>
		                <li><a href="#"><img src="templates/images/rss.png" alt=""></a></li>
		            </ul><!-- end .social-icons -->
		 
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Навигация сайта</h3>
		            <ul class="links-list">
		                <li><a href="index.php">Главная</a></li>
		                <li><a href="index.php?page=signup">Регистрация</a></li>
		                <li><a href="index.php?page=cargosearch">Поиск грузов</a></li>
		                <li><a href="index.php?page=transportsearch">Поиск транспорта</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Партнеры</h3>
		            <ul class="links-list">
		                <li><a href="http://enlab.su/">ENLAB.SU</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Реклама</h3>
		            <ul class="links-list">
		                <li><a href="http://enlab.su/">Автор CMS Cargo<br/>(avtoperevozki.kz)</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <!--div class="one-fifth-last">
		            <a href="#"><h3>Trailers</h3></a>
		            <a href="#"><h3>Photos</h3></a>
		            <a href="#"><h3>Contacts</h3></a>
		        </div--><!-- end .one-fifth-last -->
		        
			</div><!-- end .center-wrapper -->
		</div><!-- end #footer -->
		<!--<div style="font-size:12px">Copyright &copy; 2013 Avtoperevozki.kz. All rights reserved. Create CMS <a href="mailto:flashhacker1988@gmail.com">Alex M.A.K.</a></div-->
	</body>
</html>
