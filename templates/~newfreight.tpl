{extends file="index.tpl"}
{block name="title"}
	<div class="title">
		Добавить груз
	</div>
{/block}

{block name="main"}
	<script type="text/javascript">
		$(document).ready(function(){
			var temp_obj=null;
			function ajaxGetCities()
			{
				temp_obj=$(this).next();
				temp_obj.attr("disabled",true);
				countryid=$(this).val();
				$.get('getcityoptions.php?id='+countryid, function(data) {
					temp_obj.html(data);
					$("#delete").val(data);
				});
				temp_obj.attr("disabled",false);
			}
			$("#more-origin-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-from").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			$("#more-destination-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-to").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			
			$(".country-select").change(ajaxGetCities);
			function recalculateVolume()
			{
				l=$("input[name='length']").val();
				w=$("input[name='width']").val();
				h=$("input[name='height']").val();
				if(l.length==0 || w.length==0 || h.length==0) return;
				ll=parseFloat(l);
				ww=parseFloat(w);
				hh=parseFloat(h);
				$("input[name='volume']").val(ll*ww*hh);
			}
			$("input[name='length']").blur(recalculateVolume);
			$("input[name='width']").blur(recalculateVolume);
			$("input[name='height']").blur(recalculateVolume);
			{literal}
				$( "input[name='date_from']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
				$( "input[name='date_due']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
			{/literal}
			$("input[name='date_due']").change(function(){
				var today = new Date();
				var plus2months = new Date(new Date(today).setMonth(today.getMonth()+3));
				var dtarr=$(this).val().split(".");
				var date2compare=new Date(dtarr[2],dtarr[1],dtarr[0]);
				if(date2compare>plus2months)
				{
					alert("Дата не должна превышать 2-х месяцев");
					$(this).val("");  
				}
			});
		});
	</script>
	{if $error=="input"}
		<div class="warning">К сожалению, вы допустили ошибку при заполнении формы. Пожалуйста, попробуйте заполнить ее еще раз.</div>
	{/if}
	<form id="cargo-form" action="index.php?page=newreight&action=do" method="post">
		<table><tr><td>
		<div style="text-align:right">Все, что выделено <b>жирным</b> - обязательно для заполнения. Остальные поля, при желании, можете пропустить.</div>
		</tr></td></table>
		<table>
			<tr>
				<td style="70%"><label>Предполагаемая дата погрузки</label></td>
				<td class="form-input">с <input type="text" name="date_from" value="{$current_date}"/> по <input type="text" name="date_due" value="{$due_date}"/>
				{if $error=="date"}<div class="warning">Даты были введены не правильно</div>{/if}
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Пункт погрузки</label></td>
				<td class="form-input">
					<div id="country-city-from" class="location-selection">
						{html_options name="blah" class="country-select" options=$from_country selected=222 style="width:250px;"}
						{html_options name="origin[]" options=$from_city selected=21332 style="width:250px;"}
					</div>
					<a id="more-origin-link" href="javascript:void(0)">Дополнительный пункт погрузки</a>
					{if $error=="noorigin"}<div class="warning">Введите хотя бы один пункт отправки</div>{/if}
				</td>
			</tr>
			<tr class="separator">
				<td style="70%"><label>Пункт выгрузки</label></td>
				<td class="form-input">
					<div id="country-city-to" class="location-selection">
						{html_options name="blah" class="country-select" options=$from_country selected=222 style="width:250px;"}
						{html_options name="destination[]" options=$from_city selected=21332 style="width:250px;"}
					</div>
					<a id="more-destination-link" href="javascript:void(0)">Дополнительный пункт выгрузки</a>
					{if $error=="nodestination"}<div class="warning">Введите хотя бы один пункт выгрузки</div>{/if}
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Описание груза (стройматериалы, сборный груз, оборудование и т.д.)</label></td>
				<td class="form-input">
					<input type="text" name="name" value="{$cargo.cargo_name}"/>
					{if $error=="noname"}<div class="warning">Введите наименование груза пожалуйста</div>{/if}
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Необходимый тип транспорта<br />
					<a href="#modal_text" title="Справка о типах транспорта" class="modal">Справка о типах транспорта</a></label></td>
				<td class="form-input">
					{html_options name="transports" options=$transports selected=0}<br/>
					<input type="checkbox" class="checkbox" name="canconsolidate">Возможность консолидации
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Требуется единиц транспорта</label></td>
				<td class="form-input"><input type="text" name="car_amount" value="{$cargo.vehicles_number}"/></td>
			</tr>
			<tr>
				<td style="70%"><label>Вес в тоннах</label></td>
				<td class="form-input"><input type="text" name="weight" value="{$cargo.weight}"/></td>
			</tr>
			<tr>
				<td style="70%"><label>Обьем в кубических метрах</label></td>
				<td class="form-input"><input type="text" name="volume" value="{$cargo.volume}"/></td>
			</tr>
			<tr>
				<td style="70%"><label>Или укажите размеры</label></td>
				<td class="form-input">
					<input type="text" name="length" value="{$cargo.length}"/> длина(метры) <br />
					<input type="text" name="width" value="{$cargo.width}"/> ширина(метры) <br />
					<input type="text" name="height" value="{$cargo.height}"/> высота(метры)
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Примерное расстояние (км)</label></td>
				<td class="form-input"><input type="text" name="distance"/><br/><a href="http://www.ati.su/Trace/" target="_blank">расчет расстояний</a></td>
			</tr>
			<tr>
				<td style="70%"><label>Стоимость перевозки в тенге</label></td>
				<td class="form-input"><input type="text" name="price" value="{if isset($cargo.price)}{$cargo.price}{/if}"/></td>
			</tr>
			<tr>
				<td style="70%"><label>Дополнительная информация</label></td>
				<td class="form-input"><textarea name="additional_info" style="width:250px; height:50px;" value="{$cargo.volume}"></textarea></td>
			</tr>
		</table>
		<input type="submit" value="Добавить объявление о грузе"/>
	</form>
	<div id="modal_text">
	<table>
		<tr>
			<th>Вид транспорта</th>
			<th>Тип транспорта</th>
			<th>Характеристики</th>
			<th>Обязательные поля</th>
		</tr>
		<tr id="id_1">
			<td>
				<label>Автобус</label>
			</td>
			<td>
				-
			</td>
			<td>
				«Посадочных мест, шт.» _____<br />
				«Дополнительная информация» ________
			</td>
			<td>
				Нет
			</td>
		</tr>
		<tr>
			<td>
				<label>Микроавтобус</label>
			</td>
			<td>
				-
			</td>
			<td>
				«Посадочных мест» _____<br />
				«Дополнительная информация» ________
			</td>
			<td>
				Нет
			</td>
		</tr>
		<tr>
			<td>
				<b>Минивен</b>
			</td>
			<td>
				<label>Крытый</label><br />
				Рефрижератор<br />
				Изотермический фургон<br />
				Тентованный<br />
				<label>Открытый</label><br />
				Бортовой<br />
				Другой (самостоятельный ввод)

			</td>
			<td>

			</td>
			<td>
				Грузоподъемность, Объем
			</td>
		</tr>
		<tr>
			<td>
				<label>Тягач с полуприцепом</label>
			</td>
			<td>
				
				<label>Крытый</label><br />
				Рефрижератор<br />
				Изотермический фургон<br />
				Тентованный<br />
				<label>Открытый</label><br />
				Бортовой<br />
				Трал<br />
				Контейнеровоз<br />
				<labe>Самосвал</label><br />
				Цистерна<br />
				Цементовоз<br />
				Другой (самостоятельный ввод)
			    </label>
			</td>
			<td>

			</td>
			<td>
				Грузоподъемность, Объем
			</td>
		</tr>
		<tr>
			<td>
				<label>Автовоз</label>
			</td>
			<td>
			
			</td>
			<td>
				«Марка» _____________<br />
				«Вместимость, машин»_______________<br />
				«Дополнительная информация»________
			</td>
			<td>
				Нет
			</td>
		</tr>
		<tr>
			<td>
				<label>Эвакуатор</label>
			</td>
			<td>
			
			</td>
			<td>
				«Марка» _____________<br />
				«Вместимость, машин»_______________<br />
				«Дополнительная информация»________
			</td>
			<td>
				Нет
			</td>
		</tr>
		<tr>
			<td>
				<label>Автокран</label>
			</td>
			<td>
			
			</td>
			<td>
				«Марка» _____________<br />
				«Вместимость, машин»_______________<br />
				«Дополнительная информация»________
			</td>
			<td>
				Грузоподъемность
			</td>
		</tr>
		<tr>
			<td>
				<label>Экскаватор</label>
			</td>
			<td>
			
			</td>
			<td>
				«Марка» _____________<br />
				«Вместимость, машин»_______________<br />
				«Дополнительная информация»________
			</td>
			<td>
				Нет
			</td>
		</tr>
		<tr>
			<td>
				<label>Трактор</label>
			</td>
			<td>
			
			</td>
			<td>
				«Марка» _____________<br />
 				«Дополнительная информация»________
			</td>
			<td>
				Нет
			</td>
		</tr>
		<tr>
			<td>
				<label>Грузовик без прицепа</label>
			</td>
			<td>
				Крытый<br />
				Рефрижератор<br />
				Изотермический фургон<br />
				Тентованный<br />
				Открытый<br />
				Бортовой<br />
				Трал<br />
				Контейнеровоз<br />
				Самосвал<br />
				Цистерна<br />
				Ц ементовоз<br /><br />
				Другой (самостоятельный ввод)
			</td>
			<td>
				Грузоподъемность, Объем
			</td>
			<td>
				Нет
			</td>
		</tr>
		<tr>
			<td>
				<label>Манипулятор</label>
			</td>
			<td>
				
			</td>
			<td>
				«Марка» _____________<br />
 				«Дополнительная информация»________
			</td>
			<td>
				Грузоподъемность, Объем
			</td>
		</tr>
		<tr>
			<td>
				<label>Другой (самостоятельный ввод»</label>
			</td>
			<td>
				
			</td>
			<td>
				«Дополнительная информация»________<br />
				«Дополнительная информация»________<br />
				«Дополнительная информация»________
			</td>
			<td>
				Нет
			</td>
		</tr>
	</table>
	</div>
{/block}
	