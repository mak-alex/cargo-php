{if isset($cargos)}
	<table id="cargo-table">
					<tr>
											<th>
												<strong>Даты погрузки и прибытия<br/>
													Откуда<img src="templates/images/arrow-right.png" style="width:10px; height:10px">Куда
												</strong>
											</th>
											<th><strong>Наименование<br/>Стоимость</strong></th>
											<th><strong>Тип транспорта<br/>кол-во транспорта</strong></th>
											<th><strong>Грузоподъемность</strong></th>
											<th><strong>Объем и габариты</strong></th>
											<th><strong>Управление</strong></th>
										</tr>
		{foreach item="cargo" key="id" from=$cargos}
			<!--tr>
				<td colspan="6" class="origin-destination">
					{foreach name="originsloop" item="origin" from=$cargo.origins}
						<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
					{/foreach}
					&rarr;
					{foreach name="destinationsloop" item="destination" from=$cargo.destinations}
						<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
					{/foreach}
					<div style="float:right"><a href="javascript:void(0)" class="display-contacts-link" cargoid="{$id}">Отобразить контакты</a></div>
				</td>
			</tr-->
			<tr class="cargo-container">
				<!--td class="favorite">{if isset($my_id)}<img class="add-to-favorites" cargoid="{$id}" src="templates/img/favorite_no.png" alt=""/>{/if}</td-->
				<td class="delivery-dates">
					{$cargo.load_from|date_format:"%d.%m.%G"} &mdash; {$cargo.load_due|date_format:"%d.%m.%G"}
							<div class="origin-destination">
								{foreach name="originsloop" item="origin" from=$cargo.origins}
									<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
								{/foreach}
								<img src="templates/images/arrow-right.png" style="width:10px; height:10px">
								{foreach name="destinationsloop" item="destination" from=$cargo.destinations}
									<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
								{/foreach}
							</div>
				</td>
				<td>
					<div class=""><strong>Наименование:</strong> {$cargo.cargo_name}</div>
									<div class="cargo-name"><strong>Доп.инфо:</strong> {$cargo.info}</div>
									<div class="cargo-name"><strong>Возможность консолидации:</strong> {if $cargo.canconsolidate=="on"}Да{else}Нет{/if}</div>
									{if isset($cargo.price)}<div class="cargo-price">{$cargo.price} тенге</div>{/if}
				</td>
				<td style="text-align:center">
					<div class="car-amount"><strong>Вид транспорта:</strong> {if strlen($cargo.transport_modes)==0} Не указано{else} {$cargo.transport_modes} {/if} </div>
							<div class="car-amount"><strong>Тип транспорта:</strong> {$cargo.transport_type} </div>
							<div class="car-amount"><strong>Требуется единиц транспорта:</strong>{if strlen($cargo.vehicles_number)==0}  Не указано{else} {$cargo.vehicles_number} {/if} </div>
				</td>
				<td style="text-align:center">
					{if $cargo.weight>0}{$cargo.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
				</td>
				<td style="text-align:center">
					{if $cargo.volume>0}{$cargo.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
					{if $cargo.length>0 && $cargo.width>0 && $cargo.height>0}{$cargo.length}м x {$cargo.width}м x {$cargo.height}м м{/if}
				</td>
				<td style="width:180px;">
							<div style="text-align:right;">
									<div id="cargo-contacts-{$id}" style="text-align:right;">
                                                                  {if (strlen($cargo.contact_email)==0)}{$cargo.contact_email="не указан" }{/if}
                                                                  <a href="#" class="tooltip yellow-tooltip edit" data-html="true" data-title="Имя: {$cargo.contact_name}
                                                                  E-mail: {$cargo.contact_email}; 
                                                                  {foreach item="phone" from=$cargo.contact_phones}
                                                                  Телефон: {$phone}
                                                                  {/foreach}
                                                                  Нажмите для подробной информации"><img src="templates/images/phone.png" style="width:24px; height:24px;"></a>
                                                                  
                                                                  <a href="index.php?page=contacts&id={$id}" class="tooltip yellow-tooltip edit" data-html="true" data-title="Подробная информация
                                                                  Контактная информация
                                                                  Маршрут и расстояние
                                                                  Нажмите для подробной информации"><img src="templates/images/info.png" style="width:24px; height:24px;"></a>

									{if isset($my_id) && $my_id==$cargo.account_id}
										<a href="index.php?page=mycargo&action=delete&id={$id}" class="tooltip yellow-tooltip delete" data-html="true" data-title="Удалить данный груз"><img src="templates/images/bin.png" style="width:24px; height:24px"></a>
										<a href="index.php?page=editfreight&action=edit&id={$id}" class="tooltip yellow-tooltip edit" data-html="true" data-title="Изменить данный груз"><img src="templates/images/pencil.png" style="width:24px; height:24px"></a>
									{/if}
									</div>
						</td>
			</tr>
		{/foreach}
	</table>
{else}
	<div style="text-align:center">По указанным вами критериям, ничего не найдено.</div>
{/if}
