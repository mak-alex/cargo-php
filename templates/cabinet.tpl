{extends file="index.tpl"}
{block name="title"}
	<div class="title">
		Мой профиль
	</div>
{/block}
{block name="main"}
<div class="col w5">
	<div class="content">
		<div class="box">
			<div class="head"><div></div></div>
			<center><h2>Мой груз</h2></center>
			<table>
                        <tr><th><strong>ID</strong></th>
							<th style="width:90px;"><strong>Описание</strong></th>
							<th><strong>Тип транспорта<br/>Кол-во транспорта</strong></th>
							<th><strong>Грузоподъемность и Объем<br/>Габариты</strong></th>
							<th><strong>Управление</strong></th>
						</tr>
			</table>
			<div class="desc" style="height:250px;min-height:250px; overflow: auto;">				
                <table> 
                	{foreach item="cabinet" key="id" from=$cabinets}
				                        <tr>
				                            <td width="24px">
				                            	{$cabinet.id}
				                            <td>
											<div class="cargo-name"><strong>Наименование:</strong> {$cabinet.cargo_name}</div>
											<div class="cargo-name"><strong>Доп.инфо:</strong> {$cabinet.info}</div>
											<div class="cargo-price">Цена:{$cabinet.price} тенге</div>
										</td>
										<td style="text-align:center">
											<div>Тип транспорта: {$cabinet.transport_required}</div>
											<div class="car-amount">Нужно {$cabinet.vehicles_number} машин</div>
										</td>
										<td style="text-align:center">
											{$cabinet.weight}т. 	{$cabinet.volume}м<sup>3</sup>.<br/>
											{$cabinet.length}м x {$cabinet.width}м x {$cabinet.height}м м
										</td>
										<td style="width:120px;">
											<div id="cargo-contacts-{$cabinet.id}" style="text-align:right;">
						                    	<a href="../index.php?page=contacts&id={$cabinet.id}" target="_blank" class="tooltip yellow-tooltip" data-html="true" data-title="Подробная информация
						                            Маршрут
						                            Контакты
						                            Расстояние
						                            Нажмите для подробной информации"><img src="style/images/info.png" style="width:24px; height:24px;">
						                        </a>
												<a href="index.php?type=cargo&type=delete&id={$cabinet.id}" class="tooltip yellow-tooltip delete" data-html="true" data-title="Удалить данный груз ID: {$cabinet.id}"><img src="style/images/bin.png" style="width:24px; height:24px"></a>
												<a href="cargo.php?type=edit&id={$cabinet.id}" class="tooltip yellow-tooltip edit" data-html="true" data-title="Изменить данный груз ID: {$cabinet.id}"><img src="style/images/pencil.png" style="width:24px; height:24px"></a>
										</div>
										
						                                                                  
										</td>                     
				                        </tr>
                                </td>
                       </tr>
                       {/foreach}
                       </table>
			</div>
			<div class="bottom">
				<div></div>
			</div>
		</div> 
	</div>
</div> 
<div class="col w5">
	<div class="content">
		<div class="box">
			<div class="head"><div></div></div>
			<center><h2>Транспорт пользователей</h2></center>
			<table>
                        <tr><th><strong>ID</strong></th>
							<th style="width:180px;"><strong>Описание</strong></th>
							<th><strong>Тип транспорта<br/>Кол-во транспорта</strong></th>
							<th><strong>Грузоподъемность и Объем<br/>Габариты</strong></th>
							<th><strong>Управление</strong></th>
						</tr>
			</table>
			<div class="desc" style="height:250px;min-height:250px; overflow: auto;">	  
                <table>
                	{foreach item="cabinet" key="id" from=$cabinets}
                        <tr>
                            
				
				                            <td width="24px">
				                            	<div style='height:48px;'>{$id}</div>
				                            </td>
											<td>
												<div style='height:48px;'>{[#$vehicles_number]$load_from}<img src='style/images/arrow-right.png' style='width:10px; height:10px'>$load_due </div>
				                            </td>
				                            <td>
												<div style='height:48px;'>{$transport_required}<br/>{$volume}</div>
				                            </td>
											<td style="text-align:center">
												{$cabinet.weight}т. {$cabinet.volume}м<sup>3</sup>.<br/>
												{$cabinet.length}м x {$cabinet.width}м x {$cabinet.height}м м
											</td>
				                            <td width="120px">
				                            	<div style="right:0px;height:48px;"><a href="transport.php?type=delete&id={$cabinet.id}" class="tooltip yellow-tooltip" data-title="Удалить данный транспорт ID: {$cabinet.id}"><img src="style/images/bin.png" style="width:24px; height:24px"></a>
				                            		<a href="transport.php?type=edit&id={$cabinet.id}" class="tooltip yellow-tooltip account" data-title="Изменить данный транспорт ID: {$cabinet.id} (без подтверждения)"><img src="style/images/pencil.png" style="width:24px; height:24px;"></a></div>
				                            </td>                        

				</td>
			</tr>	
			{/foreach}
		</table>
			</div>
{/block}