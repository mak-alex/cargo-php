{extends file="index.tpl"}
{block name="title"}
	<div class="title">
		Мои грузы
	</div>
{/block}
{block name="main"}
	<script type="text/javascript"></script>
	<table id="cargo-table">
		<tr><th><strong>Направление и даты</strong></th>
			<th><strong>Описание</strong></th>
			<th><strong>Тип и кол-во транспорта</strong></th>
			<th><strong>Грузоподъемность</strong></th>
			<th><strong>Габариты</strong></th>
			<th><strong>Управление</strong></th>
		</tr>
		{foreach item="cargo" key="id" from=$cargos}
			<tr>
				<td colspan="5" class="origin-destination">
					
				</td>
				<td></td>
			</tr>
			<tr class="cargo-container">
				<td class="delivery-dates">
					{$cargo.load_from|date_format:"%d.%m.%g"} &mdash; {$cargo.load_due|date_format:"%d.%m.%g"}<br/>
					{foreach name="originsloop" item="origin" from=$cargo.origins}
						<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
					{/foreach}
					&rarr;
					{foreach name="destinationsloop" item="destination" from=$cargo.destinations}
						<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
					{/foreach}
				</td>
				<td>
					<div class="cargo-name"><strong>Наименование:</strong> {$cargo.cargo_name}</div>
					<div class="cargo-name"><strong>Доп.инфо:</strong> {$cargo.info}</div>
					<div class="cargo-name"><strong>Возможность консолидации:</strong> {if $cargo.canconsolidate=="on"}Да{else}Нет{/if}</div>
					{if isset($cargo.price)}<div class="cargo-price">Цена:{$cargo.price} тенге</div>{/if}
				</td>
				<td style="text-align:center">
					<div class="car-amount"><strong>Вид транспорта:</strong> {if strlen($cargo.transport_modes)==0} Не указано{else} {$cargo.transport_modes} {/if} </div>
					<div class="car-amount"><strong>Тип транспорта:</strong> {$cargo.transport_type} </div>
					<div class="car-amount"><strong>Требуется единиц транспорта:</strong>{if strlen($cargo.vehicles_number)==0}  Не указано{else} {$cargo.vehicles_number} {/if} </div>
				</td>
				<td style="text-align:center">
					{if $cargo.weight>0}{$cargo.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
				</td>
				<td style="text-align:center">
					{if $cargo.volume>0}{$cargo.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
					{if $cargo.length>0 && $cargo.width>0 && $cargo.height>0}{$cargo.length}м x {$cargo.width}м x {$cargo.height}м м{/if}
				</td>
				<td>
					<div id="cargo-contacts-{$id}" style="text-align:right;">
                        <a href="#" class="tooltip yellow-tooltip edit" data-html="true" data-title="Имя: {$cargo.contact_name}
                            E-mail: {$cargo.contact_email}; 
                            {foreach item="phone" from=$cargo.contact_phones}
                            	{if (strlen($phone)>=7)}
                            		Телефон: {$phone}
                            	{else}
                                	
                                {/if}
                            {/foreach}"><img src="templates/images/phone.png" style="width:24px; height:24px;">
                        </a>
                    	<a href="index.php?page=contacts&id={$id}" class="tooltip yellow-tooltip" data-html="true" data-title="Подробная информация
                            Маршрут
                            Контакты
                            Расстояние
                            Нажмите для подробной информации"><img src="templates/images/info.png" style="width:24px; height:24px;">
                        </a>
						<a href="index.php?page=mycargo&action=delete&id={$id}" class="tooltip yellow-tooltip delete" data-html="true" data-title="Удалить данный груз"><img src="templates/images/bin.png" style="width:24px; height:24px"></a>
						<a href="index.php?page=editfreight&action=edit&id={$id}" class="tooltip yellow-tooltip edit" data-html="true" data-title="Изменить данный груз"><img src="templates/images/pencil.png" style="width:24px; height:24px"></a>
				</div>
				
                                                                  
				</td>
			</tr>
		{/foreach}
	</table>					
{/block}
