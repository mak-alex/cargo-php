{extends file="index.tpl"}
{block name="title"}
	<div class="title">
		Подробная информация
	</div>
{/block}
{block name="main"}
    <script type="text/javascript" src="templates/marsh.js"></script>
    {foreach item="cargo" key="id" from=$cargos}
        {foreach item="origin" from=$cargo.origins}
            <table ><tr><th style="width:15%;"><strong>Начало пути</strong>: <input type="text" id="route-from" value="{$origin}" style="width:140px;"/></th>
        {/foreach}
        {foreach item="destination" from=$cargo.destinations}
            <th style="width:15%;"><strong>Конец пути</strong>: <input type="text" id="route-to" value="{$destination}" style="width:140px;"/></th>
            <th></th><th></th><th></th><th></th><th><input style="background:#999a9b;" type="submit" value="Расчитать маршрут" onclick="createRoute();" /></th></tr>
        {/foreach}
        <tr>
                                            <th>
                                                <strong>Даты погрузки и прибытия<br/>
                                                    Откуда<img src="templates/images/arrow-right.png" style="width:10px; height:10px">Куда
                                                </strong>
                                            </th>
                                            <th><strong>Наименование<br/>Стоимость</strong></th>
                                            <th><strong>Тип транспорта<br/>кол-во транспорта</strong></th>
                                            <th><strong>Грузоподъемность</strong></th>
                                            <th><strong>Объем и габариты</strong></th>
                                            <th><strong>Протяженность<br/>Время в пути<br/>Примерная скорость</strong></th>
                                            <th><strong>Управление</strong></th>
                                        </tr>
                            <tr id="id_2">
                                <!--td class="checkbox"><input type="checkbox" name="checkbox" /></td-->
                                <td style="width:380px;">
                                    {$cargo.load_from|date_format:"%d.%m.%g"} &mdash; {$cargo.load_due|date_format:"%d.%m.%g"}<br/>
                                    {foreach name="originsloop" item="origin" from=$cargo.origins}
                                        <b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
                                    {/foreach}
                                    <img src="templates/images/arrow-right.png" style="width:10px; height:10px">
                                    {foreach name="destinationsloop" item="destination" from=$cargo.destinations}
                                        <b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
                                    {/foreach}
                                </td>
                                <td>
                                    <div class=""><strong>{$cargo.cargo_name}</strong></div>
                                    {if isset($cargo.price)}<div class="cargo-price">{$cargo.price} тенге</div>{/if}
                                </td>
                                <td style="width:140px;">
                                    <div>{$cargo.transport_required}</div>
                                    {if $cargo.vehicles_number>0}<div class="car-amount">Нужно {$cargo.vehicles_number} машин</div>{/if}
                                </td>
                                <td style="width:100px;">
                                    {if $cargo.weight>0}{$cargo.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
                                </td>
                                <td style="width:240px;">
                                    {if $cargo.volume>0}{$cargo.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}<br/>
                                    {if $cargo.length>0 && $cargo.width>0 && $cargo.height>0}{$cargo.length}м x {$cargo.width}м x {$cargo.height}м м{/if}
                                </td>
                                <td style="width:140px;">
                                    <div id="route-length"></div>
                                    <div id="route-time"></div>
                                    <div id="route-speed"></div>
                                </td>
                                <td>
                                    <div id="cargo-contacts-{$id}" style="text-align:right;">
                                        {if (strlen($cargo.contact_name)==0)}{$cargo.contact_name="не указано" }{/if}
                                        {if (strlen($cargo.contact_email)==0)}{$cargo.contact_email="не указан" }{/if}
                                        <a href="#" class="tooltip yellow-tooltip edit" data-html="true" data-title="Имя: {$cargo.contact_name}
                                            E-mail: {$cargo.contact_email}
                                            {foreach item="phone" from=$cargo.contact_phones}
                                            {if (strlen($phone)>=7)}
                                                Телефон: {$phone}
                                            {else}
                                        
                                            {/if}
                                            {/foreach}"><img src="templates/images/phone.png" style="width:24px; height:24px;">
                                        </a>
                                        {if isset($my_id) && $my_id==$cargo.account_id}
                                            <a href="index.php?page=mycargo&action=delete&id={$id}" class="tooltip yellow-tooltip delete" data-html="true" data-title="Удалить данный груз"><img src="templates/images/bin.png" style="width:24px; height:24px"></a>
                                            <a href="index.php?page=editfreight&action=edit&id={$id}" class="tooltip yellow-tooltip edit" data-html="true" data-title="Изменить данный груз"><img src="templates/images/pencil.png" style="width:24px; height:24px"></a>
                                        {/if}<br />
                                        <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir"></div> 
                                    </div>
                                </td>
                            </tr>
    {/foreach}
    </table>
    <div id="map"></div>
         
{/block}
	
