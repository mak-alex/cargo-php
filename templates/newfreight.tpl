{extends file="index.tpl"}
{block name="title"}
	<div class="title">
		Новый груз
	</div>
{/block}
{block name="main"}
	<script type="text/javascript">
		$(document).ready(function(){
			var temp_obj=null;
			function ajaxGetCities()
			{
				temp_obj=$(this).next();
				temp_obj.attr("disabled",true);
				countryid=$(this).val();
				$.get('getcityoptions.php?id='+countryid, function(data) {
					temp_obj.html(data);
					$("#delete").val(data);
				});
				temp_obj.attr("disabled",false);
			}
			$("#more-origin-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-from").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			$("#more-destination-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-to").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			
			$(".country-select").change(ajaxGetCities);
			function recalculateVolume()
			{
				l=$("input[name='length']").val();
				w=$("input[name='width']").val();
				h=$("input[name='height']").val();
				if(l.length==0 || w.length==0 || h.length==0) return;
				ll=parseFloat(l);
				ww=parseFloat(w);
				hh=parseFloat(h);
				$("input[name='volume']").val(ll*ww*hh);
			}
			$("input[name='length']").blur(recalculateVolume);
			$("input[name='width']").blur(recalculateVolume);
			$("input[name='height']").blur(recalculateVolume);
			{literal}
				$( "input[name='date_from']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
				$( "input[name='date_due']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
			{/literal}
			$("input[name='date_due']").change(function(){
				var today = new Date();
				var plus2months = new Date(new Date(today).setMonth(today.getMonth()+3));
				var dtarr=$(this).val().split(".");
				var date2compare=new Date(dtarr[2],dtarr[1],dtarr[0]);
				if(date2compare>plus2months)
				{
					alert("Дата не должна превышать 2-х месяцев");
					$(this).val("");  
				}
			});
		});
	</script>
	{if $error=="input"}
		<div class="warning">К сожалению, вы допустили ошибку при заполнении формы. Пожалуйста, попробуйте заполнить ее еще раз.</div>
	{/if}
	<form id="cargo-form" action="index.php?page=newfreight&action=do" method="post">
		<table><tr><td>
		<div style="text-align:right">Все, что выделено <b>жирным</b> - обязательно для заполнения. Остальные поля, при желании, можете пропустить.</div>
		</tr></td></table><table>
			<tr>
				<td class="form-label required"><label>Предполагаемая дата погрузки</label></td>
				<td class="form-input">с <input type="text" name="date_from" value="{$current_date}" style="width:250px" /> по <input type="text" name="date_due" value="{$due_date}" style="width:250px;"/>
				{if $error=="date"}<div class="warning">Даты были введены не правильно</div>{/if}
				</td>
			</tr>
			<tr>
				<td class="form-label required"><label>Пункт погрузки</label></td>
				<td class="form-input">
					<div id="country-city-from" class="location-selection">
						<!--{html_options name="blah" class="country-select" options=$from_country  style="width:250px"}-->
                                                {html_options name="blah" class="country-select" options=$from_country  style="width:250px"}
					        {html_options name="origin[]" options=$from_city  style="width:250px" class="js-example-basic-single"}	
					</div>
					<a id="more-origin-link" href="javascript:void(0)">Дополнительный пункт погрузки</a>

					{if $error=="noorigin"}<div class="warning">Введите хотя бы один пункт отправки</div>{/if}
				</td>
			</tr>
			<tr class="separator">
				<td class="form-label required"><label>Пункт выгрузки</label></td>
				<td class="form-input">
					<div id="country-city-to" class="location-selection">	
						{html_options name="blah" class="country-select" options=$from_country  style="width:250px"}
						{html_options name="destination[]" options=$from_city style="width:250px" class="js-example-basic-single"}
					</div>
					<a id="more-destination-link" href="javascript:void(0)">Дополнительный пункт выгрузки</a>
					{if $error=="nodestination"}<div class="warning">Введите хотя бы один пункт выгрузки</div>{/if}
				</td>
			</tr>
			<tr>
			<tr>
				<td class="form-label required"><label>Описание груза<br/>(стройматериалы, сборный груз, оборудование и т.д.)</label></td>
				<td class="form-input">
					<input type="text" name="name" style="width:250px" maxlength="20"/>
					{if $error=="noname"}<div class="warning">Введите наименование груза пожалуйста</div>{/if}
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Имеющийся вид транспорта</label></td>

				<td class="form-input">
						{html_options id="transports_modes" name="transports_modes" options=$transport_modes selected=0 style="width:250px;" onchange="moreopt()"}
				<br />
					<script type="text/javascript">
						function moreopt(){
						    var e = document.getElementById("transports_modes");
							var strUser = e.options[e.selectedIndex].value;
						    if (strUser==14){
						        document.getElementById('more-opt').innerHTML = ('\
				<br/><label>Введите вручную:</label>\
				<td class="form-input">\
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 20 символов">\
						<input type="text" name="transports_modes_manual" placeholder="Не указано" class="text w_20"  maxlength="20"/>\
					</span>\
				</td>\
						        	') ;
						    }
						}
						</script>
						<div id="more-opt"></div>
				</td>
			</tr>
			<tr>
				<td style="70%"><label>Имеющийся тип транспорта</label></td>
				<td class="form-input">
				{html_options id="transports_types" name="transports_types" options=$transports selected=0 style="width:250px;text-align:left" onchange="moreoptt()"}
				<br />
				<input type="checkbox" class="checkbox_new" id="checkbox_new" name="canconsolidate">
					<label for="checkbox_new">Возможность консолидации</label>
				<script type="text/javascript">
						function moreoptt(){
						    var e = document.getElementById("transports_types");
							var strUser = e.options[e.selectedIndex].value;
						    if (strUser==14){
						        document.getElementById('more-optt').innerHTML = ('\
				<br/><label>Введите вручную:</label>\
				<td class="form-input">\
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 20 символов">\
						<input type="text" name="transports_types_manual" placeholder="Не указано" class="text w_20"  maxlength="20"/>\
					</span>\
				</td>\
						        	') ;
						    }
						}
						</script>
						<div id="more-optt"></div>
				</td>
			</tr>
			
			<tr>
				<td class="form-label"><label>Требуется единиц транспорта</label></td>
				<td class="form-input">
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<input type="text" name="car_amount" style="width:250px" value="1" maxlength="6"/>
					</span>
				</td>
			</tr>
			<tr>
				<td class="form-label"><label>Грузоподъемность в тоннах</label></td>
				<td class="form-input">
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов. Если вам нужно указать меньше тонны, пример: 0.5">
						<input type="text" name="weight" style="width:250px" maxlength="6"/>
					</span>
				</td>
			</tr>
			<tr>
				<td class="form-label"><label>Объем кузова в кубических метрах</label></td>
				<td class="form-input"><input type="text" name="volume" style="width:250px" maxlength="6"/></td>
			</tr>
			<tr>
				<td class="form-label"><label>Или укажите размеры кузова</label></td>
				<td class="form-input">
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<label>длина(метры)</label>
						<input type="text" name="length" maxlength="6"/>
					</span><br/>
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<label>ширина(метры)</label>
						<input type="text" name="width" maxlength="6"/>
					</span><br/>
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов" >
						<label>высота(метры) </label>
						<input type="text" name="height" maxlength="6"/>
					</span> 
				</td>
			</tr>
			<!--tr>
				<td class="form-label">Примерное расстояние (км)</td>
				<td class="form-input"><input type="text" name="distance"/><br/><a href="http://www.ati.su/Trace/" target="_blank">расчет расстояний</a></td>
			</tr-->
			<tr>
				<td class="form-label"><label>Стоимость перевозки в тенге</label></td>
				<td class="form-input"><input type="text" name="price" style="width:250px" maxlength="10"/></td>
			</tr>
			<tr>
				<td class="form-label"><label>Дополнительная информация</label></td>
				<td class="form-input"><textarea name="additional_info" style="width:250px; height:50px;" maxlength="40"></textarea></td>
			</tr>
		</table>
			<input type="button" class="btn btn-default" onClick="history.go(-1)" value="Назад"/>&nbsp;&nbsp;&nbsp;<input type="submit" value="Добавить обьявление о грузе"/>
	</form>

	
{/block}
