# Trucking website
The engine is designed to PHP5 + CSS3 + JQuery + HTML5

* Functional working client side:
	* Add load
	* Add transport
	* my load
	* My transport
	* Search trucks with filters
	* Transport search with filters
![ScreenShot](https://bytebucket.org/enlab/cargo-php/raw/55f12102584696b3dd5a6a033f8130d5a91bd129/screenshots/gruzy.kz.png?token=6f919f378b74f4902580328ee7bc0a017cd728de)
* Functional working admin's part:
	* Change load any user
	* Change the transport of any user
	* User statistics (sorting goods and free transport)
	* Database backups

# Installing the engine to the server
* git clone git@bitbucket.org:enlab/cargo-php.git /var/www/avtoperevozki
	*  mysql -u(name_user) -p(user_pass)
        * CREATE DATABASE name_database;
        * mysql  -u(name_user) -p(user_pass) name_database < /var/www/avtoperevozki/backup/desiredl.sql
	* vim /var/www/avtoperevozki/conf.php
	* Change the *name*, *password*, and *database name* to their values
	* The End! Open to browser http://localhost/avtoperevozki/
	* *Login admin*: shmadmin *Pass*: Qq123456
![ScreenShot](https://bytebucket.org/enlab/cargo-php/raw/c8b17da8bd67b74bae6552c93c4fe96c5ffab3db/screenshots/gruzy.kz.admin.png?token=c798f337f1d9bcd157f92aaa979c61b7978d7632)
