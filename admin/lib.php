<?php

function dateToDB($date)
{
	list($d,$m,$y)=explode(".",$date);
	if(!checkdate($m,$d,$y)) return false;
	return "$y-$m-$d";
}

function dbToDate($date)
{
	list($y,$m,$d)=explode("-",$date);
	return "$d.$m.$y";
}

?>