<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once "../config.php";

/** 
 * Êëàññ äëÿ àâòîðèçàöèè
 * @author äèçàéí ñòóäèÿ ox2.ru 
 */ 
class AuthClass {
    private $_login = "shmadmin"; //Óñòàíàâëèâàåì ëîãèí
    private $_password = "Qq123456"; //Óñòàíàâëèâàåì ïàðîëü

    /**
     * Ïðîâåðÿåò, àâòîðèçîâàí ïîëüçîâàòåëü èëè íåò
     * Âîçâðàùàåò true åñëè àâòîðèçîâàí, èíà÷å false
     * @return boolean 
     */
    public function isAuth() {
        if (isset($_SESSION["is_auth"])) { //Åñëè ñåññèÿ ñóùåñòâóåò
            return $_SESSION["is_auth"]; //Âîçâðàùàåì çíà÷åíèå ïåðåìåííîé ñåññèè is_auth (õðàíèò true åñëè àâòîðèçîâàí, false åñëè íå àâòîðèçîâàí)
        }
        else return false; //Ïîëüçîâàòåëü íå àâòîðèçîâàí, ò.ê. ïåðåìåííàÿ is_auth íå ñîçäàíà
    }
    
    /**
     * Àâòîðèçàöèÿ ïîëüçîâàòåëÿ
     * @param string $login
     * @param string $passwors 
     */
    public function auth($login, $passwors) {
        if ($login == $this->_login && $passwors == $this->_password) { //Åñëè ëîãèí è ïàðîëü ââåäåíû ïðàâèëüíî
            $_SESSION["is_auth"] = true; //Äåëàåì ïîëüçîâàòåëÿ àâòîðèçîâàííûì
            $_SESSION["login"] = $login; //Çàïèñûâàåì â ñåññèþ ëîãèí ïîëüçîâàòåëÿ
            return true;
        }
        else { //Ëîãèí è ïàðîëü íå ïîäîøåë
            $_SESSION["is_auth"] = false;
            return false; 
        }
    }
    
    /**
     * Ìåòîä âîçâðàùàåò ëîãèí àâòîðèçîâàííîãî ïîëüçîâàòåëÿ 
     */
    public function getLogin() {
        if ($this->isAuth()) { //Åñëè ïîëüçîâàòåëü àâòîðèçîâàí
            return $_SESSION["login"]; //Âîçâðàùàåì ëîãèí, êîòîðûé çàïèñàí â ñåññèþ
        }
    }
    
    
    public function out() {
        $_SESSION = array(); //Î÷èùàåì ñåññèþ
        session_destroy(); //Óíè÷òîæàåì
    }
}

$auth = new AuthClass();

if (isset($_POST["login"]) && isset($_POST["password"])) { //Åñëè ëîãèí è ïàðîëü áûëè îòïðàâëåíû
    if (!$auth->auth($_POST["login"], $_POST["password"])) { //Åñëè ëîãèí è ïàðîëü ââåäåí íå ïðàâèëüíî
        echo "<h2 style=\"color:red;\">Ëîãèí è ïàðîëü ââåäåí íå ïðàâèëüíî!</h2>";
    }
}

if (isset($_GET["is_exit"])) { //Åñëè íàæàòà êíîïêà âûõîäà
    if ($_GET["is_exit"] == 1) {
        $auth->out(); //Âûõîäèì
        header("Location: ?is_exit=0"); //Ðåäèðåêò ïîñëå âûõîäà
    }
}

function dateToDB($date)
{
	list($d,$m,$y)=explode(".",$date);
	if(!checkdate($m,$d,$y)) return false;
	return "$y-$m-$d";
}

function dbToDate($date)
{
	list($y,$m,$d)=explode("-",$date);
	return "$d.$m.$y";
}

if ($auth->isAuth()) { // Åñëè ïîëüçîâàòåëü àâòîðèçîâàí, ïðèâåòñòâóåì:  

if(!isset($_GET['id'])) exit();

if (isset($_GET['action']) && $_GET['action']=="save" && isset($_GET['id'])) {
	$id=intval($_GET['id']);
	$transport_type=intval($_POST['transports']);
	$car_amount=intval($_POST['car_amount']);
	$weight=floatval($_POST['weight']);
	$volume=floatval($_POST['volume']);
	$length=floatval($_POST['length']);
	$width=floatval($_POST['width']);
	$height=floatval($_POST['height']);
	$price=floatval($_POST['price']);
	
	
	$origin_raw=$_POST['origin'];
	foreach($origin_raw as $o)
	{
		if(intval($o)>0) $origin[]=intval($o);
	}
	if(count($origin)==0)
	{
		header("Location:cargo.php?page=editfreight&error=noorigin");
		echo "<H1> EEROR </H1>";
		exit();
	}
	$destination_raw=$_POST['destination'];
	foreach($destination_raw as $d)
	{
		if(intval($d)>0) $destination[]=intval($d);
	}
	

	if(count($destination)==0)
	{
		header("Location:cargo.php?page=editfreight&error=nodestination");
		exit();
	}

	if(!($date_from=dateToDB($_POST['date_from'])))
	{
		header("Location:cargo.php?page=editfreight&error=date");
		exit();
	}
	if(!($date_due=dateToDB($_POST['date_due'])))
	{
		header("Location:cargo.php?page=editfreight&error=date");
		exit();
	}
	$name=htmlspecialchars(trim($_POST['cargo_name']),ENT_QUOTES);
	if(strlen($name)==0)
	{
		header("Location:cargo.php?page=editfreight&error=noname");
		exit();
	}
	$additional_info=htmlspecialchars(trim($_POST['additional_info']),ENT_QUOTES);
	if(strlen($name)==0)
	{
		header("Location:cargo.php?page=editfreight&error=additional_info");
		exit();
	}
	if($db->query("UPDATE cargo SET  cargo_name='$name', additional_info='$additional_info', transport_required='$transport_type', weight='$weight', volume='$volume', vehicles_number='$car_amount', length='$length', width='$width', height='$height', price='$price' WHERE id='$id'")) 
	{
    	$db->rollback();
	  	header("Location:cargo.php");
	  	exit();
	}

	$freight_id=$db->insertId();
	foreach($origin as $o)
	{
		if(!$db->query("UDPATE `cargo_origins` SET (`cargo_id`,`origin`) VALUES('$freight_id','$o') WHERE id='$id'"))
		{
			$db->rollback();
			header("Location:cargo.php?page=editfreight&error=db&2");
			exit();
		}
	}
	foreach($destination as $d)
	{
		if(!$db->query("UDPATE `cargo_destinations` SET (`cargo_id`,`destination`) VALUES('$freight_id','$d') WHERE id='$id'"))
		{
			$db->rollback();
			header("Location:cargo.php?page=editfreight&error=db&1");
			exit();
		}
	}
	/*$freight_id=$db->insertId();
	foreach($origin as $o)
	{
		if($db->query("UPDATE `transport_origins` SET `transport_id`='$freight_id',`origin`='$o' WHERE `id`='$id'"))
		{
			$db->rollback();
			header("Location:cargo.php?page=editfreight&error=db&2");
			exit();
		}
	}
	foreach($destination as $d)
	{
		if($db->query("UPDATE `transport_destinations` SET `transport_id`='$freight_id',`destination`='$d' WHERE `id`='$id'"))
		{
			$db->rollback();
			header("Location:cargo.php?page=editfreight&error=db&1");
			exit();
		}
	}*/
	$db->commit();
	//header("Location:cargo.php");
}		// Get countries from database
}

?>
