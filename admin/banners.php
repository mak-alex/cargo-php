<?php
$digiAdsPath = '../ads.dat';
require dirname(__FILE__).'/ads.inc.php';

///////////////////////////////////////
// Don't Edit Anything Below This Line!
///////////////////////////////////////
if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = '';
}
switch ($_REQUEST['action']) {
    case 'config':
        config();
        break;
    case 'list':
        view();
        break;
    case 'edit':
        edit();
        break;
    case 'add':
        add();
        break;
    case 'codegen':
        codegen();
        break;
    case 'logout':
        logout();
        break;
    default:
        menu();
        break;
}
function dateselect($name, $date)
{
    global $digiAdsTime;
    $month = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
    if($date == '99999999') {
        $date = $digiAdsTime;
    }
    $m = date('n', $date);
    $d = date('j', $date);
    $y = date('Y', $date);
    $output = '<select name="' .$name. '_month">';
    for ($i = 0; $i < 12; $i++) {
        $j = $i + 1;
        $s = '';
        if ($j == $m) {
            $s = 'selected="selected"';
        }
        $output .= "<option $s value=\"$j\">$month[$i]</option>";
    }
    $output .= '</select> <select name="' .$name. '_day">';
    for ($i = 1; $i < 32; $i++) {
        $s = '';
        if ($i == $d) {
            $s = 'selected="selected"';
        }
        $output .= "<option $s value=\"$i\">$i</option>";
    }
    $output .= '</select> <select name="' .$name. '_year">';
    for ($i = 2001; $i < 2021; $i++) {
        $s = '';
        if ($i == $y) {
            $s = 'selected="selected"';
        }
        $output .= "<option $s value=\"$i\">$i</option>";
    }
    $output .= '</select>';
    return $output;
}
function head($title)
{
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="keywords" content="найти перевозчика, найти груз, найти транспорт,  автоперевозки, avtoperevozki, доставка грузов, перевозки грузов, перевезти груз, рузоперевозки, попутный груз, попутный транспорт, грузовик, найти фуру, грузы в Казахстан, грузы из Казахстана, грузы по Казахстану, грузы в Россию, грузы в Москву, догруз, рефрижератор, тент, контейнеровоз, шаланда, дальнобойщик" />
		<meta name="description" content="" />
		<meta name="author" content="Mikhailenko Alexandr (AlexMAK)">
		<title>AVTOPEREVOZKI.KZ</title>
		<link rel="stylesheet" href="style/css/style.css" type="text/css" media="screen" charset="utf-8" />
		<script src="style/js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script src="style/js/global.js" type="text/javascript" charset="utf-8"></script>
		<script src="style/js/modal.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script></head><body>

		<div id="header">
			<div class="col w5 bottomlast">
				<a href="" class="logo">
					<!--img src="style/images/logo.png" alt="Logo" style="border-radius: 20px;"/-->
				</a>
			</div>

			<div class="col w5 last right bottomlast">
				<div style="margin-bottom:; right:250px;"><!--img height="240px" src="http://lp.bigbiceps.kz/images/map.png"--></div>
							<p class="last">Вы вошли как <span class="strong">shmadmin</span><br/>
							<a href="../index.php">Вернуться на сайт</a><br/>
							<a href="auth.php?signout">Выйти</a></p>
			</div>
			<div class="clear"></div>
		</div>

		<div id="wrapper">
			<div id="minwidth">
				<div id="holder">
					<div id="menu">
						<div id="left"></div>
						<div id="right"></div>
						<ul>
							<li>
								<a href="index.php"><span>Главная страница</span></a>
							</li>
							<li>
								<a href="cargo.php"><span>Грузы сайта</span></a>
							</li>
							<li>
								<a href="transport.php"><span>Транспорт сайта</span></a>
							</li>
							<li>
								<a href="statistic.php"><span>Статистика пользователей</span></a>
							</li>
							<li>
				                <a href="backupSQL.php"><span>Резервное копирование</span></a>
				            </li>
							<li>
								<a href="banners.php"><span>Баннеры</span></a>
							</li>
						</ul>
						<div class="clear"></div>
					</div>
					<div id="submenu">
						<div class="modules_left">
							<div class="module buttons">
								<a href="" class="dropdown_button"><small class="icon clipboard"></small><span>Меню баннерной системы</span></a>
								<div class="dropdown help_index">
									<div class="arrow"></div>
									<div class="content">
										<div class="col w11">									
                                             <a href="banners.php?action=config">
												Конфигурация
												<span></span> 
                                             </a><hr/>
                                             <a href="banners.php?action=list">
												Список баннеров
											 </a><hr/>
                                             <a href="banners.php?action=add">
												Добавить баннер
											 </a><hr/>
                                             <a href="banners.php?action=codegen">
												Сгенерировать код
											 </a><hr/>
										</div>
										<div class="clear"></div>
										<a class="button red right close"><small class="icon cross"></small><span>Закрыть</span></a>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="title">
							'.$title.'
						</div></div>
						<div class="modules_right">
						</div>
					</div>
					<div style="text-align:center">
				       </div>
							<div id="table" class="help">
							<div class="col w10 last">
								<div class="content">
<div align="center"><table align="center" style="width:40%;" cellspacing="2" cellpadding="2"><tr><td>';
   // echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /><title>mak-ads - admin - ' .$title. '</title><style type="text/css">body, td{font-family:verdana,helvetica,arial,sans-serif;font-size:12px;color:#000000;background-color:#ffffff;}b{font-weight:bold;}h1{font-size:2.5em;}.smalltext{font-size:11px;}.error{color:#ff0000;}</style></head><body><div align="center"><h1>digi-ads</h1><b>' .$title. '</b></div><br /><br /><table align="center" width="550" border="0" cellspacing="2" cellpadding="2"><tr><td align="left">';
}
function foot()
{
   // echo '</td></tr></table><br /><br /><div align="center"><hr width="550"><span class="smalltext"><a href="banners.php?action=config">Конфигурация</a> | <a href="banners.php?action=list">Список баннеров</a> | <a href="banners.php?action=add">Добавить баннер</a> | <a href="banners.php?action=codegen">Сгенерировать код</a> <p>mak-ads 1.2 &copy; 2013-2014 <a href="mailto:flashhacker1988@gmail.com">Alexander M.A.K.</a></span></div></body></html>';
		echo '</td></tr></table><div id="footer">
		    <div class="center-wrapper">
		 
		        <div class="one-fifth text-align-center">
		            <a class="" href="#"><img width="135px" src="style/images/logo.png" alt="" style=" border-radius: 10px;"></a>
		            <hr>
		            <p><em> Логистика и транспорт для требовательных клиентов.<br />У вас есть груз? У нас есть транспорт!</em></p>
		            <hr>
		            <ul class="social-icons align-center">
		                <li><a href="#"><img src="style/images/facebook.png" alt=""></a></li>
		                <li><a href="#"><img src="style/images/twitter.png" alt=""></a></li>
		                <li><a href="#"><img src="style/images/rss.png" alt=""></a></li>
		            </ul><!-- end .social-icons -->
		 
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Навигация сайта</h3>
		            <ul class="links-list">
		                <li><a href="#">Главная</a></li>
		                <li><a href="#">Регистрация</a></li>
		                <li><a href="#">Поиск грузов</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Партнеры</h3>
		            <ul class="links-list">
		                <li><a href="#">ENLAB.SU</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Реклама</h3>
		            <ul class="links-list">
		                <li><a href="http://enlab.su/">Социальная барахолка</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <!--div class="one-fifth-last">';
}
function menu()
{
    head('Main Menu');
    echo 'If this is your first visit to the control panel, please choose configuration; otherwise, please choose from the following options:<ul><li><a href="banners.php?action=config">Конфигурация</a></li><li><a href="banners.php?action=list">Список баннеров</a></li><li><a href="banners.php?action=add">Добавить баннер</a></li><li><a href="banners.php?action=codegen">Сгенерировать код</a></li></ul>';
    foot();
    exit;
}
function config()
{
    global $digiAds;
    if (isset($_POST['save'])) {

        $digiAds['click_url'] = trim($_POST['click_url']);
        $digiAds['js_url'] = trim($_POST['js_url']);
        $digiAds['target'] = trim($_POST['target']);
        $digiAds['border'] = $_POST['border'];
        writeads();
        menu();
    } else if (isset($_POST['cancel'])) {
        menu();
    } else {
        head('Конфигурация');
        echo 'You can change any of the following properties. It is recommended that you do not edit ads.dat directly.<hr width="550" /><form method="post" action="banners.php"><input type="hidden" name="action" value="config" /><li><b>Click URL</b></li><br /><br />This is the exact URL to the click.php script. This is where the user is taken to when they click on an ad. It then logs the click and forwards them to the appropriate web site.<br /><br />Click URL: <input type="text" name="click_url" value="' .$digiAds['click_url']. '" /><hr width="550" /><li><b>JavaScript URL</b></li><br /><br />This is the exact URL to the js.php script. This is used by the code generator to generate JavaScript invocation code.<br /><br />JavaScript URL: <input type="text" name="js_url" value="' .$digiAds['js_url']. '" /><hr width="550" /><li><b>Target</b></li><br /><br />This lets you specify the target attribute of all links. To open ads in a new window set the target value to &quot;_blank&quot; (without the quotes). If you do not want or need a target, leave this field empty.<br /><br />Target: <input type="text" name="target" value="' .$digiAds['target']. '" /><hr width="550" /><li><b>Border</b></li><p>This sets the amount of border, in pixels, around an ad. Set this to 0 for no border.<br /><br />Border: <select name="border" style="width:250px;">';
        for ($i = 0; $i <= 5; $i++) {
            if ($digiAds['border'] == $i) {
                $s = ' selected="selected"';
            } else {
                $s = '';
            }
            echo "<option value=\"$i\"$s>$i</option>";
        }
        echo '</select><br /><br /><input type="submit" name="save" value="Сохранить" /> <input type="submit" name="cancel" value="Отменить" />';
        foot();
    }
}
function view()
{
    global $ads;
    head('All Ads');
    echo '<table border="1" cellspacing="0" cellpadding="1"><tr><td nowrap="nowrap"><span class="smalltext"><b>Наименование (ID):</b></span></td><td><span class="smalltext"><b>Ссылка на сайт:</b></span></td><td><span class="smalltext"><b>Ссылка на изображение:</b></span></td><td><span class="smalltext"><b>Включено:</b></span></td><td><span class="smalltext"><b>Вес:</b></span></td><td><span class="smalltext"><b>Истекает:</b></span></td><td><span class="smalltext"><b>Remaining:</b></span></td><td><span class="smalltext"><b>Показов:</b></span></td><td><span class="smalltext"><b>C/T:</b></span></td></tr>';
    foreach ($ads as $ad) {
        $data = explode('||', $ad);
        $enabled = $data[1] ? 'Да' : '<span class="error">Нет</span>';
        if($data[3] == '99999999') {
            $expires = 'Никогда';
        } else {
            $expires = date('m/d/y', $data[3]);
        }
        if ($data[4] == -1) {
            $remaining = 'Неограниченный';
        } else {
            $remaining = $data[4];
        }
        if (strlen($data[9]) > 25) {
            $linkUrl = substr($data[9], 0, 15). '...' .substr($data[9], -7);
        } else {
            $linkUrl = $data[9];
        }
        if (strlen($data[10]) > 25) {
            $imageUrl = substr($data[10], 0, 15). '...' .substr($data[10], -7);
        } else {
            $imageUrl = $data[10];
        }
        echo "<tr><td nowrap=\"nowrap\"><span class=\"smalltext\"><a href=\"banners.php?action=edit&id=$data[0]\" title=\"Edit Ad\">$data[11] ($data[0])</a></span></td><td nowrap=\"nowrap\"><span class=\"smalltext\"><a href=\"$data[9]\">$linkUrl</a></span></td><td nowrap=\"nowrap\"><span class=\"smalltext\"><a href=\"$data[10]\">$imageUrl</a></span></td><td><span class=\"smalltext\">$enabled</span></td><td><span class=\"smalltext\">$data[2]</span></td><td><span class=\"smalltext\">$expires</span></td><td><span class=\"smalltext\">$remaining</span></td><td><span class=\"smalltext\">$data[5]</span></td><td><span class=\"smalltext\">$data[6]</span></td></tr>";
    }
    echo '</table>';
    foot();
}
function edit()
{
    global $ads;
    if (!isset($_REQUEST['id'])) {
        die('Нет объявлений ID не указано');
    }
    if (isset($_POST['save'])) {
        for ($i = 0; $i < count($ads); $i++) {
            if(@ereg('^' .$_POST['id']. '\|\|', $ads[$i])) {
                $data = explode('||', $ads[$i]);
                if (isset($_POST['ad_en']) && $_POST['ad_en'] == 1) {
                    $data[1] = 1;
                } else {
                    $data[1] = 0;
                }
                if (isset($_POST['ad_reset']) && $_POST['ad_reset'] == 1) {
                    $data[5] = 0;
                    $data[6] = 0;
                }
                if (isset($_POST['ad_noexpires']) && $_POST['ad_noexpires'] == 1) {
                    $data[3] = '99999999';
                } else {
                    $data[3] = mktime(0, 0, 0, $_POST['ad_expires_month'], $_POST['ad_expires_day'], $_POST['ad_expires_year']);
                }
                $data[2] = $_POST['ad_weight'];
                $data[4] = $_POST['ad_remain'];
                $data[7] = $_POST['ad_width'];
                $data[8] = $_POST['ad_height'];
                $data[9] = $_POST['ad_link'];
                $data[10] = $_POST['ad_image'];
                $data[11] = $_POST['ad_name'];
                $ads[$i] = join('||', $data);
                break;
            }
        }
        writeads();
        menu();
    } else if (isset($_POST['delete'])) {
        if (!isset($_POST['confirm_delete']) || $_POST['confirm_delete'] != 1) {
            die('Вы не подтвердили удаление. <a href="javascript:window.history.go(-1);">[Back]</a>');
        }
        foreach ($ads as $ad) {
            if(!@ereg('^' .$_POST['id']. '\|\|', $ad)) {
                $nads[] = $ad;
            }
        }
        $ads = $nads;
        writeads();
        menu();
    } else if (isset($_POST['cancel'])) {
        menu();
    } else {
        foreach ($ads as $ad) {
            if(@ereg('^' .$_GET['id']. '\|\|', $ad)) {
                $data = explode('||', $ad);
                break;
            }
        }
        if (!isset($data)) {
            die('Ad ID ' .$_GET['id']. ' was not found');
        }
        if ($data[1] == 1) {
            $isen = 'checked="checked"';
        } else {
            $isen = '';
        }
        $expires = dateselect('ad_expires', $data[3]);
        if ($data[3] == '99999999') {
            $noexpires = 'checked="checked"';
        } else {
            $noexpires = '';
        }
        head('Edit Ad');
        echo 'You can edit any of the following properties for Ad ID ' .$_GET['id']. ':<form method="post" action="banners.php"><input type="hidden" name="action" value="edit" /><input type="hidden" name="id" value="' .$_GET['id']. '" /><table width="550" border="1" cellspacing="0" cellpadding="1"><tr><td><b>Наименование:</b></td><td><input type="text" name="ad_name" value="' .$data[11]. '" size="30" /></td></tr><tr><td><b>Включено?</b></td><td><input type="checkbox" ' .$isen. ' name="ad_en" value="1" /> Ad is Enabled</td></tr><tr><td><b>Ссылка на сайт:</b></td><td><input type="text" name="ad_link" value="' .$data[9]. '" size="30" /></td></tr><tr><td><b>Ссылка на изображение:</b></td><td><input type="text" name="ad_image" value="' .$data[10]. '" size="30" /></td></tr><tr><td><b>Ширина:</b></td><td><input type="text" name="ad_width" value="' .$data[7]. '" size="4" /></td></tr><tr><td><b>Высота:</b></td><td><input type="text" name="ad_height" value="' .$data[8]. '" size="4" /></td></tr><tr><td><b>Вес изображения:</b></td><td><input type="text" name="ad_weight" value="' .$data[2]. '" size="4" /></td></tr><tr><td><b>Показов:</b> ' .$data[5]. '&nbsp;<b>C/T:</b> ' .$data[6]. '</td><td><input type="checkbox" name="ad_reset" value="1" /> Обнулить счетчик</td></tr><tr><td><b>Impressions Remaining:</b><br /><span class="smalltext">Set to <b>-1</b> for unlimited</span></td><td><input type="text" name="ad_remain" value="' .$data[4]. '" size="4" /></td></tr><tr><td><b>Истекает:</b></td><td>' .$expires. ' <input type="checkbox" name="ad_noexpires" ' .$noexpires. ' value="1" /> Никогда не Истекает</td></tr></table><br /><div align="center"><input type="submit" name="save" value="Сохранить" /> <input type="submit" name="cancel" value="Отменить" /><br /><br /><input type="checkbox" name="confirm_delete" value="1" /> Поставьте галочку если подтверждаете удаление<br /><input type="submit" name="delete" value="Удалить баннер" /><br /><br /><br /><span class="smalltext">Предпросмотр:</span><br /><a href="' .$data[9]. '" target="_blank"><img src="' .$data[10]. '" alt="' .$data[11]. '" width="' .$data[7]. '" height="' .$data[8]. '" border="0" /></a></div></form>';
        foot();
    }
}
function add()
{
    global $digiAds, $ads;
    if (isset($_POST['save'])) {
        $data = array(11);
        if ($_POST['ad_custom_id'] != '') {
            $data[0] = $_POST['ad_custom_id'];
        } else {
            $data[0] = $digiAds['next_autoindex'];
            $digiAds['next_autoindex']++;
        }
        if (isset($_POST['ad_en']) && $_POST['ad_en'] == 1) {
            $data[1] = 1;
        } else {
            $data[1] = 0;
        }
        $data[2] = $_POST['ad_weight'];
        if (isset($_POST['ad_noexpires']) && $_POST['ad_noexpires'] == 1) {
            $data[3] = '99999999';
        } else {
            $data[3] = mktime(0, 0, 0, $_POST['ad_expires_month'], $_POST['ad_expires_day'], $_POST['ad_expires_year']);
        }
        $data[4] = $_POST['ad_remain'];
        $data[5] = 0;
        $data[6] = 0;
        $data[7] = $_POST['ad_width'];
        $data[8] = $_POST['ad_height'];
        $data[9] = $_POST['ad_link'];
        $data[10] = $_POST['ad_image'];
        $data[11] = stripslashes($_POST['ad_name']);
        $ads[] = join('||', $data);
        writeads();
        menu();
    } else if (isset($_POST['cancel'])) {
        menu();
    } else {
        $expires = dateselect('ad_expires', '99999999');
        head('Новый баннер');
        echo '<form method="post" action="banners.php"><input type="hidden" name="action" value="add"><table width="550" border="1" cellspacing="0" cellpadding="1"><tr><td><b>Пользовательский ID объявления:</b><br /><span class="smalltext">Только буквы и цифры<br />Оставьте пустым, если пользовательский ID</span></td><td><input type="text" name="ad_custom_id" size="30" /></td></tr><tr><td><b>Наименование:</b></td><td><input type="text" name="ad_name" value="New Ad" size="30" /></td></tr><tr><td><b>Включено?</b></td><td><input type="checkbox" checked="checked" name="ad_en" value="1" /> Ad is Enabled</td></tr><tr><td><b>Ссылка на сайт:</b></td><td><input type="text" name="ad_link" value="http://" size="30" /></td></tr><tr><td><b>Ссылка на изображение:</b></td><td><input type="text" name="ad_image" value="http://" size="30" /></td></tr><tr><td><b>Ширина:</b></td><td><input type="text" name="ad_width" value="468" size="4" /></td></tr><tr><td><b>Высота:</b></td><td><input type="text" name="ad_height" value="60" size="4" /></td></tr><tr><td><b>Вес:</b></td><td><input type="text" name="ad_weight" value="1" size="4" /></td></tr><tr><td><b>Impressions Remaining:</b><br/><span class="smalltext">Set to <b>-1</b> for unlimited</span></td><td><input type="text" name="ad_remain" value="-1" size="4"/></td></tr><tr><td><b>Истекает:</b></td><td>' .$expires. ' <input type="checkbox" name="ad_noexpires" checked="checked" value="1" /> Никогда не Истекает</td></tr></table><br /><div align="center"><input type="submit" name="save" value="Сохранить" /> <input type="submit" name="cancel" value="Отменить" /></div></form>';
        foot();
    }
}
function codegen()
{
global $digiAds;
    if (isset($_POST['invocation_type']) && $_POST['invocation_type'] == 'php') {
        if (isset($_POST['nextstep']) && $_POST['nextstep'] == 2) {
            if (isset($_POST['numtypes'])) {
                $numtypes = $_POST['numtypes'];
            } else {
                $numtypes = 1;
            }
            head('Code Generator: Step 2');
            echo "<form method=\"post\" action=\"banners.php\"><input type=\"hidden\" name=\"action\" value=\"codegen\" /><input type=\"hidden\" name=\"invocation_type\" value=\"php\" /><input type=\"hidden\" name=\"nextstep\" value=\"3\" /><input type=\"hidden\" name=\"numtypes\" value=\"$numtypes\">Absolute Path to ads.php: <input type=\"text\" name=\"path\" value=\"" .dirname(__FILE__). "/ads.php\" size=\"35\" /><br /><br /><table>";
            for ($i = 1; $i <= $numtypes; $i++) {
                echo "<tr><td><b>Ad Type $i</b></td></tr><tr><td>Specific Ad ID (if applicable): <input type=\"text\" name=\"adID_$i\" size=\"4\" /> skip next row if not blank</td></tr><tr><td># of Ads: <input type=\"text\" name=\"numads_$i\" size=\"4\" /> Width (if applicable): <input type=\"text\" name=\"width_$i\" size=\"4\" /> Height (width required): <input type=\"text\" name=\"height_$i\" size=\"4\" /></td></tr>";
            }
            echo '</table><div align="center"><input type="submit" value="Generate Code" /> <input type="reset" value="Reset" /></form>';
            foot();
            exit;
        } else if (isset($_POST['nextstep']) && $_POST['nextstep'] == 3) {
            head('Code Generator: Step 3');
            echo 'Copy and Paste the code below to a PHP file where you\'d like the ads displayed:<br /><br /><font face="Verdana, Helvetica" size="-1"><pre>';
            echo "&lt;?php include '" .$_POST['path']. "'; ?&gt;\n";
            for ($i = 1; $i <= $_POST['numtypes']; $i++) {
                if ($_POST["adID_$i"] != '') {
                    echo "\n/* Code to display ad ID " .$_POST["adID_$i"]. " */\n";
                    echo "&lt;?php \$buttons$i = new digiAds (" .$_POST["adID_$i"]. "); ?&gt;\n";
                    echo "&lt;?php echo \$buttons" .$i. "->ad[0]; ?&gt;\n";
                } else {
                    echo "\n/* Code to display " .$_POST["numads_$i"]. " ad(s)";
                    if (($_POST["width_$i"] != '') && ($_POST["height_$i"] != '')) {
                        echo " with width of " .$_POST["width_$i"]. " and height of " .$_POST["height_$i"];
                    }
                    echo " */\n&lt;?php \$buttons$i = new digiAds (null, " .$_POST["numads_$i"];
                    if (($_POST["width_$i"] != '') && ($_POST["height_$i"] != '')) {
                        echo ", " .$_POST["width_$i"]. ", " .$_POST["height_$i"];
                    }
                    echo "); ?&gt;\n";
                    for ($j = 0; $j < $_POST["numads_$i"]; $j++) {
                        echo "&lt;?php echo \$buttons" .$i. "->ad[$j]; ?&gt;\n";
                    }
                }
            }
        }
        echo '</pre></font>';
        foot();
        exit;
    } else if (isset($_POST['invocation_type']) && $_POST['invocation_type'] == 'js') {
        head('Code Generator: Step 2');
        echo 'Copy and Paste the code below to a HTML file where you\'d like the ad displayed:<br /><br /><font face="Verdana, Helvetica" size="-1"><pre>';
        if (isset($_POST['ad_id']) && $_POST['ad_id'] != '') {
            echo '&lt;script language=&quot;javascript&quot; type=&quot;text/javascript&quot; src=&quot;' .$digiAds['js_url']. '?id=' .$_POST['ad_id']. '&quot;&gt;&lt;/script&gt;';
        } else if (isset($_POST['ad_width']) && isset($_POST['ad_height']) && $_POST['ad_width'] != '' && $_POST['ad_height'] != '') {
            echo '&lt;script language=&quot;javascript&quot; type=&quot;text/javascript&quot; src=&quot;' .$digiAds['js_url']. '?width=' .$_POST['ad_width']. '&height=' .$_POST['ad_height']. '&quot;&gt;&lt;/script&gt;';
        } else {
            echo '&lt;script language=&quot;javascript&quot; type=&quot;text/javascript&quot; src=&quot;' .$digiAds['js_url']. '&quot;&gt;&lt;/script&gt;';
        }
        echo '</pre></font>';
        foot();
        exit;
    } else {
        head('Code Generator: Step 1');
        echo 'The code generator will create the necessary PHP or JavaScript code for you to copy and paste wherever you\'d like ads displayed. Because of digi-ads unique design, you can display multiple ads on the same page--even different types of ads (a type of ad can be a group of ads of different width and height, just 1 unique ad, or all ads in the database) when PHP invocation is used to display ads. Should you use JavaScript invocation to display ads, only one ad can be displayed per page. Therefore, it is to your advantage to use PHP to display ads.<hr width="550" /><li><b>PHP Invocation</b></li><form method="post" action="banners.php"><input type="hidden" name="action" value="codegen" /><input type="hidden" name="invocation_type" value="php" /><input type="hidden" name="nextstep" value="2" />How many different types of ads would you like to display? <input type="text" name="numtypes" value="1" size="4" maxlength="2" /> <input type="submit" value="Go!" /></form><hr width="550" /><li><b>JavaScript Invocation</b></li><form method="post" action="banners.php"><input type="hidden" name="action" value="codegen" /><input type="hidden" name="invocation_type" value="js" />Display ad ID <input type="text" name="ad_id" size="4" /> OR<br />Display one ad with width of <input type="text" name="ad_width" size="4" /> and height of <input type="text" name="ad_height" size="4" /> OR<br />Display all ads in the database by leaving the above fields blank. <input type="submit" value="Go!" /></form>';
        foot();
    }
}
?>
