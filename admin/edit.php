<?php

require_once "auth.php";
require_once "../config.php";
ob_start();
if(!isset($_GET['type']) || !isset($_GET['id'])) exit();
$id=intval($_GET['id']);

if(isset($_GET['type']) && isset($_GET['type'])='cargo' && isset($_GET['id'])) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="keywords" content="найти перевозчика, найти груз, найти транспорт,  автоперевозки, avtoperevozki, доставка грузов, перевозки грузов, перевезти груз, рузоперевозки, попутный груз, попутный транспорт, грузовик, найти фуру, грузы в Казахстан, грузы из Казахстана, грузы по Казахстану, грузы в Россию, грузы в Москву, догруз, рефрижератор, тент, контейнеровоз, шаланда, дальнобойщик" />
		<meta name="description" content="" />
		<meta name="author" content="Mikhailenko Alexandr (AlexMAK)">
		<title>AVTOPEREVOZKI.KZ</title>
		<link rel="stylesheet" href="style/css/style.css" type="text/css" media="screen" charset="utf-8" />
		<script src="style/js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script src="style/js/global.js" type="text/javascript" charset="utf-8"></script>
		<script src="style/js/modal.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
			{literal}
				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback(drawChart);
				function drawChart() {
				var data = google.visualization.arrayToDataTable([
				  ['Месяц/год', 'Запрашиваемые цены', 'Предлагаемые цены'],
			{/literal}
				  /*
				  ['2004',  1000,      400],
				  ['2005',  1170,      460],
				  ['2006',  660,       1120],
				  ['2007',  1030,      540]
				  */
				  {foreach name="chartloop" item="chart" key="load_date" from=$prices_chart}
					['{$load_date}',{$chart.cargo},{$chart.transport}]{if $smarty.foreach.chartloop.last}{else}{literal}, {/literal}{/if}
				  {/foreach}
			{literal}
				]);

				var options = {
				  title: 'Предлагаемые и запрашиваемые цены за перевозку грузов'
				};

				var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
				chart.draw(data, options);
				}
			{/literal}
		</script>
	</head>
	<body>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".display-contacts-link").click(function(){
					$("#cargo-contacts-"+$(this).attr("cargoid")).toggle();
				});
				$.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
				$(".hide-on-click").focus(function(){
					$(this).val("");
				});
			});
		</script>
		<div id="header">
			<div class="col w5 bottomlast">
				<a href="" class="logo">
					<img src="style/images/logo.png" alt="Logo" style="border-radius: 20px;"/>
				</a>
			</div>

			<div class="col w5 last right bottomlast">
				<div style="margin-bottom: -240px; right:250px;"><img height="240px" src="http://lp.bigbiceps.kz/images/map.png"></div>
					<p class="last">Вы вошли как <span class="strong">Администратор</span><br/>
					<a href="auth.php?signout">Выйти</a></p>
			</div>
			<div class="clear"></div>
		</div>
		<div id="wrapper">
			<div id="minwidth">
				<div id="holder">
					<div id="menu">
						<div id="left"></div>
						<div id="right"></div>
						<ul>
							<li>
								<a href="index.php"><span>Главная страница</span></a>
							</li>
							<li>
								<a href="index.php?page=cargosearch"><span>Поиск грузов</span></a>
							</li>
							<li>
								<a href="index.php?page=transportsearch"><span>Поиск транспорта</span></a>
							</li>
							<li>
								<a href="index.php?page=page&name=contacts"><span>Обратная связь и контакты</span></a>
							</li>
							<li>
								<a href="index.php?page=page&name=usefullinfo"><span>Полезная информация</span></a>
							</li>
						</ul>
						<div class="clear"></div>
					</div>
					<div id="submenu">
						<div class="modules_left">
							<!--div class="module buttons">
								<a href="" class="dropdown_button"><small class="icon clipboard"></small><span>Меню пользователя</span></a>
								<div class="dropdown help_index">
									<div class="arrow"></div>
									<div class="content">
										<div class="col w11">									
										<a href="index.php?page=newfreight">
											Новый груз
											<span>Добавьте свой груз для отправки</span>
										</a>
										<hr/>
										<a href="index.php?page=mycargo">
											Мои грузы
											<span>Грузы которые вы добавили</span>
										</a>
										<hr/>
										<a href="index.php?page=newtransport">
											Новый транспорт
											<span>Добавьте свой транспорт и заработайте денег</span>
										</a>
										<hr/>
										<a href="index.php?page=mytransport">
											Мой транспорт
											<span>Транспорт который вы добавляли</span>
										</a>
										<hr/>
										</div>
										<div class="clear"></div>
										<a class="button red right close"><small class="icon cross"></small><span>Закрыть</span></a>
										<div class="clear"></div>
									</div>
								</div>
							</div-->
						</div>
						<div class="title">
							Конфигурирование сайта
						</div>
						<div class="modules_right">
						</div>
					</div>
							<div id="table" class="help">
							<div class="col w10 last">
								<div class="content">
	<script type="text/javascript">
		$(document).ready(function(){
			var temp_obj=null;
			function ajaxGetCities()
			{
				temp_obj=$(this).next();
				temp_obj.attr("disabled",true);
				countryid=$(this).val();
				$.get('getcityoptions.php?id='+countryid, function(data) {
					temp_obj.html(data);
					$("#delete").val(data);
				});
				temp_obj.attr("disabled",false);
			}
			$("#more-origin-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-from").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			$("#more-destination-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-to").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			
			$(".country-select").change(ajaxGetCities);
			function recalculateVolume()
			{
				l=$("input[name='length']").val();
				w=$("input[name='width']").val();
				h=$("input[name='height']").val();
				if(l.length==0 || w.length==0 || h.length==0) return;
				ll=parseFloat(l);
				ww=parseFloat(w);
				hh=parseFloat(h);
				$("input[name='volume']").val(ll*ww*hh);
			}
			$("input[name='length']").blur(recalculateVolume);
			$("input[name='width']").blur(recalculateVolume);
			$("input[name='height']").blur(recalculateVolume);
			{literal}
				$( "input[name='date_from']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
				$( "input[name='date_due']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
			{/literal}
			$("input[name='date_due']").change(function(){
				var today = new Date();
				var plus2months = new Date(new Date(today).setMonth(today.getMonth()+3));
				var dtarr=$(this).val().split(".");
				var date2compare=new Date(dtarr[2],dtarr[1],dtarr[0]);
				if(date2compare>plus2months)
				{
					alert("Дата не должна превышать 2-х месяцев");
					$(this).val("");  
				}
			});
		});
	</script>
	<?php
	$db->query("SELECT `id`, `load_from`, `load_due`, `cargo_name`, `transport_required`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `cargo` WHERE `id`='$id'");
	$cargo_arr=array();
	

	while(list( $id, $load_from,  $load_due,  $cargo_name,  $transport_required,  $weight,  $volume,  $vehicles_number,  $length,  $width,  $height,  $price)=$db->fetchRow())
	{
		?>
	<form id="cargo-form" action="index.php?page=editfreight&action=edit&id={$cargo.id}&action=save" method="post">
		<table><tr><td>
		<div style="text-align:right">Все, что выделено <b>жирным</b> - обязательно для заполнения. Остальные поля, при желании, можете пропустить.</div>
		</tr></td></table>
		<table>
			<tr>
				<td class="form-label required"><label>Предполагаемая дата погрузки</label></td>
				<td class="form-input">с <input type="text" name="date_from" value=""<?php echo $current_date; ?>"" class="text w_20" /> по <input type="text" name="date_due" value="<?php echo $due_date; ?>" class="text w_20" />
				</td>
			</tr>
			<tr>
				<td class="form-label required"><label>Пункт погрузки</label></td>
				<td class="form-input">
					<div id="country-city-from" class="location-selection">
						{html_options name="blah" class="country-select" options=$from_country selected=222 style="width:250px"}
						{html_options name="origin[]" options=$from_city selected=$cargo.load_from style="width:250px"}
					</div>
					<!--a id="more-origin-link" href="javascript:void(0)">Дополнительный пункт погрузки</a-->
					<!--{if $error=="noorigin"}<div class="warning">Введите хотя бы один пункт отправки</div>{/if}-->
				</td>
			</tr>
			<tr class="separator">
				<td class="form-label required"><label>Пункт выгрузки</label></td>
				<td class="form-input">
					<div id="country-city-to" class="location-selection">
						<select name="blah" class="country-select">
							<option value="<?php echo $from_country; ?>"><?php echo $from_country; ?></option>
						</select>
						<!--{html_options name="blah" class="country-select" options=$from_country selected=222 style="width:250px"}
						{html_options name="destination[]" options=$from_city selected=21332 style="width:250px"}-->
					</div>
					<!--a id="more-destination-link" href="javascript:void(0)">Дополнительный пункт выгрузки</a-->
					<!--{if $error=="nodestination"}<div class="warning">Введите хотя бы один пункт выгрузки</div>{/if}-->
				</td>
			</tr>
			<tr>
				<td class="form-label required"><label>Описание груза (стройматериалы, сборный груз, оборудование и т.д.)</label></td>
				<td class="form-input">
					<input type="text" name="name" value="<?php echo $cargo_name; ?>" class="text w_20" />
				</td>
			</tr>
			<tr>
				<td class="form-label required"><label>Необходимый тип транспорта<br /><a href="#" target="_blank">Справка о типах транспорта</a></label></td>
				<td class="form-input">
					<div class="col w2">
								<div class="content">
									<form action="test.php">
										<p>
											<select name="select" id="select">
												<option value="option1">Любой</option>
												<option value="option2">Тент</option>
												<option value="option3">Изометрические фургон</option>
												<option value="option4">Рефрижератор</option>
												<option value="option5">Трал</option>
												<option value="option6">Контейнеровоз</option>
												<option value="option7">Другое</option>
											</select>
											
										</p>
									</form>
								</div>
							</div><br />
					<input type="checkbox" class="checkbox" name="canconsolidate">Возможность консолидации
				</td>
			</tr>
			<tr>
				<td class="form-label"><label>Сколько машин необходимо?</label></td>
				<td class="form-input"><input type="text" name="car_amount" value="<?php echo $vehicles_number; ?>" class="text w_20" /></td>
			</tr>
			<tr>
				<td class="form-label"><label>Вес в тоннах</label></td>
				<td class="form-input"><input type="text" name="weight" value="<?php echo $weight; ?>" class="text w_20" /></td>
			</tr>
			<tr>
				<td class="form-label"><label>Обьем в кубических метрах</label></td>
				<td class="form-input"><input type="text" name="volume" value="<?php echo $volume; ?>" class="text w_20" /></td>
			</tr>
			<tr>
				<td class="form-label"><label>Или укажите размеры</label></td>
				<td class="form-input">
					<input type="text" name="length" value="<?php echo $length; ?>" class="text w_20" /> длина(метры) <br />
					<input type="text" name="width" value="<?php echo $width; ?>" class="text w_20" /> ширина(метры) <br />
					<input type="text" name="height" value="<?php echo $height; ?>" class="text w_20" / >высота(метры) 
				</td>
			</tr>
			<tr>
				<td class="form-label"><label>Примерное расстояние (км)</label></td>
				<td class="form-input"><input type="text" name="distance" class="text w_20" /><br/><a href="http://www.ati.su/Trace/" target="_blank">расчет расстояний</a></td>
			</tr>
			<tr>
				<td class="form-label"><label>Стоимость перевозки в тенге</label></td>
				<td class="form-input"><input type="text" name="price" value="<?php echo $price; ?>" class="text w_20" /></td>
			</tr>
			<tr>
				<td class="form-label"><label>Дополнительная информация</label></td>
				<td class="form-input"><textarea name="additional_info" style="width:100%; height:50px;" value="<?php echo $volume; ?>"></textarea></td>
			</tr>
		</table>
		<?php } ?>
		<input type="submit" value="Сохранить изменения"/>
	</form>
					</div>
							</div>
							
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
						<div id="body_footer">
							<div id="bottom_left"><div id="bottom_right"></div></div>
						</div>
					</div>
				</div>
			</div>
			<div class="clear-both"></div>
		</div>
		<div id="footer">
		    <div class="center-wrapper">
		 
		        <div class="one-fifth text-align-center">
		            <a class="" href="#"><img width="135px" src="style/images/logo.png" alt="" style=" border-radius: 10px;"></a>
		            <hr>
		            <p><em> Логистика и транспорт для требовательных клиентов.<br />У вас есть груз? У нас есть транспорт!</em></p>
		            <hr>
		            <ul class="social-icons align-center">
		                <li><a href="#"><img src="style/images/facebook.png" alt=""></a></li>
		                <li><a href="#"><img src="style/images/twitter.png" alt=""></a></li>
		                <li><a href="#"><img src="style/images/rss.png" alt=""></a></li>
		            </ul><!-- end .social-icons -->
		 
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Навигация сайта</h3>
		            <ul class="links-list">
		                <li><a href="#">Главная</a></li>
		                <li><a href="#">Регистрация</a></li>
		                <li><a href="#">Поиск грузов</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Партнеры</h3>
		            <ul class="links-list">
		                <li><a href="#">ENLAB.SU</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Реклама</h3>
		            <ul class="links-list">
		                <li><a href="http://enlab.su/">Социальная барахолка</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <!--div class="one-fifth-last">
		            <a href="#"><h3>Trailers</h3></a>
		            <a href="#"><h3>Photos</h3></a>
		            <a href="#"><h3>Contacts</h3></a>
		        </div--><!-- end .one-fifth-last -->
		        
			</div><!-- end .center-wrapper -->
		</div><!-- end #footer -->
		<!--<div style="font-size:12px">Copyright &copy; 2013 Avtoperevozki.kz. All rights reserved. Create CMS <a href="mailto:flashhacker1988@gmail.com">Alex M.A.K.</a></div-->
	</body>
</html>
<?

/*
echo "<pre>";
print_r($cargo_arr);
echo "</pre>";
*/
$smarty->assign("cargos",$cargo_arr);
// Get countries from database
	$db->query("SELECT `id`, `country_name_ru` FROM `country_`");
	while(list($id,$name)=$db->fetchRow()) $countries_list[$id]=$name;
	$smarty->assign("from_country",$countries_list);

	// Get kazakhstan cities from database. I used DISTINCT coz apparently there are duplicate names for cities in city_ table so far. for instance Baikonur is mentioned 3 times, two of which for Kazakhstan
	$db->query("SELECT `id`, `city_name_ru` FROM `city_` WHERE `id_country`='222' GROUP BY `city_name_ru` ORDER BY `city_name_ru`");
	while(list($id,$name)=$db->fetchRow()) $city_list[$id]=$name;
	$smarty->assign("from_city",$city_list);

	$smarty->assign("current_date",date("d.m.Y"));
	$smarty->assign("due_date",date("d.m.Y",time()+259200));

	// Get transport types from database
	$db->query("SELECT `id`,`name` FROM `transport_types`");
	while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
	$smarty->assign("transports",$transports);

?>
