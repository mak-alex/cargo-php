<?php
require_once "../config.php";
session_start(); //Çàïóñêàåì ñåññèè

/** 
 * Êëàññ äëÿ àâòîðèçàöèè
 * @author äèçàéí ñòóäèÿ ox2.ru 
 */ 
class AuthClass {
    private $_login = "shmadmin"; //Óñòàíàâëèâàåì ëîãèí
    private $_password = "Qq123456"; //Óñòàíàâëèâàåì ïàðîëü

    /**
     * Ïðîâåðÿåò, àâòîðèçîâàí ïîëüçîâàòåëü èëè íåò
     * Âîçâðàùàåò true åñëè àâòîðèçîâàí, èíà÷å false
     * @return boolean 
     */
    public function isAuth() {
        if (isset($_SESSION["is_auth"])) { //Åñëè ñåññèÿ ñóùåñòâóåò
            return $_SESSION["is_auth"]; //Âîçâðàùàåì çíà÷åíèå ïåðåìåííîé ñåññèè is_auth (õðàíèò true åñëè àâòîðèçîâàí, false åñëè íå àâòîðèçîâàí)
        }
        else return false; //Ïîëüçîâàòåëü íå àâòîðèçîâàí, ò.ê. ïåðåìåííàÿ is_auth íå ñîçäàíà
    }
    
    /**
     * Àâòîðèçàöèÿ ïîëüçîâàòåëÿ
     * @param string $login
     * @param string $passwors 
     */
    public function auth($login, $passwors) {
        if ($login == $this->_login && $passwors == $this->_password) { //Åñëè ëîãèí è ïàðîëü ââåäåíû ïðàâèëüíî
            $_SESSION["is_auth"] = true; //Äåëàåì ïîëüçîâàòåëÿ àâòîðèçîâàííûì
            $_SESSION["login"] = $login; //Çàïèñûâàåì â ñåññèþ ëîãèí ïîëüçîâàòåëÿ
            return true;
        }
        else { //Ëîãèí è ïàðîëü íå ïîäîøåë
            $_SESSION["is_auth"] = false;
            return false; 
        }
    }
    
    /**
     * Ìåòîä âîçâðàùàåò ëîãèí àâòîðèçîâàííîãî ïîëüçîâàòåëÿ 
     */
    public function getLogin() {
        if ($this->isAuth()) { //Åñëè ïîëüçîâàòåëü àâòîðèçîâàí
            return $_SESSION["login"]; //Âîçâðàùàåì ëîãèí, êîòîðûé çàïèñàí â ñåññèþ
        }
    }
    
    
    public function out() {
        $_SESSION = array(); //Î÷èùàåì ñåññèþ
        session_destroy(); //Óíè÷òîæàåì
    }
}

$auth = new AuthClass();

if (isset($_POST["login"]) && isset($_POST["password"])) { //Åñëè ëîãèí è ïàðîëü áûëè îòïðàâëåíû
    if (!$auth->auth($_POST["login"], $_POST["password"])) { //Åñëè ëîãèí è ïàðîëü ââåäåí íå ïðàâèëüíî
        echo "<h2 style=\"color:red;\">Ëîãèí è ïàðîëü ââåäåí íå ïðàâèëüíî!</h2>";
    }
}

if (isset($_GET["is_exit"])) { //Åñëè íàæàòà êíîïêà âûõîäà
    if ($_GET["is_exit"] == 1) {
        $auth->out(); //Âûõîäèì
        header("Location: ?is_exit=0"); //Ðåäèðåêò ïîñëå âûõîäà
    }
}
?>

<?php if ($auth->isAuth()) { // Åñëè ïîëüçîâàòåëü àâòîðèçîâàí, ïðèâåòñòâóåì:  
if ($_GET['type']=="delete" && isset($_GET['id'])) {
	$id=intval($_GET['id']);
	if($db->query("DELETE FROM `cargo` WHERE `id`='$id'"))
	{
		//echo "here";
		header("Location:cargo.php");
		exit();
	}	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="keywords" content="найти перевозчика, найти груз, найти транспорт,  автоперевозки, avtoperevozki, доставка грузов, перевозки грузов, перевезти груз, рузоперевозки, попутный груз, попутный транспорт, грузовик, найти фуру, грузы в Казахстан, грузы из Казахстана, грузы по Казахстану, грузы в Россию, грузы в Москву, догруз, рефрижератор, тент, контейнеровоз, шаланда, дальнобойщик" />
		<meta name="description" content="" />
		<meta name="author" content="Mikhailenko Alexandr (AlexMAK)">
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>AVTOPEREVOZKI.KZ</title>
		<link rel="stylesheet" href="style/css/style.css" type="text/css" media="screen" charset="utf-8" />
		<script src="style/js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script src="style/js/global.js" type="text/javascript" charset="utf-8"></script>
		<script src="style/js/modal.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
			{literal}
				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback(drawChart);
				function drawChart() {
				var data = google.visualization.arrayToDataTable([
				  ['Месяц/год', 'Запрашиваемые цены', 'Предлагаемые цены'],
			{/literal}
				  /*
				  ['2004',  1000,      400],
				  ['2005',  1170,      460],
				  ['2006',  660,       1120],
				  ['2007',  1030,      540]
				  */
				  {foreach name="chartloop" item="chart" key="load_date" from=$prices_chart}
					['{$load_date}',{$chart.cargo},{$chart.transport}]{if $smarty.foreach.chartloop.last}{else}{literal}, {/literal}{/if}
				  {/foreach}
			{literal}
				]);

				var options = {
				  title: 'Предлагаемые и запрашиваемые цены за перевозку грузов'
				};

				var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
				chart.draw(data, options);
				}
			{/literal}
		</script>
	</head>
	<body>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".display-contacts-link").click(function(){
					$("#cargo-contacts-"+$(this).attr("cargoid")).toggle();
				});
				$.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
				$(".hide-on-click").focus(function(){
					$(this).val("");
				});
			});
		</script>
		<div id="header">
			<div class="col w5 bottomlast">
				<a href="" class="logo">
					<!--img src="style/images/logo.png" alt="Logo" style="border-radius: 20px;"/-->
				</a>
			</div>

			<div class="col w5 last right bottomlast">
				<div style="margin-bottom:; right:250px;"><!--img height="240px" src="http://lp.bigbiceps.kz/images/map.png"--></div>
					<p class="last">Вы вошли как <span class="strong"><? echo $auth->getLogin(); ?></span><br/>
					<a href="../index.php">Вернуться на сайт</a><br/>
					<a href="?is_exit=1">Выйти</a></p>
			</div>
			<div class="clear"></div>
		</div>
		<div id="wrapper">
			<div id="minwidth">
				<div id="holder">
					<div id="menu">
						<div id="left"></div>
						<div id="right"></div>
						<ul>
							<li>
								<a href="index.php"><span>Главная страница</span></a>
							</li>
							<li>
								<a href="cargo.php"><span>Грузы сайта</span></a>
							</li>
							<li>
								<a href="transport.php"><span>Транспорт сайта</span></a>
							</li>
							<li>
								<a href="statistic.php"><span>Статистика пользователей</span></a>
							</li>
							<li>
				                <a href="backupSQL.php"><span>Резервное копирование</span></a>
				            </li>
							<li>
								<a href="banners.php"><span>Баннеры</span></a>
							</li>
							<!--li>
								<a href="index.php?page=cargosearch"><span>Поиск грузов</span></a>
							</li>
							<li>
								<a href="index.php?page=transportsearch"><span>Поиск транспорта</span></a>
							</li>
							<li>
								<a href="index.php?page=page&name=contacts"><span>Обратная связь и контакты</span></a>
							</li>
							<li>
								<a href="index.php?page=page&name=usefullinfo"><span>Полезная информация</span></a>
							</li-->
						</ul>
						<div class="clear"></div>
					</div>
					<div id="submenu">
						<div class="modules_left">
							<!--div class="module buttons">
								<a href="" class="dropdown_button"><small class="icon clipboard"></small><span>Меню пользователя</span></a>
								<div class="dropdown help_index">
									<div class="arrow"></div>
									<div class="content">
										<div class="col w11">									
										<a href="index.php?page=newfreight">
											Новый груз
											<span>Добавьте свой груз для отправки</span>
										</a>
										<hr/>
										<a href="index.php?page=mycargo">
											Мои грузы
											<span>Грузы которые вы добавили</span>
										</a>
										<hr/>
										<a href="index.php?page=newtransport">
											Новый транспорт
											<span>Добавьте свой транспорт и заработайте денег</span>
										</a>
										<hr/>
										<a href="index.php?page=mytransport">
											Мой транспорт
											<span>Транспорт который вы добавляли</span>
										</a>
										<hr/>
										</div>
										<div class="clear"></div>
										<a class="button red right close"><small class="icon cross"></small><span>Закрыть</span></a>
										<div class="clear"></div>
									</div>
								</div>
							</div-->
						</div>
						<div class="title">
							Редактирование груза
						</div>
						<div class="modules_right">
						</div>
					</div>
							<div id="table" class="help">
							<div class="col w10 last">
								<div class="content">
	<?php
	if($_GET['type']=="edit" && isset($_GET['id'])) {
	$id=intval($_GET['id']);
	$db->query("SELECT `id`, `load_from`, `load_due`, `cargo_name`, `additional_info`, `transport_required`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `cargo` WHERE `id`='$id'");
	while(list( $id, $date_from,  $date_due,  $cargo_name,  $info, $transport_type,  $weight,  $volume,  $car_amount,  $length,  $width,  $height,  $price)=$db->fetchRow())
	{
		?>
	<form id="cargo-form" action="editfreight.php?action=save&id=<?php echo $id; ?>" method="post">
		<table><tr><td>
		<div style="text-align:right">Все, что выделено <b>жирным</b> - обязательно для заполнения. Остальные поля, при желании, можете пропустить.</div>
		</tr></td></table>
		<table>
			
			<tr>
				<td class="form-label required"><label>Предполагаемая дата погрузки</label></td>
				<td class="form-input">с <input type="text" name="date_from" value="<?php echo date("d.m.y", time($date_from)); ?>" class="text w_20" /> по <input type="text" name="date_due" value="<?php echo date("d.m.y", time($date_due));; ?>" class="text w_20" />
				</td>
			</tr>
			<tr>
				<td class="form-label required"><label>Пункт погрузки</label></td>
				<td class="form-input">
					<div id="country-city-from" class="location-selection">
						<!--{html_options name="blah" class="country-select" options=$from_country selected=222 style="width:250px"}-->
						<select name="blah" class="country-select" style="width:250px">
						<?
						// Get countries from database
						$db->query("SELECT `id`, `country_name_ru` FROM `country_`");
						while(list($id,$name)=$db->fetchRow()) {
							echo "<option value='$id' selected=222>$name</option>";
						}
						?>
						</select>
						<select name="origin[]" style="width:250px">
						<?
						// Get kazakhstan cities from database. I used DISTINCT coz apparently there are duplicate names for cities in city_ table so far. for instance Baikonur is mentioned 3 times, two of which for Kazakhstan
						$db->query("SELECT `id`, `city_name_ru` FROM `city_` WHERE `id_country`='222' GROUP BY `city_name_ru` ORDER BY `city_name_ru`");
						while(list($id,$name)=$db->fetchRow()) {
							echo "<option value='$id'>$name</option>";
						}
						?>
						</select>
						<!--{html_options name="origin[]" options=$from_city selected=$from_city style="width:250px"}-->
					</div>
					<!--a id="more-origin-link" href="javascript:void(0)">Дополнительный пункт погрузки</a-->
					<!--{if $error=="noorigin"}<div class="warning">Введите хотя бы один пункт отправки</div>{/if}-->
				</td>
			</tr>
			<tr class="separator">
				<td class="form-label required"><label>Пункт выгрузки</label></td>
				<td class="form-input">
					<div id="country-city-to" class="location-selection">
						<select name="blah" class="country-select" style="width:250px">
						<?
						// Get countries from database
						$db->query("SELECT `id`, `country_name_ru` FROM `country_`");
						while(list($id,$name)=$db->fetchRow()) {
							echo "<option value='$id' selected='222'>$name</option>";
						}
						?>
						</select>
						<select name="destination[]" style="width:250px">
						<?
						// Get kazakhstan cities from database. I used DISTINCT coz apparently there are duplicate names for cities in city_ table so far. for instance Baikonur is mentioned 3 times, two of which for Kazakhstan
						$db->query("SELECT `id`, `city_name_ru` FROM `city_` WHERE `id_country`='222' GROUP BY `city_name_ru` ORDER BY `city_name_ru`");
						while(list($id,$name)=$db->fetchRow()) {
							echo "<option value='$id'>$name</option>";
						}
						?>
						</select>
					</div>
					<!--a id="more-destination-link" href="javascript:void(0)">Дополнительный пункт выгрузки</a-->
					<!--{if $error=="nodestination"}<div class="warning">Введите хотя бы один пункт выгрузки</div>{/if}-->
				</td>
			</tr>
			<tr>
				<td class="form-label required"><label>Описание груза (стройматериалы, сборный груз, оборудование и т.д.)</label></td>
				<td class="form-input">
					<input type="text" name="cargo_name" value="<?php echo $cargo_name; ?>" class="text w_20" />
				</td>
			</tr>
			<tr>
				<td class="form-label required"><label>Необходимый тип транспорта <a href="#" target="_blank">Справка о типах транспорта</a></label></td>
				<td class="form-input">
					<!--{html_options name="transports" options=$transports selected=0 style="width:250px;"}<br/>-->
					<input type="checkbox" class="checkbox" name="canconsolidate">Возможность консолидации
				</td>
			</tr>
			<tr>
				<td class="form-label"><label>Сколько машин необходимо?</label></td>
				<td class="form-input">
				<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
					<input type="text" name="car_amount" value="<?php echo $car_amount; ?>" class="text w_20" />
				</span>
				</td>
			</tr>
			<tr>
				<td class="form-label"><label>Вес в тоннах</label></td>
				<td class="form-input">
				<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
					<input type="text" name="weight" value="<?php echo $weight; ?>" class="text w_20" />
				</span>
				</td>
			</tr>
			<tr>
				<td class="form-label"><label>Обьем в кубических метрах</label></td>
				<td class="form-input">
				<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
					<input type="text" name="volume" value="<?php echo $volume; ?>" class="text w_20" />
				</span>
				</td>
			</tr>
			<tr>
				<td class="form-label"><label>Или укажите размеры</label></td>
				<td class="form-input">
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<input type="text" name="length" value="<?php echo $length; ?>" class="text w_20" />
					</span> длина(метры) <br />
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<input type="text" name="width" value="<?php echo $width; ?>" class="text w_20" />
					</span> ширина(метры) <br />
					<span class=" tooltip yellow-tooltip" data-html="true" data-title="Не более 6 символов">
						<input type="text" name="height" value="<?php echo $height; ?>" class="text w_20" / >
					</span> высота(метры) 
				</td>
			</tr>
			<!--tr>
				<td class="form-label"><label>Примерное расстояние (км)</label></td>
				<td class="form-input"><input type="text" name="distance" class="text w_20" /><br/><a href="http://www.ati.su/Trace/" target="_blank">расчет расстояний</a></td>
			</tr-->
			<tr>
				<td class="form-label"><label>Стоимость перевозки в тенге</label></td>
				<td class="form-input"><input type="text" name="price" value="<?php echo $price; ?>" class="text w_20" /></td>
			</tr>
			<tr>
				<td class="form-label"><label>Дополнительная информация</label></td>
				<td class="form-input"><textarea name="additional_info" style="width:250px; height:50px;" placeholder="<?php echo $info; ?>"></textarea></td>
			</tr>
		</table>
		<input type="submit" value="Сохранить изменения"/>
	</form>
	<?php } ?>
	<?} else {
									?>
										<table>
										<tr>
											<th><strong>ID</strong></th>
											<th><strong>Описание</strong></th>
											<th><strong>Тип и кол-во транспорта</strong></th>
											<th><strong>Вес</strong></th>
											<th><strong>Габариты</strong></th>
											<th><strong>Управление</strong></th>
										</tr>
				                        <?php
										$db->query("SELECT `id`, `load_from`, `load_due`, `cargo_name`, `transport_required`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `cargo`");
										while(list($id, $load_from, $load_due, $cargo_name, $transport_required, $weight, $volume, $vehicles_number, $length, $width, $height, $price)=$db->fetchRow())
										{

				                         ?>
				                        <tr>
				                            <td width="24px">
				                            	<? echo $id; ?>
				                            <td>
											<div class="cargo-name"><strong>Наименование:</strong> <? echo $cargo_name;?></div>
											<div class="cargo-name"><strong>Доп.инфо:</strong> <? echo $info; ?></div>
											<div class="cargo-price">Цена:<? echo $price;?> тенге</div>
										</td>
										<td style="text-align:center">
											<div>Тип транспорта: <? echo $transport_required; ?></div>
											<div class="car-amount">Нужно <? echo $vehicles_number;?> машин</div>
										</td>
										<td style="text-align:center">
											<? echo $weight;?>т.
										</td>
										<td style="text-align:center">
											<? echo $volume; ?>м<sup>3</sup>.
											<? echo $length; ?>м x <? echo $width; ?>м x <? echo $height;?>м м
										</td>
										<td>
											<div id="cargo-contacts-<? echo $id;?>" style="text-align:right;">
						                    	<a href="../index.php?page=contacts&id=<? echo $id; ?>" target="_blank" class="tooltip yellow-tooltip" data-html="true" data-title="Подробная информация
						                            Маршрут
						                            Контакты
						                            Расстояние
						                            Нажмите для подробной информации"><img src="style/images/info.png" style="width:24px; height:24px;">
						                        </a>
												<a href="cargo.php?type=delete&id=<? echo $id; ?>" class="tooltip yellow-tooltip delete" data-html="true" data-title="Удалить данный груз"><img src="style/images/bin.png" style="width:24px; height:24px"></a>
												<a href="cargo.php?type=edit&id=<? echo $id; ?>" class="tooltip yellow-tooltip edit" data-html="true" data-title="Изменить данный груз"><img src="style/images/pencil.png" style="width:24px; height:24px"></a>
										</div>
										
						                                                                  
										</td>                     
				                        </tr>
				                        <?php } ?>
				                        </table>
                        			<?php } ?>
 
							</div>
							</div>
							
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
						<div id="body_footer">
							<div id="bottom_left"><div id="bottom_right"></div></div>
						</div>
					</div>
				</div>
			</div>
			<div class="clear-both"></div>
		</div>
		<div id="footer">
		    <div class="center-wrapper">
		 
		        <div class="one-fifth text-align-center">
		            <a class="" href="#"><img width="135px" src="style/images/logo.png" alt="" style=" border-radius: 10px;"></a>
		            <hr>
		            <p><em> Логистика и транспорт для требовательных клиентов.<br />У вас есть груз? У нас есть транспорт!</em></p>
		            <hr>
		            <ul class="social-icons align-center">
		                <li><a href="#"><img src="style/images/facebook.png" alt=""></a></li>
		                <li><a href="#"><img src="style/images/twitter.png" alt=""></a></li>
		                <li><a href="#"><img src="style/images/rss.png" alt=""></a></li>
		            </ul><!-- end .social-icons -->
		 
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Навигация сайта</h3>
		            <ul class="links-list">
		                <li><a href="#">Главная</a></li>
		                <li><a href="#">Регистрация</a></li>
		                <li><a href="#">Поиск грузов</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Партнеры</h3>
		            <ul class="links-list">
		                <li><a href="#">ENLAB.SU</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Реклама</h3>
		            <ul class="links-list">
		                <li><a href="http://enlab.su/">Социальная барахолка</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <!--div class="one-fifth-last">
		            <a href="#"><h3>Trailers</h3></a>
		            <a href="#"><h3>Photos</h3></a>
		            <a href="#"><h3>Contacts</h3></a>
		        </div--><!-- end .one-fifth-last -->
		        
			</div><!-- end .center-wrapper -->
		</div><!-- end #footer -->
		<!--<div style="font-size:12px">Copyright &copy; 2013 Avtoperevozki.kz. All rights reserved. Create CMS <a href="mailto:flashhacker1988@gmail.com">Alex M.A.K.</a></div-->
	</body>
</html>
<?php
}  else { //Åñëè íå àâòîðèçîâàí, ïîêàçûâàåì ôîðìó ââîäà ëîãèíà è ïàðîëÿ
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="keywords" content="найти перевозчика, найти груз, найти транспорт,  автоперевозки, avtoperevozki, доставка грузов, перевозки грузов, перевезти груз, рузоперевозки, попутный груз, попутный транспорт, грузовик, найти фуру, грузы в Казахстан, грузы из Казахстана, грузы по Казахстану, грузы в Россию, грузы в Москву, догруз, рефрижератор, тент, контейнеровоз, шаланда, дальнобойщик" />
		<meta name="description" content="" />
		<meta name="author" content="Mikhailenko Alexandr (AlexMAK)">
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>AVTOPEREVOZKI.KZ</title>
		<link rel="stylesheet" href="style/css/style.css" type="text/css" media="screen" charset="utf-8" />
		<body>
<script type="text/javascript">
			$(document).ready(function(){
				$(".display-contacts-link").click(function(){
					$("#cargo-contacts-"+$(this).attr("cargoid")).toggle();
				});
				$.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
				$(".hide-on-click").focus(function(){
					$(this).val("");
				});
			});
		</script>
		<div id="header">
			<div class="col w5 bottomlast">
				<a href="" class="logo">
					<img src="style/images/logo.png" alt="Logo" style="border-radius: 20px;"/>
				</a>
			</div>

			<div class="col w5 last right bottomlast">
				<div style="margin-bottom: -240px; right:250px;"><img height="240px" src="http://lp.bigbiceps.kz/images/map.png"></div>
					
			</div>
			<div class="clear"></div>
		</div>
		<div id="wrapper">
			<div id="minwidth">
				<div id="holder">
					<div id="menu">
						<div id="left"></div>
						<div id="right"></div>
						<ul>
							<li>
								<a href="../index.php"><span>Главная страница</span></a>
							</li>
							<li>
								<a href="mailto:flashhacker1988@gmail.com"><span>Заказать модуль или сайт</span></a>
							</li>
							<!--li>
								<a href="index.php?page=cargosearch"><span>Поиск грузов</span></a>
							</li>
							<li>
								<a href="index.php?page=transportsearch"><span>Поиск транспорта</span></a>
							</li>
							<li>
								<a href="index.php?page=page&name=contacts"><span>Обратная связь и контакты</span></a>
							</li>
							<li>
								<a href="index.php?page=page&name=usefullinfo"><span>Полезная информация</span></a>
							</li-->
						</ul>
						<div class="clear"></div>
					</div>
					<div id="submenu">
						<div class="modules_left">
							<!--div class="module buttons">
								<a href="" class="dropdown_button"><small class="icon clipboard"></small><span>Меню пользователя</span></a>
								<div class="dropdown help_index">
									<div class="arrow"></div>
									<div class="content">
										<div class="col w11">									
										<a href="index.php?page=newfreight">
											Новый груз
											<span>Добавьте свой груз для отправки</span>
										</a>
										<hr/>
										<a href="index.php?page=mycargo">
											Мои грузы
											<span>Грузы которые вы добавили</span>
										</a>
										<hr/>
										<a href="index.php?page=newtransport">
											Новый транспорт
											<span>Добавьте свой транспорт и заработайте денег</span>
										</a>
										<hr/>
										<a href="index.php?page=mytransport">
											Мой транспорт
											<span>Транспорт который вы добавляли</span>
										</a>
										<hr/>
										</div>
										<div class="clear"></div>
										<a class="button red right close"><small class="icon cross"></small><span>Закрыть</span></a>
										<div class="clear"></div>
									</div>
								</div>
							</div-->
						</div>
						<div class="title">
							Авторизация
						</div>
						<div class="modules_right">
						</div>
					</div>
						<div id="table" class="help">
							<div class="col w10 last">
								<div class="content">
									<center>
									<form method="post" action="">
									    Логин: <input type="text" name="login" value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; ?>" />
									    Пароль: <input type="password" name="password" value="" />
									    <input type="submit" value="Авторизоваться" />
									</form>
									<img src="http://wallspaper.ru/uploads/gallery/main/16/122357.jpg" height="400px"></img>
								</center>
		<div id="footer">
		    <div class="center-wrapper">
		 
		        <div class="one-fifth text-align-center">
		            <a class="" href="#"><img width="135px" src="style/images/logo.png" alt="" style=" border-radius: 10px;"></a>
		            <hr>
		            <p><em> Логистика и транспорт для требовательных клиентов.<br />У вас есть груз? У нас есть транспорт!</em></p>
		            <hr>
		            <ul class="social-icons align-center">
		                <li><a href="#"><img src="style/images/facebook.png" alt=""></a></li>
		                <li><a href="#"><img src="style/images/twitter.png" alt=""></a></li>
		                <li><a href="#"><img src="style/images/rss.png" alt=""></a></li>
		            </ul><!-- end .social-icons -->
		 
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Навигация сайта</h3>
		            <ul class="links-list">
		                <li><a href="#">Главная</a></li>
		                <li><a href="#">Регистрация</a></li>
		                <li><a href="#">Поиск грузов</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Партнеры</h3>
		            <ul class="links-list">
		                <li><a href="#">ENLAB.SU</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <div class="one-fifth">
		            <h3>Реклама</h3>
		            <ul class="links-list">
		                <li><a href="http://enlab.su/">Социальная барахолка</a></li>
		            </ul><!-- end .links-list -->
		        </div><!-- end .one-fifth -->
		 
		        <!--div class="one-fifth-last">
		            <a href="#"><h3>Trailers</h3></a>
		            <a href="#"><h3>Photos</h3></a>
		            <a href="#"><h3>Contacts</h3></a>
		        </div--><!-- end .one-fifth-last -->
		        
			</div><!-- end .center-wrapper -->
		</div><!-- end #footer -->
		<!--<div style="font-size:12px">Copyright &copy; 2013 Avtoperevozki.kz. All rights reserved. Create CMS <a href="mailto:flashhacker1988@gmail.com">Alex M.A.K.</a></div-->
	</body>
</html>
<?php }
ob_end_flush();

?>
