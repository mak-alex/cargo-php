{extends file="index.tpl"}
{block name="title"}
	<div class="title">
		Контакты
	</div>
{/block}
{block name="main"}
<div class="col w5"><div class="content">
	<form action="mods/contactus.php" method="post" style="padding:30px">
		<table>
			<tr>
				<th><label>Связаться с нами</label></th>
			</tr>
			<tr id="id_1">
				<td><input type="text" name="name" value="Ваше имя" class="text w_20" /></td>
			</tr>
			<tr id="id_2">
				<td><input type="text" name="email" value="Ваше E-mail" class="text w_20" /></td>
			</tr>
			<tr id="id_3">
				<td><textarea style="width:517px" name="" class="hide-on-click">Ваше сообщение</textarea></td>
			</tr>
		</table>
		<input type="submit" value="Отправить"/>
	</form>
</div></div>
<div class="col w5"><div class="content">
	<div style="padding:30px">
	<table>
		<tr>
			<th><label>Наши контакты</label></th>
		</tr>
		<tr id="id_1">
			<td>Если у Вас возникли сложности с поиском грузов или с тем, как найти перевозчика, <br />Вы можете связаться с нами:</td>
		</tr>
		<tr id="id_2">
			<td><b>e-mail</b>: <a href='mailto:info@avtoperevozki.kz'>info@avtoperevozki.kz</a></td>
		</tr>
		<tr id="id_3">
			<td><b>skype</b>: <a href='skype:avtoperevozki kz'>avtoperevozki kz</a></td>
		</tr>
		<tr id="id_4">
			<td><b>тел.</b>: +7 727 227 72 67</td>
		</tr>
		<!--div style="padding:30px">
			Если у Вас возникли сложности с поиском грузов или с тем, как найти перевозчика, Вы можете связаться с нами:<br/>
			<ul>
				<li><b>e-mail</b>: <a href='mailto:info@avtoperevozki.kz'>info@avtoperevozki.kz</a></li>
				<li><b>skype</b>: <a href='skype:avtoperevozki kz'>avtoperevozki kz</a></li>
				<li><b>тел.</b>: +7 727 227 72 67</li>
			</ul>
		</div-->
	</table>
</div></div>
{/block}
