{extends file="index.tpl"}
{block name="main"}
	<script type="text/javascript"></script>
	<div class="title">Ваши грузы</div>
	<table id="cargo-table">
		{foreach item="cargo" key="id" from=$cargos}
			<tr>
				<td colspan="5" class="origin-destination">
					{foreach name="originsloop" item="origin" from=$cargo.origins}
						<b>{$origin}{$id}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
					{/foreach}
					&rarr;
					{foreach name="destinationsloop" item="destination" from=$cargo.destinations}
						<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
					{/foreach}
				</td>
			</tr>
			<tr class="cargo-container">
				<td class="delivery-dates">
					{$cargo.load_from|date_format:"%d.%m.%g"} &mdash; {$cargo.load_due|date_format:"%d.%m.%g"}
					<div><a href="index.php?page=editfreight&action=edit&id={$id}" class="edit">Редактировать</a></div>
                	<div><a href="index.php?page=mycargo&action=delete&id={$id}" class="delete">Удалить</a></div>
				</td>
				<td>
					<div class="cargo-name">{$cargo.cargo_name}</div>
					{if isset($cargo.price)}<div class="cargo-price">Цена:{$cargo.price} тенге</div>{/if}
				</td>
				<td style="text-align:center">
					<div>{$cargo.transport_required}</div>
					{if $cargo.vehicles_number>0}<div class="car-amount">Нужно {$cargo.vehicles_number} машин</div>{/if}
				</td>
				<td style="text-align:center">
					{if $cargo.weight>0}{$cargo.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
				</td>
				<td style="text-align:center">
					{if $cargo.volume>0}{$cargo.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
					{if $cargo.length>0 && $cargo.width>0 && $cargo.height>0}{$cargo.length}м x {$cargo.width}м x {$cargo.height}м м{/if}
				</td>
			</tr>
		{/foreach}
	</table>					
{/block}
