{if isset($cargos)}
	<table id="cargo-table">
		{foreach item="cargo" key="id" from=$cargos}
			<tr>
				<td colspan="6" class="origin-destination">
					{foreach name="originsloop" item="origin" from=$cargo.origins}
						<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
					{/foreach}
					&rarr;
					{foreach name="destinationsloop" item="destination" from=$cargo.destinations}
						<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
					{/foreach}
					<div style="float:right"><a href="javascript:void(0)" class="display-contacts-link" cargoid="{$id}">Отобразить контакты</a></div>
				</td>
			</tr>
			<tr class="cargo-container">
				<td class="favorite">{if isset($my_id)}<img class="add-to-favorites" cargoid="{$id}" src="templates/img/favorite_no.png" alt=""/>{/if}</td>
				<td class="delivery-dates">
					{$cargo.load_from|date_format:"%d.%m"} &mdash; {$cargo.load_due|date_format:"%d.%m"}
					{if isset($my_id) && $my_id==$cargo.account_id}<div><a href="index.php?page=mycargo&action=delete&id={$id}" class="delete">Удалить</a></div>{/if}
				</td>
				<td>
					<div class="cargo-name">{$cargo.cargo_name}</div>
					{if isset($cargo.price)}<div class="cargo-price">Цена:{$cargo.price} тенге</div>{/if}
				</td>
				<td style="text-align:center">
					<div>{$cargo.transport_required}</div>
					{if $cargo.vehicles_number>0}<div class="car-amount">Нужно {$cargo.vehicles_number} машин</div>{/if}
				</td>
				<td style="text-align:center">
					{if $cargo.weight>0}{$cargo.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
				</td>
				<td style="text-align:center">
					{if $cargo.volume>0}{$cargo.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
					{if $cargo.length>0 && $cargo.width>0 && $cargo.height>0}{$cargo.length}м x {$cargo.width}м x {$cargo.height}м м{/if}
				</td>
			</tr>
			<tr id="cargo-contacts-{$id}" class="cargo-contacts" style="display:none">
				<td colspan="6">
						<div class="cargo-contact-name">{$cargo.contact_name}</div>
						<div>{$cargo.contact_email}</div>
						<div class="cargo-phones">
							{foreach item="phone" from=$cargo.contact_phones}
								{$phone}
							{/foreach}
							<div><a href="javascript:void(0)" class="display-contacts-link" cargoid="{$id}" style="text-decoration:none; margin-right:10px; font-weight:bold;">X</a></div>
						</div>
						
				</td>
			</tr>
		{/foreach}
	</table>
{else}
	<div style="text-align:center">По указанным вами критериям, ничего не найдено.</div>
{/if}
