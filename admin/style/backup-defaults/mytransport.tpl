{extends file="index.tpl"}
{block name="main"}
	<script type="text/javascript"></script>
	<div class="title">Ваш транспорт</div>
	<table id="cargo-table">
		{foreach item="transport" key="id" from=$transports}
			<tr class="cargo-container">
				<td class="delivery-dates">
					{$transport.load_from|date_format:"%d.%m"} &mdash; {$transport.load_due|date_format:"%d.%m"}
					<div><a href="index.php?page=mytransport&action=delete&id={$id}" class="delete">Удалить</a></div>
				</td>
				<td>
					<div class="origin-destination">
						{foreach name="originsloop" item="origin" from=$transport.origins}
							<b>{$origin}</b>{if $smarty.foreach.originsloop.last}{else}{literal}, {/literal}{/if}
						{/foreach}
						&rarr;
						{foreach name="destinationsloop" item="destination" from=$transport.destinations}
							<b>{$destination}</b>{if $smarty.foreach.destinationsloop.last}{else}{literal}, {/literal}{/if}
						{/foreach}
					</div>
					{if isset($transport.price)}<div class="cargo-price">Цена:{$transport.price} тенге</div>{/if}
				</td>
				<td style="text-align:center">
					<div>{$transport.transport_required}</div>
					{if $transport.vehicles_number>0}<div class="car-amount">Нужно {$transport.vehicles_number} машин</div>{/if}
				</td>
				<td style="text-align:center">
					{if $transport.weight>0}{$transport.weight}т.{else}<div class="note">тоннаж<br/>неизвестен</div>{/if}
				</td>
				<td style="text-align:center">
					{if $transport.volume>0}{$transport.volume}м<sup>3</sup>.{else}<div class="note">обьем<br/>не задан</div>{/if}
					{if $transport.length>0 && $transport.width>0 && $transport.height>0}{$transport.length}м x {$transport.width}м x {$transport.height}м м{/if}
				</td>
			</tr>
		{/foreach}
	</table>					
{/block}
