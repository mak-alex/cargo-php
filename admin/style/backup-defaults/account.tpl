{extends file="index.tpl"}
{block name="main"}
	<script type="text/javascript">
		$(document).ready(function(){
			$("#change_password_link").click(function(){
				$("#change-password-form").toggle();
			});
			$(".phone-input").blur(function(){
				if($(this).val().replace(/ /g,'').length<11 || $(this).val().replace(/ /g,'').length>12) $(this).after('<div class="warning">Убедитесь, что вы ввели телефон корректно.</div>');
			});
			$("#add-phone-link").click(function(){
				$(this).before('<div><input class="phone-input" type="text" name="phone[]" value="+7"/></div>');
			});
		});
	</script>
	<div class="title">Ваши данные</div>
		{if $error=="input"}
			<div class="warning">К сожалению, вы допустили ошибку при заполнении формы. Пожалуйста, попробуйте заполнить ее еще раз.</div>
		{/if}
		<table>
			<tr>
				<td class="form-label">Электронный адрес</td>
				<td>
					<div>{$my_email} <a id="change_password_link" href="javascript:void(0)">Сменить пароль</a></div>
					{if isset($success)}<div class="success">Пароль успешно изменен.</div>{/if}
					{if $error=="donotmatch"}<div class="warning">К сожалению, вы допустили ошибку. Введенные пароли не совпадают.</div>{/if}
					{if $error=="incorrectoldpass"}<div class="warning">К сожалению, вы допустили ошибку. Текущий пароль не верен.</div>{/if}
					<form id="change-password-form" action="index.php?page=account&action=newpassword" method="post" style="display:none">
						<input type="password" name="old_password"/> Текущий пароль<br/>
						<input type="password" name="new_password"/> Желаемый новый пароль<br/>
						<input type="password" name="new_password_again"/> Новый пароль еще раз<br/>
						<input type="submit" value="Сменить"/>
					</form>
				</td>
			</tr>
			<tr>
				<td class="form-label">Страна</td>
				<td class="form-input" style="white-space:nowrap"><form action="index.php?page=account&action=newcountry" method="post">{html_options name="country" options=$countries_list selected=$my_country_id} <input type="submit" value="Изменить"/></form></td>
			</tr>
			<tr>
				<td class="form-label">Город или селение</td>
				<td class="form-input"><input type="text" name="city"/></td>
			</tr>
			<tr class="separator">
				<td class="form-label">Юридический статус</td>
				<td class="form-input" style="white-space:nowrap"><form action="index.php?page=account&action=newstatus" method="post">{html_options name="legal_statuses_list" options=$legal_statuses_list selected=$my_status_id} <input type="submit" value="Изменить"/></form></td>
			</tr>
			<tr>
				<td class="form-label">Ваше имя или наименование вашей организации</td>
				<td class="form-input"><form action="" method="post"><input type="text" name="name" value="{$my_name}"/><input type="submit" value="Изменить" disabled="disabled"/></form></td>
			</tr>
			<tr>
				<td class="form-label">Телефон(ы)</td>
				<td class="form-input">
					<form action="index.php?page=account&action=changecontacts" method="post">
						{foreach from=$my_phones item=phone}
							<div><input type="text" class="phone-input" name="phone[]" value="{$phone}"/></div>
						{/foreach}
						<div><a id="add-phone-link" href="javascript:void(0)">Добавить еще телефон</a></div>
						<input type="submit" value="Обновить"/>
					</form>
				</td>
			</tr>			
		</table>
{/block}
