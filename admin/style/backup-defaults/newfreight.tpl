{extends file="index.tpl"}
{block name="main"}
	<script type="text/javascript">
		$(document).ready(function(){
			var temp_obj=null;
			function ajaxGetCities()
			{
				temp_obj=$(this).next();
				temp_obj.attr("disabled",true);
				countryid=$(this).val();
				$.get('getcityoptions.php?id='+countryid, function(data) {
					temp_obj.html(data);
					$("#delete").val(data);
				});
				temp_obj.attr("disabled",false);
			}
			$("#more-origin-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-from").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			$("#more-destination-link").click(function(){
				$(this).before("<div class='location-selection'>"+$("#country-city-to").html()+"</div>");
				$(".country-select").change(ajaxGetCities);
			});
			
			$(".country-select").change(ajaxGetCities);
			function recalculateVolume()
			{
				l=$("input[name='length']").val();
				w=$("input[name='width']").val();
				h=$("input[name='height']").val();
				if(l.length==0 || w.length==0 || h.length==0) return;
				ll=parseFloat(l);
				ww=parseFloat(w);
				hh=parseFloat(h);
				$("input[name='volume']").val(ll*ww*hh);
			}
			$("input[name='length']").blur(recalculateVolume);
			$("input[name='width']").blur(recalculateVolume);
			$("input[name='height']").blur(recalculateVolume);
			{literal}
				$( "input[name='date_from']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
				$( "input[name='date_due']" ).datepicker({dateFormat: 'dd.mm.yy',constrainInput: true});
			{/literal}
			$("input[name='date_due']").change(function(){
				var today = new Date();
				var plus2months = new Date(new Date(today).setMonth(today.getMonth()+3));
				var dtarr=$(this).val().split(".");
				var date2compare=new Date(dtarr[2],dtarr[1],dtarr[0]);
				if(date2compare>plus2months)
				{
					alert("Дата не должна превышать 2-х месяцев");
					$(this).val("");  
				}
			});
		});
	</script>
	<div class="title">Параметры перевозки</div>
	{if $error=="input"}
		<div class="warning">К сожалению, вы допустили ошибку при заполнении формы. Пожалуйста, попробуйте заполнить ее еще раз.</div>
	{/if}
	<form id="cargo-form" action="index.php?page=newreight&action=do" method="post">
		<div style="text-align:right">Все, что выделено <b>жирным</b> - обязательно для заполнения. Остальные поля, при желании, можете пропустить.</div>
		<table>
			{foreach item="cargo" key="id" from=$cargos}
			<tr>
				<td class="form-label required">Предполагаемая дата погрузки</td>
				<td class="form-input">с <input type="text" name="date_from" value="{$current_date}"/> по <input type="text" name="date_due" value="{$due_date}"/>
				{if $error=="date"}<div class="warning">Даты были введены не правильно</div>{/if}
				</td>
			</tr>
			<tr>
				<td class="form-label required">Пункт погрузки</td>
				<td class="form-input">
					<div id="country-city-from" class="location-selection">
						{html_options name="blah" class="country-select" options=$from_country selected=222 style="width:100%"}
						{html_options name="origin[]" options=$from_city selected=$cargo.load_from}
					</div>
					<a id="more-origin-link" href="javascript:void(0)">Дополнительный пункт погрузки</a>
					{if $error=="noorigin"}<div class="warning">Введите хотя бы один пункт отправки</div>{/if}
				</td>
			</tr>
			<tr class="separator">
				<td class="form-label required">Пункт выгрузки</td>
				<td class="form-input">
					<div id="country-city-to" class="location-selection">
						{html_options name="blah" class="country-select" options=$from_country selected=222 style="width:100%"}
						{html_options name="destination[]" options=$from_city selected=21332}
					</div>
					<a id="more-destination-link" href="javascript:void(0)">Дополнительный пункт выгрузки</a>
					{if $error=="nodestination"}<div class="warning">Введите хотя бы один пункт выгрузки</div>{/if}
				</td>
			</tr>
			<tr>
				<td class="form-label required">Описание груза (стройматериалы, сборный груз, оборудование и т.д.)</td>
				<td class="form-input">
					<input type="text" name="name" style="width:100%" value="{$cargo.cargo_name}"/>
					{if $error=="noname"}<div class="warning">Введите наименование груза пожалуйста</div>{/if}
				</td>
			</tr>
			<tr>
				<td class="form-label required">Необходимый тип транспорта <a href="#" target="_blank">Справка о типах транспорта</a></td>
				<td class="form-input">
					{html_options name="transports" options=$transports selected=0}<br/>
					<input type="checkbox" name="canconsolidate"/> Возможность консолидации
				</td>
			</tr>
			<tr>
				<td class="form-label">Сколько машин необходимо?</td>
				<td class="form-input"><input type="text" name="car_amount" style="width:100%" value="{$cargo.vehicles_number}"/></td>
			</tr>
			<tr>
				<td class="form-label">Вес в тоннах</td>
				<td class="form-input"><input type="text" name="weight" style="width:100%" value="{$cargo.weight}"/></td>
			</tr>
			<tr>
				<td class="form-label">Обьем в кубических метрах</td>
				<td class="form-input"><input type="text" name="volume" style="width:100%" value="{$cargo.volume}"/></td>
			</tr>
			<tr>
				<td class="form-label">Или укажите размеры</td>
				<td class="form-input">
					длина(метры) <input type="text" name="length" value="{$cargo.length}"/>
					ширина(метры) <input type="text" name="width" value="{$cargo.width}"/>
					высота(метры) <input type="text" name="height" value="{$cargo.height}"/>
				</td>
			</tr>
			<tr>
				<td class="form-label">Примерное расстояние (км)</td>
				<td class="form-input"><input type="text" name="distance"/><br/><a href="http://www.ati.su/Trace/" target="_blank">расчет расстояний</a></td>
			</tr>
			<tr>
				<td class="form-label">Стоимость перевозки в тенге</td>
				<td class="form-input"><input type="text" name="price" style="width:100%" value="{if isset($cargo.price)}{$cargo.price}{/if}"/></td>
			</tr>
			<tr>
				<td class="form-label">Дополнительная информация</td>
				<td class="form-input"><textarea name="additional_info" style="width:100%; height:50px;" value="{$cargo.volume}"></textarea></td>
			</tr>
			{/foreach}
		</table>
		<input type="submit" value="Сохранить изменения"/>
	</form>
{/block}
	