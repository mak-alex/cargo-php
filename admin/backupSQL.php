<?php
require_once "../config.php";
session_start(); //Çàïóñêàåì ñåññèè

/** 
 * Êëàññ äëÿ àâòîðèçàöèè
 * @author äèçàéí ñòóäèÿ ox2.ru 
 */ 
class AuthClass {
    private $_login = "shmadmin"; //Óñòàíàâëèâàåì ëîãèí
    private $_password = "Qq123456"; //Óñòàíàâëèâàåì ïàðîëü

    /**
     * Ïðîâåðÿåò, àâòîðèçîâàí ïîëüçîâàòåëü èëè íåò
     * Âîçâðàùàåò true åñëè àâòîðèçîâàí, èíà÷å false
     * @return boolean 
     */
    public function isAuth() {
        if (isset($_SESSION["is_auth"])) { //Åñëè ñåññèÿ ñóùåñòâóåò
            return $_SESSION["is_auth"]; //Âîçâðàùàåì çíà÷åíèå ïåðåìåííîé ñåññèè is_auth (õðàíèò true åñëè àâòîðèçîâàí, false åñëè íå àâòîðèçîâàí)
        }
        else return false; //Ïîëüçîâàòåëü íå àâòîðèçîâàí, ò.ê. ïåðåìåííàÿ is_auth íå ñîçäàíà
    }
    
    /**
     * Àâòîðèçàöèÿ ïîëüçîâàòåëÿ
     * @param string $login
     * @param string $passwors 
     */
    public function auth($login, $passwors) {
        if ($login == $this->_login && $passwors == $this->_password) { //Åñëè ëîãèí è ïàðîëü ââåäåíû ïðàâèëüíî
            $_SESSION["is_auth"] = true; //Äåëàåì ïîëüçîâàòåëÿ àâòîðèçîâàííûì
            $_SESSION["login"] = $login; //Çàïèñûâàåì â ñåññèþ ëîãèí ïîëüçîâàòåëÿ
            return true;
        }
        else { //Ëîãèí è ïàðîëü íå ïîäîøåë
            $_SESSION["is_auth"] = false;
            return false; 
        }
    }
    
    /**
     * Ìåòîä âîçâðàùàåò ëîãèí àâòîðèçîâàííîãî ïîëüçîâàòåëÿ 
     */
    public function getLogin() {
        if ($this->isAuth()) { //Åñëè ïîëüçîâàòåëü àâòîðèçîâàí
            return $_SESSION["login"]; //Âîçâðàùàåì ëîãèí, êîòîðûé çàïèñàí â ñåññèþ
        }
    }
    
    
    public function out() {
        $_SESSION = array(); //Î÷èùàåì ñåññèþ
        session_destroy(); //Óíè÷òîæàåì
    }
}

$auth = new AuthClass();

if (isset($_POST["login"]) && isset($_POST["password"])) { //Åñëè ëîãèí è ïàðîëü áûëè îòïðàâëåíû
    if (!$auth->auth($_POST["login"], $_POST["password"])) { //Åñëè ëîãèí è ïàðîëü ââåäåí íå ïðàâèëüíî
        echo "<h2 style=\"color:red;\">Ëîãèí è ïàðîëü ââåäåí íå ïðàâèëüíî!</h2>";
    }
}

if (isset($_GET["is_exit"])) { //Åñëè íàæàòà êíîïêà âûõîäà
    if ($_GET["is_exit"] == 1) {
        $auth->out(); //Âûõîäèì
        header("Location: ?is_exit=0"); //Ðåäèðåêò ïîñëå âûõîäà
    }
}

?>

<?php if ($auth->isAuth()) { // Åñëè ïîëüçîâàòåëü àâòîðèçîâàí, ïðèâåòñòâóåì:  
  /* Массив с именами баз данных (имя базы данных можно посмотреть в phpMyAdmin) */
  $db_names = array();
  $db_names[] = "v_8075_cargo";
  $db_names[] = "v_8075_freight";
 
  /* Массив с именами директорий, в которых лежат все файлы сайта */
  /* ВАЖНО: Все пути должны быть физическими и идти от корня сервера. Точный физический путь можно посмотреть через phpinfo() */
  $source_dirs = array();
  $source_dirs[] = "../backup";
  //$source_dirs[] = "/home/mysite2.ru";
 
  $offset_dirs = strlen("/avtoperevozki.kz/"); // Служебная переменная, служащая для устранения лишних папок в архиве
 
  /* Параметры подключения к базе данных */
  $host = "localhost";
  $user = "v_8075_freight";
  $password = "ZVlt1cWY";
 
  $dump_dir = "../backup"; // Директория, куда будут помещаться архивы
  $delay_delete = 35 * 24 * 3600; // Время в секундах, через которое архивы будут удаляться
  $filezip = "backup_".date("Y-m-d").".zip"; // Имя архива
  $filesql = "backup_".date("Y-m-d");
if ($_GET['sql']=="Backup") {
  set_time_limit(0); // Убираем ограничение на максимальное время работы скрипта

  //deleteOldArchive(); // Удаляем все старые архивы
 
  if (file_exists($dump_dir."/".$filezip)) exit; // Если архив с таким именем уже есть, то заканчиваем скрипт
 
  $db_files = array(); // Массив, куда будут помещаться файлы с дампом баз данных
 
  for ($i = 0; $i < count($db_names); $i++) {
    $filename = $db_names[$i].".sql"; // Имя файла с дампом базы данных
    $db_files[] = $dump_dir."/".$filename; // Помещаем файл в массив
    $fp = fopen($dump_dir."/".$filesql.".sql", "a"); // Открываем файл
    $db = new mysqli($host, $user, $password, $db_names[$i]); // Соединяемся с базой данных
    $db->query("SET NAMES 'utf-8'"); // Устанавливаем кодировку соединения
    $result_set = $db->query("SHOW TABLES"); // Запрашиваем все таблицы из базы
    while (($table = $result_set->fetch_assoc()) != false) {
      /* Перебор всех таблиц в базе данных */
      $table = array_values($table);
      if ($fp) {
        $result_set_table = $db->query("SHOW CREATE TABLE `".$table[0]."`"); // Получаем запрос на создание таблицы
        $query = $result_set_table->fetch_assoc();
        $query = array_values($query);
        fwrite($fp, "\n".$query[1].";\n"); // Добавляем результат в файл
        $rows = "SELECT * FROM `".$table[0]."`";
        $result_set_rows = $db->query($rows); // Получаем список всех записей в таблице
        while (($row = $result_set_rows->fetch_assoc()) != false) {
          $query = "";
          /* Путём перебора всех записей добавляем запросы на их создание в файл */
          foreach ($row as $field) {
            if (is_null($field)) $field = "NULL";
            else $field = "'".$db->real_escape_string($field)."'"; // Экранируем значения
            if ($query == "") $query = $field;
            else $query .= ", ".$field;
          }
          $query = "INSERT INTO `".$table[0]."` VALUES (".$query.");";
          fwrite($fp, $query);
        }
      }
    }
    fclose($fp); // Закрываем файл
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="найти перевозчика, найти груз, найти транспорт,  автоперевозки, avtoperevozki, доставка грузов, перевозки грузов, перевезти груз, рузоперевозки, попутный груз, попутный транспорт, грузовик, найти фуру, грузы в Казахстан, грузы из Казахстана, грузы по Казахстану, грузы в Россию, грузы в Москву, догруз, рефрижератор, тент, контейнеровоз, шаланда, дальнобойщик" />
    <meta name="description" content="" />
    <meta name="author" content="Mikhailenko Alexandr (AlexMAK)">
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>AVTOPEREVOZKI.KZ</title>
    <link rel="stylesheet" href="style/css/style.css" type="text/css" media="screen" charset="utf-8" />
    <script src="style/js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script src="style/js/global.js" type="text/javascript" charset="utf-8"></script>
    <script src="style/js/modal.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      {literal}
        google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Месяц/год', 'Запрашиваемые цены', 'Предлагаемые цены'],
      {/literal}
          /*
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
          */
          {foreach name="chartloop" item="chart" key="load_date" from=$prices_chart}
          ['{$load_date}',{$chart.cargo},{$chart.transport}]{if $smarty.foreach.chartloop.last}{else}{literal}, {/literal}{/if}
          {/foreach}
      {literal}
        ]);

        var options = {
          title: 'Предлагаемые и запрашиваемые цены за перевозку грузов'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
        }
      {/literal}
    </script>
  </head>
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        $(".display-contacts-link").click(function(){
          $("#cargo-contacts-"+$(this).attr("cargoid")).toggle();
        });
        $.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $(".hide-on-click").focus(function(){
          $(this).val("");
        });
      });
    </script>
    <div id="header">
      <div class="col w5 bottomlast">
        <a href="" class="logo">
          <!--img src="style/images/logo.png" alt="Logo" style="border-radius: 20px;"/-->
        </a>
      </div>

      <div class="col w5 last right bottomlast">
        <div style="margin-bottom:x; right:250px;"><!--img height="240px" src="http://lp.bigbiceps.kz/images/map.png"--></div>
          <p class="last">Вы вошли как <span class="strong"><? echo $auth->getLogin(); ?></span><br/>
            <a href="../index.php">Вернуться на сайт</a><br/>
          <a href="?is_exit=1">Выйти</a></p>
      </div>
      <div class="clear"></div>
    </div>
    <div id="wrapper">
      <div id="minwidth">
        <div id="holder">
          <div id="menu">
            <div id="left"></div>
            <div id="right"></div>
            <ul>
              <li>
                <a href="index.php"><span>Главная страница</span></a>
              </li>
              <li>
                <a href="cargo.php"><span>Грузы сайта</span></a>
              </li>
              <li>
                <a href="transport.php"><span>Транспорт сайта</span></a>
              </li>
              <li>
                <a href="statistic.php"><span>Статистика пользователей</span></a>
              </li>
              <li>
                <a href="backupSQL.php"><span>Резервное копирование</span></a>
              </li>
              <li>
                <a href="banners.php"><span>Баннеры</span></a>
              </li>
              <!--li>
                <a href="index.php?page=cargosearch"><span>Поиск грузов</span></a>
              </li>
              <li>
                <a href="index.php?page=transportsearch"><span>Поиск транспорта</span></a>
              </li>
              <li>
                <a href="index.php?page=page&name=contacts"><span>Обратная связь и контакты</span></a>
              </li>
              <li>
                <a href="index.php?page=page&name=usefullinfo"><span>Полезная информация</span></a>
              </li-->
            </ul>
            <div class="clear"></div>
          </div>
          <div id="submenu">
            <div class="modules_left">
              <!--div class="module buttons">
                <a href="" class="dropdown_button"><small class="icon clipboard"></small><span>Меню пользователя</span></a>
                <div class="dropdown help_index">
                  <div class="arrow"></div>
                  <div class="content">
                    <div class="col w11">                 
                    <a href="index.php?page=newfreight">
                      Новый груз
                      <span>Добавьте свой груз для отправки</span>
                    </a>
                    <hr/>
                    <a href="index.php?page=mycargo">
                      Мои грузы
                      <span>Грузы которые вы добавили</span>
                    </a>
                    <hr/>
                    <a href="index.php?page=newtransport">
                      Новый транспорт
                      <span>Добавьте свой транспорт и заработайте денег</span>
                    </a>
                    <hr/>
                    <a href="index.php?page=mytransport">
                      Мой транспорт
                      <span>Транспорт который вы добавляли</span>
                    </a>
                    <hr/>
                    </div>
                    <div class="clear"></div>
                    <a class="button red right close"><small class="icon cross"></small><span>Закрыть</span></a>
                    <div class="clear"></div>
                  </div>
                </div>
              </div-->
            </div>
            <div class="title">
              Статистика
            </div>
            <div class="modules_right">
            </div>
          </div>
              <div id="table" class="help">
              <div class="col w10 last">
                <div class="content">

                  </div>
              </div>
              <div class="col w5">
              <div class="content">
                <div class="box">
                  <div class="head"><div></div></div>
                  <center><h2>Последние резервные копии</h2></center>
                      <div class="desc" style="height:250px;min-height:250px; overflow: auto;">
                        <? 
                            $dir = $dump_dir;   //задаём имя директории
                            if(is_dir($dir)) {   //проверяем наличие директории
                                 echo '<b>'.$dir.'</b> - директория с резервными копиями;<br>';
                                 $files = scandir($dir);    //сканируем (получаем массив файлов)
                                 array_shift($files); // удаляем из массива '.'
                                 array_shift($files); // удаляем из массива '..'
                                 for($i=0; $i<sizeof($files); $i++) echo '-<b>Резервная копия:</b> '.$files[$i].'<br/>';  //выводим все файлы
                            }
                            else echo $dir.' -такой директории нет;<br>';
                        ?>
                        </div>
                       <div class="bottom">
                      <div></div>
                    </div>
                  </div> 
                </div>
              </div>
              <div class="col w5">
              <div class="content">
                <div class="box">
                  <div class="head"><div></div></div>
                  <center><h2>Создать резервную копию</h2></center>
                  <div class="desc" style="height:250px;min-height:250px; overflow: auto;">
                      <strong>Директория с копиями: </strong><? echo $dump_dir; ?><br />
                      <strong>Имя созданной копии: </strong><? echo $filesql.".sql"; ?><br/>
                  </div>
                   <div class="bottom">
                  <div></div>
                </div>
              </div> 
            </div>
          </div>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div id="body_footer">
              <div id="bottom_left"><div id="bottom_right"></div></div>
            </div>
          </div>
        </div>
      </div>
      <div class="clear-both"></div>
    </div>
    <div id="footer">
        <div class="center-wrapper">
     
            <div class="one-fifth text-align-center">
                <a class="" href="#"><img width="135px" src="style/images/logo.png" alt="" style=" border-radius: 10px;"></a>
                <hr>
                <p><em> Логистика и транспорт для требовательных клиентов.<br />У вас есть груз? У нас есть транспорт!</em></p>
                <hr>
                <ul class="social-icons align-center">
                    <li><a href="#"><img src="style/images/facebook.png" alt=""></a></li>
                    <li><a href="#"><img src="style/images/twitter.png" alt=""></a></li>
                    <li><a href="#"><img src="style/images/rss.png" alt=""></a></li>
                </ul><!-- end .social-icons -->
     
            </div><!-- end .one-fifth -->
     
            <div class="one-fifth">
                <h3>Навигация сайта</h3>
                <ul class="links-list">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Регистрация</a></li>
                    <li><a href="#">Поиск грузов</a></li>
                </ul><!-- end .links-list -->
            </div><!-- end .one-fifth -->
     
            <div class="one-fifth">
                <h3>Партнеры</h3>
                <ul class="links-list">
                    <li><a href="#">ENLAB.SU</a></li>
                </ul><!-- end .links-list -->
            </div><!-- end .one-fifth -->
     
            <div class="one-fifth">
                <h3>Реклама</h3>
                <ul class="links-list">
                    <li><a href="http://enlab.su/">Социальная барахолка</a></li>
                </ul><!-- end .links-list -->
            </div><!-- end .one-fifth -->
     
            <!--div class="one-fifth-last">
                <a href="#"><h3>Trailers</h3></a>
                <a href="#"><h3>Photos</h3></a>
                <a href="#"><h3>Contacts</h3></a>
            </div--><!-- end .one-fifth-last -->
            
      </div><!-- end .center-wrapper -->
    </div><!-- end #footer -->
    <!--<div style="font-size:12px">Copyright &copy; 2013 Avtoperevozki.kz. All rights reserved. Create CMS <a href="mailto:flashhacker1988@gmail.com">Alex M.A.K.</a></div-->
  </body>
</html>
<?
    $db->close(); // Закрываем соединение с базой данных и переходим к следующей
  }
 
  $zip = new ZipArchive(); // Создаём объект класса ZipArchive
  $allfiles = array(); // Массив со списком всех файлов, которые будут помещены в архив
  if ($zip->open($dump_dir."/".$filezip, ZipArchive::CREATE) === true) {
    for ($i = 0; $i < count($source_dirs); $i++) {
      /* Рекурсивный перебор всех директорий */
      if (is_dir($source_dirs[$i])) recoursiveDir($source_dirs[$i]);
      else $allfiles[] = $source_dirs[$i]; // Добавляем файл в итоговый массив
      foreach ($allfiles as $val){
        /* Добавляем в ZIP-архив все полученные файлы */
        $local = substr($val, $offset_dirs);
        $zip->addFile($val, $local);
      }
    }
    /* Добавляем в ZIP-архив все дампы баз данных */
    for ($i = 0; $i < count($db_files); $i++) {
      $local = substr($db_files[$i], strlen($dump_dir) + 1);
      $zip->addFile($db_files[$i], $local);
    }
    $zip->close();
  }
 
  for ($i = 0; $i < count($db_files); $i++) unlink($db_files[$i]); // Очищаем массив db_files
 
  /* Функция для рекурсивного перебора и сохранения всех файлов и папок в массив, который затем возвращается */
  function recoursiveDir($dir){
    global $allfiles;
    if ($files = glob($dir."/{,.}*", GLOB_BRACE)) {
      foreach($files as $file){
        $b_name = basename($file);
        if (($b_name == ".") || ($b_name == "..")) continue;
        if (is_dir($file)) recoursiveDir($file);
        else $allfiles[] = $file;
      }
    }
  }
 
  /* Функция для удаления всех старых архивов */
  function deleteOldArchive() {
    global $dump_dir;
    global $delay_delete;
    $ts = time();
    $files = glob($dump_dir."/*.zip");
    foreach ($files as $file)
      if ($ts - filemtime($file) > $delay_delete) unlink($file);
  }
} else {
  function dirr($dir){
  if (is_dir($dir)) {
     if ($dh = opendir($dir)) {
         while (($file = readdir($dh)) !== false) {
             echo "Файл: $file : тип: " . filetype($dir . $file) . "<br>";
             if(filetype($dir.$file)=='dir'){
               dirr($dir.$file);
             }
         }
         closedir($dh);
     }
  }

}
  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="найти перевозчика, найти груз, найти транспорт,  автоперевозки, avtoperevozki, доставка грузов, перевозки грузов, перевезти груз, рузоперевозки, попутный груз, попутный транспорт, грузовик, найти фуру, грузы в Казахстан, грузы из Казахстана, грузы по Казахстану, грузы в Россию, грузы в Москву, догруз, рефрижератор, тент, контейнеровоз, шаланда, дальнобойщик" />
    <meta name="description" content="" />
    <meta name="author" content="Mikhailenko Alexandr (AlexMAK)">
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>AVTOPEREVOZKI.KZ</title>
    <link rel="stylesheet" href="style/css/style.css" type="text/css" media="screen" charset="utf-8" />
    <script src="style/js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script src="style/js/global.js" type="text/javascript" charset="utf-8"></script>
    <script src="style/js/modal.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      {literal}
        google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Месяц/год', 'Запрашиваемые цены', 'Предлагаемые цены'],
      {/literal}
          /*
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
          */
          {foreach name="chartloop" item="chart" key="load_date" from=$prices_chart}
          ['{$load_date}',{$chart.cargo},{$chart.transport}]{if $smarty.foreach.chartloop.last}{else}{literal}, {/literal}{/if}
          {/foreach}
      {literal}
        ]);

        var options = {
          title: 'Предлагаемые и запрашиваемые цены за перевозку грузов'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
        }
      {/literal}
    </script>
  </head>
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
        $(".display-contacts-link").click(function(){
          $("#cargo-contacts-"+$(this).attr("cargoid")).toggle();
        });
        $.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                'Июл','Авг','Сен','Окт','Ноя','Дек'],
                dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                weekHeader: 'Не',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $(".hide-on-click").focus(function(){
          $(this).val("");
        });
      });
    </script>
    <div id="header">
      <div class="col w5 bottomlast">
        <a href="" class="logo">
          <!--img src="style/images/logo.png" alt="Logo" style="border-radius: 20px;"/-->
        </a>
      </div>

      <div class="col w5 last right bottomlast">
        <div style="margin-bottom:x; right:250px;"><!--img height="240px" src="http://lp.bigbiceps.kz/images/map.png"--></div>
          <p class="last">Вы вошли как <span class="strong"><? echo $auth->getLogin(); ?></span><br/>
            <a href="../index.php">Вернуться на сайт</a><br/>
          <a href="?is_exit=1">Выйти</a></p>
      </div>
      <div class="clear"></div>
    </div>
    <div id="wrapper">
      <div id="minwidth">
        <div id="holder">
          <div id="menu">
            <div id="left"></div>
            <div id="right"></div>
            <ul>
              <li>
                <a href="index.php"><span>Главная страница</span></a>
              </li>
              <li>
                <a href="cargo.php"><span>Грузы сайта</span></a>
              </li>
              <li>
                <a href="transport.php"><span>Транспорт сайта</span></a>
              </li>
              <li>
                <a href="statistic.php"><span>Статистика пользователей</span></a>
              </li>
              <li>
                <a href="backupSQL.php"><span>Резервное копирование</span></a>
              </li>
              <li>
                <a href="banners.php"><span>Баннеры</span></a>
              </li>
              <!--li>
                <a href="index.php?page=cargosearch"><span>Поиск грузов</span></a>
              </li>
              <li>
                <a href="index.php?page=transportsearch"><span>Поиск транспорта</span></a>
              </li>
              <li>
                <a href="index.php?page=page&name=contacts"><span>Обратная связь и контакты</span></a>
              </li>
              <li>
                <a href="index.php?page=page&name=usefullinfo"><span>Полезная информация</span></a>
              </li-->
            </ul>
            <div class="clear"></div>
          </div>
          <div id="submenu">
            <div class="modules_left">
              <!--div class="module buttons">
                <a href="" class="dropdown_button"><small class="icon clipboard"></small><span>Меню пользователя</span></a>
                <div class="dropdown help_index">
                  <div class="arrow"></div>
                  <div class="content">
                    <div class="col w11">                 
                    <a href="index.php?page=newfreight">
                      Новый груз
                      <span>Добавьте свой груз для отправки</span>
                    </a>
                    <hr/>
                    <a href="index.php?page=mycargo">
                      Мои грузы
                      <span>Грузы которые вы добавили</span>
                    </a>
                    <hr/>
                    <a href="index.php?page=newtransport">
                      Новый транспорт
                      <span>Добавьте свой транспорт и заработайте денег</span>
                    </a>
                    <hr/>
                    <a href="index.php?page=mytransport">
                      Мой транспорт
                      <span>Транспорт который вы добавляли</span>
                    </a>
                    <hr/>
                    </div>
                    <div class="clear"></div>
                    <a class="button red right close"><small class="icon cross"></small><span>Закрыть</span></a>
                    <div class="clear"></div>
                  </div>
                </div>
              </div-->
            </div>
            <div class="title">
              Статистика
            </div>
            <div class="modules_right">
            </div>
          </div>
              <div id="table" class="help">
              <div class="col w10 last">
                <div class="content">

                  </div>
              </div>
              <div class="col w5">
              <div class="content">
                <div class="box">
                  <div class="head"><div></div></div>
                  <center><h2>Последние резервные копии</h2></center>
                      <div class="desc" style="height:250px;min-height:250px; overflow: auto;">
                        <? 
                            $dir = $dump_dir;   //задаём имя директории
                            if(is_dir($dir)) {   //проверяем наличие директории
                                echo '<b>'.$dir.'</b> - директория с резервными копиями;<br>';
                                 $files = scandir($dir);    //сканируем (получаем массив файлов)
                                 array_shift($files); // удаляем из массива '.'
                                 array_shift($files); // удаляем из массива '..'
                                 for($i=0; $i<sizeof($files); $i++) echo '-<b>Резервная копия:</b> '.$files[$i].'<br/>';  //выводим все файлы
                            }
                            else echo $dir.' -такой директории нет;<br>';
                        ?>
                        </div>
                       <div class="bottom">
                      <div></div>
                    </div>
                  </div> 
                </div>
              </div>
              <div class="col w5">
              <div class="content">
                <div class="box">
                  <div class="head"><div></div></div>
                  <center><h2>Создать резервную копию</h2></center>
                  <div class="desc" style="height:250px;min-height:250px; overflow: auto;">
                    <form action='' method="get" >
                      <strong>Директория с копиями: </strong><? echo $dump_dir; ?><br />
                      <strong>Имя будущей копии: </strong><? echo $filesql.".sql"; ?><br/>
                      <input type="submit" method="get" name="sql" value="Backup">
                    </form>
                  </div>
                   <div class="bottom">
                  <div></div>
                </div>
              </div> 
            </div>
          </div>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div id="body_footer">
              <div id="bottom_left"><div id="bottom_right"></div></div>
            </div>
          </div>
        </div>
      </div>
      <div class="clear-both"></div>
    </div>
    <div id="footer">
        <div class="center-wrapper">
     
            <div class="one-fifth text-align-center">
                <a class="" href="#"><img width="135px" src="style/images/logo.png" alt="" style=" border-radius: 10px;"></a>
                <hr>
                <p><em> Логистика и транспорт для требовательных клиентов.<br />У вас есть груз? У нас есть транспорт!</em></p>
                <hr>
                <ul class="social-icons align-center">
                    <li><a href="#"><img src="style/images/facebook.png" alt=""></a></li>
                    <li><a href="#"><img src="style/images/twitter.png" alt=""></a></li>
                    <li><a href="#"><img src="style/images/rss.png" alt=""></a></li>
                </ul><!-- end .social-icons -->
     
            </div><!-- end .one-fifth -->
     
            <div class="one-fifth">
                <h3>Навигация сайта</h3>
                <ul class="links-list">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Регистрация</a></li>
                    <li><a href="#">Поиск грузов</a></li>
                </ul><!-- end .links-list -->
            </div><!-- end .one-fifth -->
     
            <div class="one-fifth">
                <h3>Партнеры</h3>
                <ul class="links-list">
                    <li><a href="#">ENLAB.SU</a></li>
                </ul><!-- end .links-list -->
            </div><!-- end .one-fifth -->
     
            <div class="one-fifth">
                <h3>Реклама</h3>
                <ul class="links-list">
                    <li><a href="http://enlab.su/">Социальная барахолка</a></li>
                </ul><!-- end .links-list -->
            </div><!-- end .one-fifth -->
     
            <!--div class="one-fifth-last">
                <a href="#"><h3>Trailers</h3></a>
                <a href="#"><h3>Photos</h3></a>
                <a href="#"><h3>Contacts</h3></a>
            </div--><!-- end .one-fifth-last -->
            
      </div><!-- end .center-wrapper -->
    </div><!-- end #footer -->
    <!--<div style="font-size:12px">Copyright &copy; 2013 Avtoperevozki.kz. All rights reserved. Create CMS <a href="mailto:flashhacker1988@gmail.com">Alex M.A.K.</a></div-->
  </body>
</html>
  <?
}
}
?>
