<?php
require_once "../config.php";

/** 
 * Êëàññ äëÿ àâòîðèçàöèè
 * @author äèçàéí ñòóäèÿ ox2.ru 
 */ 
class AuthClass {
    private $_login = "shmadmin"; //Óñòàíàâëèâàåì ëîãèí
    private $_password = "Qq123456"; //Óñòàíàâëèâàåì ïàðîëü

    /**
     * Ïðîâåðÿåò, àâòîðèçîâàí ïîëüçîâàòåëü èëè íåò
     * Âîçâðàùàåò true åñëè àâòîðèçîâàí, èíà÷å false
     * @return boolean 
     */
    public function isAuth() {
        if (isset($_SESSION["is_auth"])) { //Åñëè ñåññèÿ ñóùåñòâóåò
            return $_SESSION["is_auth"]; //Âîçâðàùàåì çíà÷åíèå ïåðåìåííîé ñåññèè is_auth (õðàíèò true åñëè àâòîðèçîâàí, false åñëè íå àâòîðèçîâàí)
        }
        else return false; //Ïîëüçîâàòåëü íå àâòîðèçîâàí, ò.ê. ïåðåìåííàÿ is_auth íå ñîçäàíà
    }
    
    /**
     * Àâòîðèçàöèÿ ïîëüçîâàòåëÿ
     * @param string $login
     * @param string $passwors 
     */
    public function auth($login, $passwors) {
        if ($login == $this->_login && $passwors == $this->_password) { //Åñëè ëîãèí è ïàðîëü ââåäåíû ïðàâèëüíî
            $_SESSION["is_auth"] = true; //Äåëàåì ïîëüçîâàòåëÿ àâòîðèçîâàííûì
            $_SESSION["login"] = $login; //Çàïèñûâàåì â ñåññèþ ëîãèí ïîëüçîâàòåëÿ
            return true;
        }
        else { //Ëîãèí è ïàðîëü íå ïîäîøåë
            $_SESSION["is_auth"] = false;
            return false; 
        }
    }
    
    /**
     * Ìåòîä âîçâðàùàåò ëîãèí àâòîðèçîâàííîãî ïîëüçîâàòåëÿ 
     */
    public function getLogin() {
        if ($this->isAuth()) { //Åñëè ïîëüçîâàòåëü àâòîðèçîâàí
            return $_SESSION["login"]; //Âîçâðàùàåì ëîãèí, êîòîðûé çàïèñàí â ñåññèþ
        }
    }
    
    
    public function out() {
        $_SESSION = array(); //Î÷èùàåì ñåññèþ
        session_destroy(); //Óíè÷òîæàåì
    }
}

$auth = new AuthClass();

if (isset($_POST["login"]) && isset($_POST["password"])) { //Åñëè ëîãèí è ïàðîëü áûëè îòïðàâëåíû
    if (!$auth->auth($_POST["login"], $_POST["password"])) { //Åñëè ëîãèí è ïàðîëü ââåäåí íå ïðàâèëüíî
        echo "<h2 style=\"color:red;\">Ëîãèí è ïàðîëü ââåäåí íå ïðàâèëüíî!</h2>";
    }
}

if (isset($_GET["is_exit"])) { //Åñëè íàæàòà êíîïêà âûõîäà
    if ($_GET["is_exit"] == 1) {
        $auth->out(); //Âûõîäèì
        header("Location: ?is_exit=0"); //Ðåäèðåêò ïîñëå âûõîäà
    }
}

?>

<?php if ($auth->isAuth()) { 
if ($_GET['type']=="account" && $_GET['type']=="delete" && isset($_GET['id']))
{
	echo "<h1>FAAAAAAAAAAAAAACK</h1>";
	$id=intval($_GET['id']);
	//echo "here $id";
	if($db->query("DELETE FROM `accounts` WHERE `id`='$id'"))
	{
		//echo "here";
		header("Location:index.php");
		exit();
	} else {
		header("Location:index.php?error=db")
	}
} elseif ($_GET['type']=="cargo" && isset($_GET['id'])) {
	$id=intval($_GET['id']);
	if($db->query("DELETE FROM `cargo` WHERE `id`='$id'"))
	{
		//echo "here";
		header("Location:cargo.php");
		exit();
	}	
} elseif ($_GET['type'])=="transport" && isset($_GET['id']) {
	$id=intval($_GET['id']);
	if($db->query("DELETE FROM `transport` WHERE `id`='$id'"))
	{
		//echo "here";
		header("Location:transport.php");
		exit();
	}	
}
}

?>