<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once "libs/lib.php";
if(!isset($_SESSION['user']))
{
	header("Location:index.php");
	exit();
}
if(!isset($_GET['id'])) exit();

if (isset($_GET['action']) && $_GET['action']=="save" && isset($_GET['id'])) {

	$origin_raw=$_POST['origin'];
	foreach($origin_raw as $o)
	{
		if(intval($o)>0) $origin[]=intval($o);
	}
	if(count($origin)==0)
	{
		header("Location:index.php?page=newtransport&error=noorigin");
		exit();
	}

	$destination_raw=$_POST['destination'];
	foreach($destination_raw as $d)
	{
		if(intval($d)>0) $destination[]=intval($d);
	}
	if(count($destination)==0)
	{
		header("Location:index.php?page=newtransport&error=nodestination");
		exit();
	}

	$id=intval($_GET['id']);
	$transport_type=intval($_POST['transports_type']);
	$car_amount=intval($_POST['car_amount']);
	$weight=floatval($_POST['weight']);
	$volume=floatval($_POST['volume']);
	$length=floatval($_POST['length']);
	$width=floatval($_POST['width']);
	$height=floatval($_POST['height']);
	$price=floatval($_POST['price']);
	
	if(!($date_from=dateToDB($_POST['date_from'])))
	{
		header("Location:index.php?page=edittransport&error=date");
		exit();
	}
	if(!($date_due=dateToDB($_POST['date_due'])))
	{
		header("Location:index.php?page=edittransport&error=date");
		exit();
	}

	if($db->query("UPDATE transport SET load_from='$date_from', load_due='$date_due', transport_required='$transport_type', weight='$weight', volume='$volume', vehicles_number='$car_amount', length='$length', width='$width', height='$height', price='$price' WHERE id='$id'")) 
	{
    	  $db->rollback();
	  header("Location:index.php?page=mytransport");
	  exit();
	}
	$brand=htmlspecialchars(trim($_POST['brand']),ENT_QUOTES);
	$capacity=intval($_POST['capacity']);
	$seats=htmlspecialchars(trim($_POST['seats']),ENT_QUOTES);
	$information=htmlspecialchars(trim($_POST['information']),ENT_QUOTES);
	$freight_id=$db->insertId();
	if(!$db->query("UDPATE `transport_characteristics` SET `brand`='$brand', `capacity`='$capacity', `information`='$information', `seats`='$seats' WHERE `transport_id`='$freight_id'"))
	{
		$db->rollback();
		echo "<h1>".$id."</h1>";
		echo "<h1>".$brand."</h1>";
		echo "<h1>".$capacity."</h1>";
		echo "<h1>".$seats."</h1>";
		echo "<h1>".$information."</h1>";
		//header("Location:index.php?page=newtransport&error=transport_characteristics");
		exit();
	}

	
	foreach($origin as $o)
	{
		if(!$db->query("UPDATE `transport_origins` SET `transport_id`='$freight_id',`origin`='$o' WHERE `id`='$id'"))
		{
			$db->rollback();
			header("Location:index.php?page=edittransport&error=db&2");
			exit();
		}
	}
	foreach($destination as $d)
	{
		if(!$db->query("UPDATE `transport_destinations` SET `transport_id`='$freight_id',`destination`='$d' WHERE `id`='$id'"))
		{
			$db->rollback();
			header("Location:index.php?page=edittransport&error=db&1");
			exit();
		}
	}
	$db->commit();
	header("Location:index.php?page=mytransport");
	/*$db->startTransaction();
	if($db->query("UPDATE cargo SET price='$price' WHERE id='$id'"))
	{
		$db->rollback();
		header("Location:index.php?page=edittransport&error=db");
		exit();
	} else {
		header("Location:index.php?page=mytransport");
	}
	$db->commit();*/
	/*$db="UPDATE cargo SET cargo_name='$name', transport_required='$transport_type', weight='$weight', volume='$volume', vehicles_number='$car_amount', length='$length', width='$width', height='$height', price='$price' WHERE id='$id'";
	if ($db) {
		echo "<h1>cdscdslckmds</h1>";
	} else {
		echo "<h1>FAAAAACK</h1>";
	}*/
}

	$db2=new DataBase($db);
	$db->query("SELECT `id`, `load_from`, `load_due`, `transport_required`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `transport` WHERE `account_id`='{$_SESSION['user']['id']}' ORDER BY `load_due` DESC");
	if($db->numRows()==0)
	{
		$smarty->assign("notransport","true");
	}
	$transport_arr=array();
	while(list($id, $date_from, $date_due, $transport_type, $weight, $volume, $car_amount, $length, $width, $height, $price)=$db->fetchRow())
	{
		$transport_arr[$id]['date_from']=$date_from;
		$transport_arr[$id]['date_due']=$date_due;
		$transport_arr[$id]['transport_type']=$transport_type;
		$transport_arr[$id]['weight']=$weight;
		$transport_arr[$id]['volume']=$volume;
		$transport_arr[$id]['car_amount']=$car_amount;
		$transport_arr[$id]['length']=$length;
		$transport_arr[$id]['width']=$width;
		$transport_arr[$id]['height']=$height;
		$transport_arr[$id]['price']=$price;
		$db2->query("SELECT `city_name_ru` FROM `transport_origins`,`city_` WHERE `transport_origins`.`transport_id`='$id' AND `transport_origins`.`origin`=`city_`.`id` ORDER BY `transport_origins`.`id`");
		while(list($o)=$db2->fetchRow()) $transport_arr[$id]['origins'][]=$o;
		$db2->query("SELECT `city_name_ru` FROM `transport_destinations`,`city_` WHERE `transport_destinations`.`transport_id`='$id' AND `transport_destinations`.`destination`=`city_`.`id` ORDER BY `transport_destinations`.`id`");
		while(list($d)=$db2->fetchRow()) $transport_arr[$id]['destinations'][]=$d;
		$db2->query("SELECT `brand`, `capacity`, `seats`, `information` FROM `transport_characteristics` WHERE `transport_id`='$id'");
		while(list($brand, $capacity, $seats, $information)=$db2->fetchRow())
		{
			$transport_arr[$id]['brand']=$brand;
			$transport_arr[$id]['capacity']=$capacity;
			$transport_arr[$id]['seats'][]=$seats;
			$transport_arr[$id]['information'][]=$information;
		}
	}
// Get countries from database
$db->query("SELECT `id`, `country_name_ru` FROM `country_`");
while(list($id,$name)=$db->fetchRow()) $countries_list[$id]=$name;
$smarty->assign("from_country",$countries_list);

// Get kazakhstan cities from database. I used DISTINCT coz apparently there are duplicate names for cities in city_ table so far. for instance Baikonur is mentioned 3 times, two of which for Kazakhstan
$db->query("SELECT `id`, `city_name_ru` FROM `city_` WHERE `id_country`='222' GROUP BY `city_name_ru` ORDER BY `city_name_ru`");
while(list($id,$name)=$db->fetchRow()) $city_list[$id]=$name;
$smarty->assign("from_city",$city_list);

$smarty->assign("current_date",date("d.m.Y"));
$smarty->assign("due_date",date("d.m.Y",time()+259200));


/*
echo "<pre>";
print_r($transport_arr);
echo "</pre>";
*/
// Get transport types from database
$db->query("SELECT `id`,`name` FROM `transport_modes`");
while(list($id,$name)=$db->fetchRow()) $transport[$id]=$name;
$smarty->assign("transport_modes",$transport);

// Get transport types from database
$db->query("SELECT `id`,`name` FROM `transport_types`");
while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
$smarty->assign("transports_type",$transports);


$smarty->assign("transports",$transport_arr);
?>
