<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once "libs/lib.php";
if(!isset($_SESSION['user']))
{
	header("Location:index.php");
	exit();
}
if(!isset($_GET['id'])) exit();

if(isset($_GET['action']) && $_GET['action']=="edit" && isset($_GET['id'])) {
	$id=intval($_GET['id']);
	$db2=new DataBase($db);
	$db->query("SELECT `id`, `load_from`, `load_due`, `cargo_name`, `additional_info`, `transport_required`, `transport_modes`, `canconsolidate`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `cargo` WHERE `id`='$id'");
	$cargo_arr=array();
	while(list( $id, $date_from,  $date_due,  $name,  $info, $transport_type, $transport_modes, $canconsolidate,  $weight,  $volume,  $car_amount,  $length,  $width,  $height,  $price)=$db->fetchRow())
	{
		$cargo_arr[$id]['id']=$id;
		$cargo_arr[$id]['date_from']=$date_from;
		$cargo_arr[$id]['date_due']=$date_due;
		$cargo_arr[$id]['name']=$name;
		$cargo_arr[$id]['info']=$info;
		$cargo_arr[$id]['transport_type']=$transport_type;
		$cargo_arr[$id]['transport_modes']=$transport_modes;
		$cargo_arr[$id]['canconsolidate']=($canconsolidate==1)?'on':'off';
		$cargo_arr[$id]['weight']=$weight;
		$cargo_arr[$id]['volume']=$volume;
		$cargo_arr[$id]['car_amount']=$car_amount;
		$cargo_arr[$id]['length']=$length;
		$cargo_arr[$id]['width']=$width;
		$cargo_arr[$id]['height']=$height;
		$cargo_arr[$id]['price']=$price;
	}
	
} else if (isset($_GET['action']) && isset($_GET['action'])=="save" && isset($_GET['id'])) {
	$origin_raw=$_POST['origin'];
	foreach($origin_raw as $o)
	{
		if(intval($o)>0) $origin[]=intval($o);
	}
	if(count($origin)==0)
	{
		header("Location:index.php?page=editfreight&error=noorigin");
		exit();
	}
	$destination_raw=$_POST['destination'];
	foreach($destination_raw as $d)
	{
		if(intval($d)>0) $destination[]=intval($d);
	}
	if(count($destination)==0)
	{
		header("Location:index.php?page=editfreight&error=nodestination");
		exit();
	}

	if(!($date_from=dateToDB($_POST['date_from'])))
	{
		header("Location:index.php?page=editfreight&error=date");
		exit();
	}
	if(!($date_due=dateToDB($_POST['date_due'])))
	{
		header("Location:index.php?page=editfreight&error=date");
		exit();
	}
	$name=htmlspecialchars(trim($_POST['name']),ENT_QUOTES);
	if(strlen($name)==0)
	{
		header("Location:index.php?page=editfreight&error=noname");
		exit();
	}
	$additional_info=htmlspecialchars(trim($_POST['additional_info']),ENT_QUOTES);
	if(strlen($name)==0)
	{
		header("Location:index.php?page=editfreight&error=noname");
		exit();
	}
	
	$transport_type=intval($_POST['transports_types']);
	$transport_modes=intval($_POST['transports_modes']);
	$canconsolidate = (intval(isset($_POST['canconsolidate']))&&intval($_POST['canconsolidate'] == 'on'))? 1 : 0;
	$car_amount=intval($_POST['car_amount']);
	$weight=floatval($_POST['weight']);
	$volume=floatval($_POST['volume']);
	$length=floatval($_POST['length']);
	$width=floatval($_POST['width']);
	$height=floatval($_POST['height']);
	$price=floatval($_POST['price']);
	$id=intval($_GET['id']);
	$db2=new DataBase($db);
	foreach($origin as $o)
	{
		$db->query("UPDATE `cargo_origins` SET `origin`='$o' WHERE `cargo_id`='$id'");
	}
	foreach($destination as $d)
	{
		$db->query("UPDATE `cargo_destinations` SET `destination`='$d' WHERE `cargo_id`='$id'");
	}
	//$db2->commit();

	if($db->query("UPDATE cargo SET load_from='$date_from', load_due='$date_due', cargo_name='$name', additional_info='$additional_info', transport_required='$transport_type', transport_modes='$transport_modes', canconsolidate='$canconsolidate', weight='$weight', volume='$volume', vehicles_number='$car_amount', length='$length', width='$width', height='$height', price='$price' WHERE id='$id'")) 
	{
      	$db->rollback();
	  	header("Location:index.php?page=mycargo");
	  	exit();
	}
	$db->commit();
	
	header("Location:index.php?page=mycargo");
}		// Get countries from database
		//$countries_list[-1]="любой";
		$db->query("SELECT `id`, `country_name_ru` FROM `country_` ORDER BY IF(id >= 214, 0, 1) DESC, id>222 ASC");
		while(list($id,$name)=$db->fetchRow()) $countries_list[$id]=$name;
		$smarty->assign("from_country",$countries_list);

		$city_list[-1]="любой";
		// Get kazakhstan cities from database. I used DISTINCT coz apparently there are duplicate names for cities in city_ table so far. for instance Baikonur is mentioned 3 times, two of which for Kazakhstan
		$db->query("SELECT `id`, `city_name_ru` FROM `city_` WHERE `id_country`='222' GROUP BY `city_name_ru` ORDER BY `city_name_ru`");
		while(list($id,$name)=$db->fetchRow()) $city_list[$id]=$name;
		$smarty->assign("from_city",$city_list);

		$smarty->assign("current_date",date("d.m.Y"));
		$smarty->assign("due_date",date("d.m.Y",time()+86400*3));

		// Get transport modes from database
		$db->query("SELECT `id`,`name` FROM `transport_modes`");
		while(list($id,$name)=$db->fetchRow()) $transport_modes[$id]=$name;
		$smarty->assign("transport_modes",$transport_modes);

		// Get transport types from database
		$db->query("SELECT `id`,`name` FROM `transport_types`");
		while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
		$smarty->assign("transports",$transports);


		/*
		echo "<pre>";
		print_r($cargo_arr);
		echo "</pre>";
		*/
		$smarty->assign("cargos",$cargo_arr);

?>
