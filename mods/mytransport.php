<?php

if(!isset($_SESSION['user']))
{
	header("Location:index.php");
	exit();
}

if(isset($_GET['action']) && $_GET['action']=="delete" && isset($_GET['id']))
{
	$id=intval($_GET['id']);
	//echo "DELETE FROM `cargo` WHERE `id`='$id'";
	$sql="DELETE FROM `transport` WHERE `id`='$id'";
	//echo $sql;
	$db->query($sql); //make sure triggers are installed to delete origins and destinations from separate table
	header("Location:index.php?page=mytransport");
	exit();
}

$db2=new DataBase($db);
$db->query("SELECT `id`,`name` FROM `transport_types`");
while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
$db->query("SELECT `id`,`name` FROM `transport_modes`");
while(list($id,$name)=$db->fetchRow()) $transportss[$id]=$name;

$db->query("SELECT `id`, `account_id`, `load_from`, `load_due`, `transport_required`, `transport_modes`, `notgabarite`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `transport` WHERE `account_id`='{$_SESSION['user']['id']}' ORDER BY `load_due` DESC");
if($db->numRows()==0)
{
	$smarty->assign("notransport","true");
}
$transport_arr=array();
while(list($id, $account_id, $date_from, $date_due, $transports_type, $transports_modes, $notgabarite, $weight, $volume, $car_amount, $length, $width, $height, $price)=$db->fetchRow())
{
	$transport_arr[$id]['date_from']=$date_from;
	$transport_arr[$id]['date_due']=$date_due;
	$transport_arr[$id]['transports_type']=$transports[$transports_type];
	$transport_arr[$id]['transports_modes']=$transportss[$transports_modes];
	$transport_arr[$id]['notgabarite']=$notgabarite;
	$transport_arr[$id]['weight']=$weight;
	$transport_arr[$id]['volume']=$volume;
	$transport_arr[$id]['car_amount']=$car_amount;
	$transport_arr[$id]['length']=$length;
	$transport_arr[$id]['width']=$width;
	$transport_arr[$id]['height']=$height;
	if($price) $transport_arr[$id]['price']=number_format($price, 0, ',', " ");
	$db2->query("SELECT `city_name_ru` FROM `transport_origins`,`city_` WHERE `transport_origins`.`transport_id`='$id' AND `transport_origins`.`origin`=`city_`.`id` ORDER BY `transport_origins`.`id`");
	while(list($o)=$db2->fetchRow()) $transport_arr[$id]['origins'][]=$o;
	$db2->query("SELECT `city_name_ru` FROM `transport_destinations`,`city_` WHERE `transport_destinations`.`transport_id`='$id' AND `transport_destinations`.`destination`=`city_`.`id` ORDER BY `transport_destinations`.`id`");
	while(list($d)=$db2->fetchRow()) $transport_arr[$id]['destinations'][]=$d;
	$db2->query("SELECT `name`,`email`,`contact_text` FROM `accounts`,`contacts` WHERE `accounts`.`id`=`contacts`.`account_id` AND `accounts`.`id`='$account_id'");
	while(list($name,$email,$contact)=$db2->fetchRow())
	{
             $transport_arr[$id]['contact_name']=$name;
             $transport_arr[$id]['contact_email']=$email;
             $transport_arr[$id]['contact_phones'][]=$contact;
	}
	$db2->query("SELECT `brand`,`capacity`, `seats`, `information` FROM `transport_characteristics` WHERE `id`='$id'");
	while(list($brand,$capacity,$seats,$information)=$db2->fetchRow())
	{
             $transport_arr[$id]['brand']=$brand;
             $transport_arr[$id]['capacity']=$capacity;
             $transport_arr[$id]['seats']=$seats;
             $transport_arr[$id]['information']=$information;
	}
}
/*
echo "<pre>";
print_r($transport_arr);
echo "</pre>";
*/
$smarty->assign("transports",$transport_arr);

?>
