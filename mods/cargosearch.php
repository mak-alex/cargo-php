<?php

require_once "libs/lib.php";

if(isset($_GET['from_city']))
{
	
	//filter
	$db2=new DataBase($db);
	$db->query("SELECT `id`,`name` FROM `transport_types`");
	while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
	
	$db->query("SELECT `id`,`name` FROM `transport_modes`");
	while(list($id,$name)=$db->fetchRow()) $transportss[$id]=$name;
	
	if(!($date_from=dateToDB($_GET['date_from'])))
	{
		//header("Location:index.php?page=newfreight&error=date");
		exit("1");
	}
	if(!($date_due=dateToDB($_GET['date_to'])))
	{
		//header("Location:index.php?page=newfreight&error=date");
		exit("2 $date_from $date_due");
	}
	
	$ADDITIONAL_SQL=" AND (`cargo`.`load_from` BETWEEN '$date_from' AND '$date_due') AND (`cargo`.`load_due` BETWEEN '$date_from' AND '$date_due') ";
	
	/*
	$weight=floatval($_GET['weight']);
	$volume=floatval($_GET['volume']);
	if($weight!=0) $ADDITIONAL_SQL.=" AND `cargo`.`volume`='$volume' ";
	if($volume!=0) $ADDITIONAL_SQL.=" AND `cargo`.`weight`='$volume' ";
	*/
	
	$weight_from=	floatval($_GET['weight_from']);
	$weight_to=		floatval($_GET['weight_to']);
	$volume_from=	floatval($_GET['volume_from']);
	$volume_to=		floatval($_GET['volume_to']);
	if($weight_to>0) $ADDITIONAL_SQL.=" AND `cargo`.`weight` BETWEEN '$weight_from' AND '$weight_to' ";
	if($volume_to>0) $ADDITIONAL_SQL.=" AND `cargo`.`volume` BETWEEN '$volume_from' AND '$volume_to' ";
	
	//echo "here";
	//if city of origin is given
	if(isset($_GET['from_city']) && intval($_GET['from_city'])>1)
	{
		$from_city=intval($_GET['from_city']);
		$ADDITIONAL_SQL.=" AND `cargo_origins_text`.`city_id`='$from_city' ";
	}
	elseif(isset($_GET['from_region']) && intval($_GET['from_region'])>0)
	{
		//if no city given but we have a country
		$from_region=intval($_GET['from_region']);
		$ADDITIONAL_SQL.=" AND `cargo_origins_text`.`region_id`='$from_region' ";
	}
	elseif(isset($_GET['from_country']) && intval($_GET['from_country'])>0)
	{
		//if no city given but we have a country
		$from_country=intval($_GET['from_country']);
		$ADDITIONAL_SQL.=" AND `cargo_origins_text`.`country_id`='$from_country' ";
	}
	//if city of destination is given
	if(isset($_GET['to_city']) && intval($_GET['to_city'])!=-1)
	{
		$to_city=intval($_GET['from_city']);
		$ADDITIONAL_SQL.=" AND `cargo_destinations_text`.`city_id`='$to_city' ";
	}
	elseif(isset($_GET['to_region']) && intval($_GET['to_region'])>0)
	{
		//if no city given but we have a country
		$to_region=intval($_GET['to_region']);
		$ADDITIONAL_SQL.=" AND `cargo_destinations_text`.`region_id`='$to_region' ";
	}
	elseif(isset($_GET['to_country']) && intval($_GET['to_country'])>0)
	{
		//if no city given but we have a country
		$to_country=intval($_GET['to_country']);
		$ADDITIONAL_SQL.=" AND `cargo_destinations_text`.`country_id`='$to_country' ";
	}

	$sql="
	SELECT
		`cargo`.`id`, `cargo`.`account_id`, `cargo`.`load_from`, `cargo`.`load_due`, `cargo`.`cargo_name`,  `cargo`.`transport_required`,`cargo`.`transport_modes`,`cargo`.`additional_info`, `cargo`.`canconsolidate`, `cargo`.`weight`, `cargo`.`volume`, `cargo`.`vehicles_number`, `cargo`.`length`, `cargo`.`width`, `cargo`.`height`, `cargo`.`price`
	FROM
		`cargo`,`cargo_origins_text`,`cargo_destinations_text`
	WHERE `cargo`.`id`=`cargo_origins_text`.`id` AND `cargo`.`id`=`cargo_destinations_text`.`id`
		$ADDITIONAL_SQL
	GROUP BY `cargo`.`id` ORDER BY `cargo`.`load_due` DESC LIMIT 30";
	
	//echo $sql;
	//"SELECT `id`, `account_id`, `load_from`, `load_due`, `cargo_name`, `transport_required`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `cargo` ORDER BY `load_due` DESC LIMIT 30"
	$db->query($sql);
	
	if($db->numRows()>0)
	{
		
		$cargo_arr=array();
		while(list($id, $account_id, $load_from, $load_due, $cargo_name, $transport_type, $transport_modes, $info, $canconsolidate, $weight, $volume, $vehicles_number, $length, $width, $height, $price)=$db->fetchRow())
		{
			$cargo_arr[$id]['account_id']=$account_id;
			$cargo_arr[$id]['load_from']=$load_from;
			$cargo_arr[$id]['load_due']=$load_due;
			$cargo_arr[$id]['cargo_name']=$cargo_name;
			$cargo_arr[$id]['transport_type']=$transports[$transport_type];
			$cargo_arr[$id]['transport_modes']=$transportss[$transport_modes];
			$cargo_arr[$id]['info']=$info;
			$cargo_arr[$id]['canconsolidate']=($canconsolidate==1)?'on':'off';
			$cargo_arr[$id]['weight']=$weight;
			$cargo_arr[$id]['volume']=$volume;
			$cargo_arr[$id]['vehicles_number']=$vehicles_number;
			$cargo_arr[$id]['length']=$length;
			$cargo_arr[$id]['width']=$width;
			$cargo_arr[$id]['height']=$height;
			if($price) $cargo_arr[$id]['price']=number_format($price, 0, ',', " ");
			$db2->query("SELECT `city_name_ru` FROM `cargo_origins`,`city_` WHERE `cargo_origins`.`cargo_id`='$id' AND `cargo_origins`.`origin`=`city_`.`id` ORDER BY `cargo_origins`.`id`");
			while(list($o)=$db2->fetchRow()) $cargo_arr[$id]['origins'][]=$o;
			$db2->query("SELECT `city_name_ru` FROM `cargo_destinations`,`city_` WHERE `cargo_destinations`.`cargo_id`='$id' AND `cargo_destinations`.`destination`=`city_`.`id` ORDER BY `cargo_destinations`.`id`");
			while(list($d)=$db2->fetchRow()) $cargo_arr[$id]['destinations'][]=$d;
			$db2->query("SELECT `name`,`email`,`contact_text` FROM `accounts`,`contacts` WHERE `accounts`.`id`=`contacts`.`account_id` AND `accounts`.`id`='$account_id'");
			while(list($name,$email,$contact)=$db2->fetchRow())
			{
				$cargo_arr[$id]['contact_name']=$name;
				$cargo_arr[$id]['contact_email']=$email;
				$cargo_arr[$id]['contact_phones'][]=$contact;
			}
		}
		$smarty->assign("cargos",$cargo_arr);
	}
	
	
	if(isset($_GET['ajax']))
	{
		$smarty->display("cargosearch_results.tpl");
		exit();
	}
}

// Get countries from database
$countries_list[-1]="любая";
$db->query("SELECT `id`,`country_name_ru` FROM `country_`");
while(list($id,$name)=$db->fetchRow()) $countries_list[$id]=$name;
$smarty->assign("from_country",$countries_list);

$city_list[-1]="любая";
$smarty->assign("from_region",$city_list);

$city_list[-1]="любой";
$smarty->assign("from_city",$city_list);

$smarty->assign("current_date",date("d.m.Y"));
$smarty->assign("due_date",date("d.m.Y",time()+259200));

//get possible weights and volumes
$db->query("SELECT DISTINCT `weight` FROM  `cargo` WHERE `weight`>0 ORDER BY `weight`");
$possible_weights[0]="---";
while(list($weight)=$db->fetchRow()) $possible_weights[]=$weight;
$smarty->assign("possible_weights",$possible_weights);

$db->query("SELECT DISTINCT `volume` FROM  `cargo` WHERE `volume`>0 ORDER BY `volume`");
$possible_volumes[0]="---";
while(list($weight)=$db->fetchRow()) $possible_volumes[]=$weight;
$smarty->assign("possible_volumes",$possible_volumes);



?>