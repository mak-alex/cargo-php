<?php

if(!isset($_SESSION['user']))
{
	header("Location:index.php");
	exit();
}

if(isset($_GET['action']) && $_GET['action']=="newpassword")
{
	$old_password=mysql_escape_String($_POST['old_password']);
	$new_password=mysql_escape_String($_POST['new_password']);
	$new_password_again=mysql_escape_String($_POST['new_password_again']);
	if($new_password!=$new_password_again)
	{
		header("Location:index.php?page=account&error=donotmatch");
		exit();
	}
	$db->query("SELECT `id` FROM `accounts` WHERE `id`='{$_SESSION['user']['id']}' AND `password`=MD5('$old_password')");
	if($db->numRows()==0)
	{
		header("Location:index.php?page=account&error=incorrectoldpass");
		exit();
	}
	else
	{
		$db->query("UPDATE `accounts` SET `password`=MD5('$new_password') WHERE `id`='{$_SESSION['user']['id']}'");
		$smarty->assign("success","passwordchanged");
	}
} else if(isset($_GET['action']) && $_GET['action']=="changecontacts")
{
	//exit("here");
	$phone=$_POST['phone'];
	for($i=0;$i<count($phone);$i++)	$phone[$i]=htmlspecialchars($phone[$i],ENT_QUOTES);
	$db->startTransaction();
	if(!$db->query("DELETE FROM `contacts` WHERE `account_id`='{$_SESSION['user']['id']}'")) $db->rollback();
	foreach($phone as $p)
	{
		if(!$db->query("INSERT INTO `contacts` (`id`,`account_id`,`contact_text`) VALUES('0','{$_SESSION['user']['id']}','$p')"))
		{
			$db->rollback();
			header("Location:index.php?page=signup&error=db");
		}
	}
	$db->commit();
} else if(isset($_GET['action']) && isset($_GET['action'])=="update")
{
	//User clicked submit on account form
	//print_r($_POST); exit();
	$country_id=intval($_POST['country']);
	$city=htmlspecialchars($_POST['city'],ENT_QUOTES);
	$status_id=intval($_POST['legal_statuses_list']);
	$email=htmlspecialchars($_POST['email'],ENT_QUOTES);
	$name=htmlspecialchars($_POST['name'],ENT_QUOTES);
	$phone=$_POST['phone'];
	
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ header("Location:index.php?page=account&error=input"); exit(); }
	//check variables for security
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ header("Location:index.php?page=account&error=input"); exit(); }
	$db->query("SELECT `id` FROM `accounts` WHERE `email`='$email'");
	//if($db->numRows()>0){ header("Location:index.php?page=account&error=emailexists"); exit();}
	/*if($password!=$password2 || strlen($password)==0){ header("Location:index.php?page=account&error=input"); exit(); }
	if(strlen($password)==0){ header("Location:index.php?page=account&error=input"); exit(); }
           if(strlen($name)==0){ header("Location:index.php?page=account&error=input"); exit(); }*/
	for($i=0;$i<count($phone);$i++)	$phone[$i]=htmlspecialchars($phone[$i],ENT_QUOTES);
	//add items to database
	$db->startTransaction();
	if(!$db->query("UPDATE `accounts` SET `country_id`='$country_id', `city`='$city', `status_id`='$status_id', `email`='$email', `name`='$name' WHERE id='{$_SESSION['user']['id']}'"))
	{
		$db->rollback();
		header("Location:index.php?page=account&error=db");
		exit();
	}
	$account_id=$db->insertId();
	/*foreach($phone as $p)
	{
		if(!$db->query("UPDATE `contacts` SET `contact_text`='$p' WHERE account_id='$account_id"))
		{
			$db->rollback();
			header("Location:index.php?page=account&error=db");
		}
	}*/
	foreach($phone as $p)
	{
           $db->query("SELECT `id` FROM `contacts` WHERE `account_id`='{$_SESSION['user']['id']}' AND `contact_text`='$p'");
           if($db->numRows()==0)
           {
                   header("Location:index.php?page=account&error=incorrectoldpass");
                   exit();
           }
           else
           {
                   $db->query("UPDATE `contacts` SET `contact_text`='$p WHERE `account_id`='{$_SESSION['user']['id']}'");
                   $smarty->assign("success","passwordchanged");
           }
	}
	$db->commit();
	header("Location:index.php?page=account");
}

if(!$db->query("SELECT `email`,`city`, `country_id`,`status_id`,`name` FROM `accounts` WHERE `id`='{$_SESSION['user']['id']}'")) echo "db error getting name";
list($email,$my_country,$country_id,$status_id,$name)=$db->fetchRow();
$db->query("SELECT `contact_text` FROM `contacts` WHERE `account_id`='{$_SESSION['user']['id']}'");
$my_phones=array();
while(list($contact)=$db->fetchRow()) $my_phones[]=$contact;

$smarty->assign("my_phones",$my_phones);
$smarty->assign("my_country",$my_country);
$smarty->assign("my_country_id",$country_id);
$smarty->assign("my_email",$email);
$smarty->assign("my_status_id",$status_id);
$smarty->assign("my_name",$name);

// Get countries from database
$db->query("SELECT `id`,`country_name_ru` FROM `country_` ORDER BY IF(id >= 214, 0, 1) DESC, id>222 ASC");
while(list($id,$name)=$db->fetchRow()) $countries_list[$id]=$name;
$smarty->assign("countries_list",$countries_list);

// Get legal statuses from database
$db->query("SELECT `id`,`status_name` FROM `legal_statuses`");
while(list($id,$name)=$db->fetchRow()) $legal_statuses_list[$id]=$name;
$smarty->assign("legal_statuses_list",$legal_statuses_list);



?>