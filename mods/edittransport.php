<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once "libs/lib.php";
if(!isset($_SESSION['user']))
{
	header("Location:index.php");
	exit();
}
if(!isset($_GET['id'])) exit();
if (isset($_GET['action']) && $_GET['action']=="edit" && isset($_GET['id'])) {
	$id=intval($_GET['id']);
	$db2=new DataBase($db);
	$db->query("SELECT `id`, `load_from`, `load_due`, `transport_required`, `transport_modes`, `notgabarite`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `transport` WHERE `id`='$id' ORDER BY `load_due` DESC");
	if($db->numRows()==0)
	{
		$smarty->assign("notransport","true");
	}
	$transport_arr=array();
	while(list($id, $date_from, $date_due, $transport_type, $transport_modes, $notgabarite, $weight, $volume, $car_amount, $length, $width, $height, $price)=$db->fetchRow())
	{
		$transport_arr[$id]['date_from']=$date_from;
		$transport_arr[$id]['date_due']=$date_due;
		$transport_arr[$id]['transport_type']=$transport_type;
		$transport_arr[$id]['transport_modes']=$transport_modes;
		$transport_arr[$id]['notgabarite']=($notgabarite==1)?'on':'off';
		$transport_arr[$id]['weight']=$weight;
		$transport_arr[$id]['volume']=$volume;
		$transport_arr[$id]['car_amount']=$car_amount;
		$transport_arr[$id]['length']=$length;
		$transport_arr[$id]['width']=$width;
		$transport_arr[$id]['height']=$height;
		$transport_arr[$id]['price']=$price;

		$db2->query("SELECT `origin` FROM `transport_origins` WHERE `transport_id`='$id' GROUP BY `origin` ORDER BY `origin`");
		while(list($o)=$db2->fetchRow()) $transport_arr[$id]['origins']=$o;
		echo $o;

		$db2->query("SELECT `destination` FROM `transport_destinations` WHERE `transport_id`='$id' GROUP BY `destination` ORDER BY `destination`");
		while(list($d)=$db2->fetchRow()) $transport_arr[$id]['destinations']=$d;

		$db2->query("SELECT `brand`, `capacity`, `seats`, `information` FROM `transport_characteristics` WHERE `transport_id`='$id' LIMIT 1");
		while(list($brand, $capacity, $seats, $information)=$db2->fetchRow())
		{
			$transport_arr[$id]['brand']=$brand;
			$transport_arr[$id]['capacity']=$capacity;
			$transport_arr[$id]['seats'][]=$seats;
			$transport_arr[$id]['information'][]=$information;
		}
	}
	// Get countries from database
	$db->query("SELECT `id`, `country_name_ru` FROM `country_`  ORDER BY IF(id >= 214, 0, 1) DESC, id>222 ASC");
	while(list($id,$name)=$db->fetchRow()) $countries_list[$id]=$name;
	$smarty->assign("from_country",$countries_list);

	if(!$db->query("SELECT `origin` FROM `transport_origins` WHERE `transport_id`='$id'")) echo "db error getting name";
	list($country_id)=$db->fetchRow();
	
	// Get kazakhstan cities from database. I used DISTINCT coz apparently there are duplicate names for cities in city_ table so far. for instance Baikonur is mentioned 3 times, two of which for Kazakhstan
	$city_list[-1]="любой";
	$db->query("SELECT `id`, `city_name_ru` FROM `city_` WHERE `id_country`='222' GROUP BY `city_name_ru` ORDER BY `city_name_ru`");
	while(list($id,$name)=$db->fetchRow()) $city_list[$id]=$name;
	$smarty->assign("from_city",$city_list);


	$smarty->assign("current_date",date("d.m.Y"));
	$smarty->assign("due_date",date("d.m.Y",time()+259200));
} elseif (isset($_GET['action']) && isset($_GET['action'])=="save" && isset($_GET['id'])) {
	$origin_raw=$_POST['origin'];
	foreach($origin_raw as $o)
	{
		if(intval($o)>0) $origin[]=intval($o);
	}
	if(count($origin)==0)
	{
		header("Location:index.php?page=editfreight&error=noorigin");
		exit();
	}
	$destination_raw=$_POST['destination'];
	foreach($destination_raw as $d)
	{
		if(intval($d)>0) $destination[]=intval($d);
	}
	if(count($destination)==0)
	{
		header("Location:index.php?page=editfreight&error=nodestination");
		exit();
	}

	if(!($date_from=dateToDB($_POST['date_from'])))
	{
		header("Location:index.php?page=edittransport&error=date");
		exit();
	}
	if(!($date_due=dateToDB($_POST['date_due'])))
	{
		header("Location:index.php?page=edittransport&error=date");
		exit();
	}

	
	$transport_type=intval($_POST['transports_type']);
	$transports_modes=intval($_POST['transports_modes']);
	//$notgabarite=intval($_POST['notgabarite'])? 1 : 0;
	$notgabarite = (intval(isset($_POST['notgabarite']))&&intval($_POST['notgabarite'] == 'on'))? 1 : 0;
	$car_amount=intval($_POST['car_amount']);
	$weight=floatval($_POST['weight']);
	$volume=floatval($_POST['volume']);
	$length=floatval($_POST['length']);
	$width=floatval($_POST['width']);
	$height=floatval($_POST['height']);
	$price=floatval($_POST['price']);
	$id=intval($_GET['id']);
	$db2=new DataBase($db);

	foreach($origin as $o)
	{
		$db->query("UPDATE `transport_origins` SET `origin`='$o' WHERE `transport_id`='$id'");
	}
	foreach($destination as $d)
	{
		$db->query("UPDATE `transport_destinations` SET `destination`='$d' WHERE `transport_id`='$id'");
	}

	
	if($db->query("UPDATE transport SET load_from='$date_from', load_due='$date_due', transport_required='$transport_type', transport_modes='$transports_modes', notgabarite='$notgabarite', weight='$weight', volume='$volume', vehicles_number='$car_amount', length='$length', width='$width', height='$height', price='$price' WHERE id='$id'")) 
	{
    	  $db->rollback();
	  header("Location:index.php?page=mytransport");
	  exit();
	}

	
	$freight_id=$db->insertId();
	$brand=htmlspecialchars(trim($_POST['brand']),ENT_QUOTES);
	$capacity=intval($_POST['capacity']);
	$seats=htmlspecialchars(trim($_POST['seats']),ENT_QUOTES);
	$information=htmlspecialchars(trim($_POST['information']),ENT_QUOTES);

	if(!$db->query("
		UDPATE `transport_characteristics` 
		`brand`='$brand', `capacity`='$capacity', `information`='$information', `seats`='$seats' WHERE `transport_id`='$freight_id'"))
	{
		$db->rollback();
		echo "<h1>".$freight_id."</h1>";
		echo "<h1>".$brand."</h1>";
		echo "<h1>".$capacity."</h1>";
		echo "<h1>".$seats."</h1>";
		echo "<h1>".$information."</h1>";
		//header("Location:index.php?page=newtransport&error=transport_characteristics");
		exit();
	}
	
	$db->commit();
	header("Location:index.php?page=mytransport");
	/*$db->startTransaction();
	if($db->query("UPDATE cargo SET price='$price' WHERE id='$id'"))
	{
		$db->rollback();
		header("Location:index.php?page=edittransport&error=db");
		exit();
	} else {
		header("Location:index.php?page=mytransport");
	}
	$db->commit();*/
	/*$db="UPDATE cargo SET cargo_name='$name', transport_required='$transport_type', weight='$weight', volume='$volume', vehicles_number='$car_amount', length='$length', width='$width', height='$height', price='$price' WHERE id='$id'";
	if ($db) {
		echo "<h1>cdscdslckmds</h1>";
	} else {
		echo "<h1>FAAAAACK</h1>";
	}*/
}
// Get countries from database
$db->query("SELECT `id`, `country_name_ru` FROM `country_` ORDER BY IF(id >= 214, 0, 1) DESC, id>222 ASC");
while(list($id,$name)=$db->fetchRow()) $countries_list[$id]=$name;
$smarty->assign("from_country",$countries_list);

//if(!$db->query("SELECT `origin` FROM `transport_origins` WHERE `transport_id`='$id'")) echo "db error getting name";
	//list($country_id)=$db->fetchRow();

// Get kazakhstan cities from database. I used DISTINCT coz apparently there are duplicate names for cities in city_ table so far. for instance Baikonur is mentioned 3 times, two of which for Kazakhstan
$city_list[-1]="любой";
$db->query("SELECT `id`, `city_name_ru` FROM `city_` WHERE `id_country`='222' GROUP BY `city_name_ru` ORDER BY `city_name_ru`");
while(list($id,$name)=$db->fetchRow()) $city_list[$id]=$name;
$smarty->assign("from_city",$city_list);

$smarty->assign("current_date",date("d.m.Y"));
$smarty->assign("due_date",date("d.m.Y",time()+259200));


/*
echo "<pre>";
print_r($transport_arr);
echo "</pre>";
*/
// Get transport types from database
$db->query("SELECT `id`,`name` FROM `transport_modes`");
while(list($id,$name)=$db->fetchRow()) $transport[$id]=$name;
$smarty->assign("transport_modes",$transport);

// Get transport types from database
$db->query("SELECT `id`,`name` FROM `transport_types`");
while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
$smarty->assign("transports_type",$transports);


$smarty->assign("transports",$transport_arr);
?>
