<?php
error_reporting(E_ALL);
if(!isset($_SESSION['user']))
{
	header("Location:index.php");
	exit();
}
if(!isset($_GET['id'])) exit();
//$id=intval($_GET['id']);

if($_GET['id']) {
	$db2=new DataBase($db);
	$db->query("SELECT `id`,`name` FROM `transport_types`");
	while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
	$id=intval($_GET['id']);
	$db->query("SELECT `id`, `account_id`, `load_from`, `load_due`, `cargo_name`, `transport_required`, `additional_info`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `cargo` WHERE `id`='$id'");
	$cargo_arr=array();
	while(list( $id, $account_id, $load_from, $load_due,  $cargo_name,  $transport_required, $info,  $weight,  $volume,  $vehicles_number,  $length,  $width,  $height,  $price)=$db->fetchRow())
	{
		$cargo_arr[$id]['id']=$id;
		$cargo_arr[$id]['load_from']=$load_from;
		$cargo_arr[$id]['load_due']=$load_due;
		$cargo_arr[$id]['cargo_name']=$cargo_name;
		$cargo_arr[$id]['transport_required']=$transports[$transport_required];
		$cargo_arr[$id]['weight']=$weight;
		$cargo_arr[$id]['volume']=$volume;
		$cargo_arr[$id]['vehicles_number']=$vehicles_number;
		$cargo_arr[$id]['length']=$length;
		$cargo_arr[$id]['width']=$width;
		$cargo_arr[$id]['height']=$height;
		$cargo_arr[$id]['info']=$info;
		if($price) $cargo_arr[$id]['price']=number_format($price, 0, ',', " ");

           $db2->query("SELECT `city_name_ru` FROM `cargo_origins`,`city_` WHERE `cargo_origins`.`cargo_id`='$id' AND `cargo_origins`.`origin`=`city_`.`id` ORDER BY `cargo_origins`.`id` LIMIT 1");
           while(list($o)=$db2->fetchRow()) $cargo_arr[$id]['origins'][]=$o;
           
           $db2->query("SELECT `city_name_ru` FROM `cargo_destinations`,`city_` WHERE `cargo_destinations`.`cargo_id`='$id' AND `cargo_destinations`.`destination`=`city_`.`id` ORDER BY `cargo_destinations`.`id` LIMIT 1");
           while(list($d)=$db2->fetchRow()) $cargo_arr[$id]['destinations'][]=$d;
           $db2->query("SELECT `name`,`email`,`contact_text` FROM `accounts`,`contacts` WHERE `accounts`.`id`=`contacts`.`account_id` AND `accounts`.`id`='$account_id'");
			while(list($name,$email,$contact)=$db2->fetchRow())
			{
				$cargo_arr[$id]['contact_name']=$name;
				$cargo_arr[$id]['contact_email']=$email;
				$cargo_arr[$id]['contact_phones'][]=$contact;
			}
	}

       $smarty->assign("cargos",$cargo_arr);
}
?>
