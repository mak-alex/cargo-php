<?php

if(!isset($_SESSION['user']))
{
	header("Location:index.php");
	exit();
}

require_once "libs/lib.php";

if(isset($_GET['action']) && $_GET['action']=="do")
{
	/*
	echo "<pre>";
	print_r($_POST);
	echo "</pre>";
	exit();
	*/
	//print_r($_POST); exit();
	if(!($date_from=dateToDB($_POST['date_from'])))
	{
		header("Location:index.php?page=newfreight&error=date");
		exit();
	}
	if(!($date_due=dateToDB($_POST['date_due'])))
	{
		header("Location:index.php?page=newfreight&error=date");
		exit();
	}
	$origin_raw=$_POST['origin'];
	foreach($origin_raw as $o)
	{
		if(intval($o)>0) $origin[]=intval($o);
	}
	if(count($origin)==0)
	{
		header("Location:index.php?page=newfreight&error=noorigin");
		exit();
	}
	$destination_raw=$_POST['destination'];
	foreach($destination_raw as $d)
	{
		if(intval($d)>0) $destination[]=intval($d);
	}
	

	if(count($destination)==0)
	{
		header("Location:index.php?page=newfreight&error=nodestination");
		exit();
	}
	$name=htmlspecialchars(trim($_POST['name']),ENT_QUOTES);
	if(strlen($name)==0)
	{
		header("Location:index.php?page=newfreight&error=noname");
		exit();
	}
	$additional_info=htmlspecialchars(trim($_POST['additional_info']),ENT_QUOTES);
	if(strlen($name)==0)
	{
		header("Location:index.php?page=editfreight&error=noname");
		exit();
	}
	$transport_type=intval($_POST['transports_types']);
	$transport_modes=intval($_POST['transports_modes']);
	$canconsolidate = (intval(isset($_POST['canconsolidate']))&&intval($_POST['canconsolidate'] == 'on'))? 1 : 0;
	$car_amount=intval($_POST['car_amount']);
	$weight=floatval($_POST['weight']);
	$volume=floatval($_POST['volume']);
	$length=floatval($_POST['length']);
	$width=floatval($_POST['width']);
	$height=floatval($_POST['height']);
	$price=floatval($_POST['price']);
	$db->startTransaction();
	if(!$db->query("
	INSERT INTO `cargo`
	(`id`, `account_id`, `load_from`, `load_due`, `cargo_name`, `transport_required`, `transport_modes`, `canconsolidate`, `additional_info`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price`)
	VALUES
	(NULL, '{$_SESSION['user']['id']}', '$date_from', '$date_due', '$name', '$transport_type', '$transport_modes', '$canconsolidate', '$additional_info', '$weight', '$volume', '$car_amount', '$length', '$width', '$height', '$price')"))
	{
		$db->rollback();
		header("Location:index.php?page=newfreight&error=db");
		exit();
	}
	$freight_id=$db->insertId();
	foreach($origin as $o)
	{
		if(!$db->query("INSERT INTO `cargo_origins` (`id`,`cargo_id`,`origin`) VALUES('0','$freight_id','$o')"))
		{
			$db->rollback();
			header("Location:index.php?page=newfreight&error=db&2");
			exit();
		}
	}
	foreach($destination as $d)
	{
		if(!$db->query("INSERT INTO `cargo_destinations` (`id`,`cargo_id`,`destination`) VALUES('0','$freight_id','$d')"))
		{
			$db->rollback();
			header("Location:index.php?page=newfreight&error=db&1");
			exit();
		}
	}
	$db->commit();
	header("Location:index.php?page=mycargo");
}

//$countries_list[-1]="любая";
// Get countries from database
$db->query("SELECT `id`, `country_name_ru` FROM `country_` ORDER BY IF(id >= 214, 0, 1) DESC, id>222 ASC");
while(list($id,$name)=$db->fetchRow()) $countries_list[$id]=$name;
$smarty->assign("from_country",$countries_list);

$city_list[-1]="любой";
// Get kazakhstan cities from database. I used DISTINCT coz apparently there are duplicate names for cities in city_ table so far. for instance Baikonur is mentioned 3 times, two of which for Kazakhstan
$db->query("SELECT `id`, `city_name_ru` FROM `city_` WHERE `id_country`='222' GROUP BY `city_name_ru` ORDER BY `city_name_ru`");
while(list($id,$name)=$db->fetchRow()) $city_list[$id]=$name;
$smarty->assign("from_city",$city_list);

$smarty->assign("current_date",date("d.m.Y"));
$smarty->assign("due_date",date("d.m.Y",time()+86400*3));

// Get transport modes from database
$db->query("SELECT `id`,`name` FROM `transport_modes`");
while(list($id,$name)=$db->fetchRow()) $transport_modes[$id]=$name;
$smarty->assign("transport_modes",$transport_modes);

// Get transport types from database
$db->query("SELECT `id`,`name` FROM `transport_types`");
while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
$smarty->assign("transports",$transports);

?>
