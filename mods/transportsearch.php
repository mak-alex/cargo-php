<?php

require_once "libs/lib.php";

if(isset($_GET['from_city']))
{
	//print_r($_GET);
	//filter
	$db2=new DataBase($db);
	$db->query("SELECT `id`,`name` FROM `transport_types`");
	while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
	
	$db->query("SELECT `id`,`name` FROM `transport_modes`");
	while(list($id,$name)=$db->fetchRow()) $transportss[$id]=$name;
	
	if(!($date_from=dateToDB($_GET['date_from'])))
	{
		//header("Location:index.php?page=newfreight&error=date");
		exit("1");
	}
	if(!($date_due=dateToDB($_GET['date_to'])))
	{
		//header("Location:index.php?page=newfreight&error=date");
		exit("2 $date_from $date_due");
	}
	
	$ADDITIONAL_SQL=" AND (`transport`.`load_from` BETWEEN '$date_from' AND '$date_due') AND (`transport`.`load_due` BETWEEN '$date_from' AND '$date_due') ";
	
	if(isset($_GET['weight']) && intval($_GET['weight'])>1)
	{
		$weight=floatval($_GET['weight']);
		if($weight!=0) $ADDITIONAL_SQL.=" AND `transport`.`volume`='$volume' ";
	}
	
	if(isset($_GET['volume']) && intval($_GET['volume'])>1)
	{
		$volume=floatval($_GET['volume']);
	    if($volume!=0) $ADDITIONAL_SQL.=" AND `transport`.`weight`='$volume' ";
	}
	
	
	//echo "here";
	//if city of origin is given
	if(isset($_GET['from_city']) && intval($_GET['from_city'])>1)
	{
		$from_city=intval($_GET['from_city']);
		$ADDITIONAL_SQL.=" AND `transport_origins_text`.`city_id`='$from_city' ";
	}
	elseif(isset($_GET['from_region']) && intval($_GET['from_region'])>0)
	{
		//if no city given but we have a country
		$from_region=intval($_GET['from_region']);
		$ADDITIONAL_SQL.=" AND `transport_origins_text`.`region_id`='$from_region' ";
	}
	elseif(isset($_GET['from_country']) && intval($_GET['from_country'])>0)
	{
		//if no city given but we have a country
		$from_country=intval($_GET['from_country']);
		$ADDITIONAL_SQL.=" AND `transport_origins_text`.`country_id`='$from_country' ";
	}
	//if city of destination is given
	if(isset($_GET['to_city']) && intval($_GET['to_city'])>0)
	{
		$to_city=intval($_GET['to_city']); 
		$ADDITIONAL_SQL.=" AND `transport_destinations_text`.`city_id`='$to_city' ";
	}
	elseif(isset($_GET['to_region']) && intval($_GET['to_region'])>0)
	{
		//if no city given but we have a country
		$to_region=intval($_GET['to_region']);
		$ADDITIONAL_SQL.=" AND `transport_destinations_text`.`region_id`='$to_region' ";
	}
	elseif(isset($_GET['to_country']) && intval($_GET['to_country'])>0)
	{
		//if no city given but we have a country
		$to_country=intval($_GET['to_country']);
		$ADDITIONAL_SQL.=" AND `transport_destinations_text`.`country_id`='$to_country' ";
	}

	$sql="
	SELECT
		`transport`.`id`, `transport`.`account_id`, `transport`.`load_from`, `transport`.`load_due`, `transport`.`transport_required`, `transport`.`transport_modes`, `transport`.`notgabarite`, `transport`.`weight`, `transport`.`volume`, `transport`.`vehicles_number`, `transport`.`length`, `transport`.`width`, `transport`.`height`, `transport`.`price`
	FROM
		`transport`,`transport_origins_text`,`transport_destinations_text`
	WHERE `transport`.`id`=`transport_origins_text`.`id` AND `transport`.`id`=`transport_destinations_text`.`id`
		$ADDITIONAL_SQL
	GROUP BY `transport`.`id` ORDER BY `transport`.`load_due` DESC LIMIT 30";
	//echo "<div>$sql</div>";
	//"SELECT `id`, `account_id`, `load_from`, `load_due`, `cargo_name`, `transport_required`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `cargo` ORDER BY `load_due` DESC LIMIT 30"
	$db->query($sql);
	
	if($db->numRows()>0)
	{
		
		$transport_arr=array();
		while(list($id, $account_id, $date_from, $date_due, $transports_type, $transports_mode, $notgabarite, $weight, $volume, $car_amount, $length, $width, $height, $price)=$db->fetchRow())
		{
			$transport_arr[$id]['account_id']=$account_id;
			$transport_arr[$id]['date_from']=$date_from;
			$transport_arr[$id]['date_due']=$date_due;
			$transport_arr[$id]['transports_type']=$transports[$transports_type];
			$transport_arr[$id]['transports_mode']=$transportss[$transports_mode];
			$transport_arr[$id]['notgabarite']=$notgabarite;//($notgabarite==1)?'on':'off';
			$transport_arr[$id]['weight']=$weight;
			$transport_arr[$id]['volume']=$volume;
			$transport_arr[$id]['car_amount']=$car_amount;
			$transport_arr[$id]['length']=$length;
			$transport_arr[$id]['width']=$width;
			$transport_arr[$id]['height']=$height;
			if($price) $transport_arr[$id]['price']=number_format($price, 0, ',', " ");
			$db2->query("SELECT `city_name_ru` FROM `transport_origins`,`city_` WHERE `transport_origins`.`transport_id`='$id' AND `transport_origins`.`origin`=`city_`.`id` ORDER BY `transport_origins`.`id`");
	while(list($o)=$db2->fetchRow()) $transport_arr[$id]['origins'][]=$o;
	$db2->query("SELECT `city_name_ru` FROM `transport_destinations`,`city_` WHERE `transport_destinations`.`transport_id`='$id' AND `transport_destinations`.`destination`=`city_`.`id` ORDER BY `transport_destinations`.`id`");
	while(list($d)=$db2->fetchRow()) $transport_arr[$id]['destinations'][]=$d;
	$db2->query("SELECT `name`,`email`,`contact_text` FROM `accounts`,`contacts` WHERE `accounts`.`id`=`contacts`.`account_id` AND `accounts`.`id`='$account_id'");
	while(list($name,$email,$contact)=$db2->fetchRow())
	{
             $transport_arr[$id]['contact_name']=$name;
             $transport_arr[$id]['contact_email']=$email;
             $transport_arr[$id]['contact_phones'][]=$contact;
	}
			$db2->query("SELECT `brand`,`capacity`, `seats`, `information` FROM `transport_characteristics` WHERE `id`='$id'");
			while(list($brand,$capacity,$seats,$information)=$db2->fetchRow())
			{
					 $transport_arr[$id]['brand']=$brand;
					 $transport_arr[$id]['capacity']=$capacity;
					 $transport_arr[$id]['seats']=$seats;
					 $transport_arr[$id]['information']=$information;
			}
		}
		$smarty->assign("transports",$transport_arr);
	}
	
	
	if(isset($_GET['ajax']))
	{
		$smarty->display("transportsearch_results.tpl");
		exit();
	}
}

// Get countries from database
$countries_list[-1]="любая";
$db->query("SELECT `id`,`country_name_ru` FROM `country_`");
while(list($id,$name)=$db->fetchRow()) $countries_list[$id]=$name;
$smarty->assign("from_country",$countries_list);

$region_list[-1]="любая";
$smarty->assign("from_region",$region_list);

$city_list[-1]="любой";
$smarty->assign("from_city",$city_list);

$smarty->assign("current_date",date("d.m.Y"));
$smarty->assign("due_date",date("d.m.Y",time()+259200));

//get possible weights and volumes
$db->query("SELECT DISTINCT `weight` FROM  `cargo` WHERE `weight`>0 ORDER BY `weight`");
$possible_weights[0]="---";
while(list($weight)=$db->fetchRow()) $possible_weights[]=$weight;
$smarty->assign("possible_weights",$possible_weights);

$db->query("SELECT DISTINCT `volume` FROM  `cargo` WHERE `volume`>0 ORDER BY `volume`");
$possible_volumes[0]="---";
while(list($weight)=$db->fetchRow()) $possible_volumes[]=$weight;
$smarty->assign("possible_volumes",$possible_volumes);



?>
