<?php

if(isset($_GET['action']) && $_GET['action']=="do")
{
	//User clicked submit on signup form
	$email=htmlspecialchars($_POST['signup_email'],ENT_QUOTES);
	$password=mysql_escape_string($_POST['signup_password']);
	$password2=mysql_escape_string($_POST['password_again']);
	$country_id=intval($_POST['country']);
	$city=htmlspecialchars($_POST['city'],ENT_QUOTES);
	$status_id=intval($_POST['legal_statuses_list']);
	$name=htmlspecialchars($_POST['name'],ENT_QUOTES);
	$phone=$_POST['phone'];
	
	//check variables for security
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ header("Location:index.php?page=signup&error=input"); exit(); }
	
	$db->query("SELECT `id` FROM `accounts` WHERE `email`='$email'");
	
	if($db->numRows()>0){ header("Location:index.php?page=signup&error=emailexists"); exit();}
	if($password!=$password2 || strlen($password)==0){ header("Location:index.php?page=signup&error=input"); exit(); }

	if(strlen($password)==0){ header("Location:index.php?page=signup&error=input"); exit(); }
	if(strlen($name)==0){ header("Location:index.php?page=signup&error=input"); exit(); }
	for($i=0;$i<count($phone);$i++)	$phone[$i]=htmlspecialchars($phone[$i],ENT_QUOTES);
	 
	//add items to database
	$db->startTransaction();
	if(!$db->query("
	INSERT INTO `accounts`
	(`id`, `country_id`, `city`, `status_id`, `email`, `password`, `name`)
	VALUES ('0', '$country_id', '$city', '$status_id', '$email', MD5('$password'), '$name')"))
	{
		$db->rollback();
		header("Location:index.php?page=signup&error=db");
		exit();
	}
	$account_id=$db->insertId();
	foreach($phone as $p)
	{
		if(!$db->query("INSERT INTO `contacts` (`id`,`account_id`,`contact_text`) VALUES('0','$account_id','$p')"))
		{
			$db->rollback();
			header("Location:index.php?page=signup&error=db");
		}
	}
	$db->commit();
	
	$_SESSION['user']['id']=$account_id;
	$_SESSION['user']['name']=$name;
	
	header("Location:index.php?page=account");
	
	
}

// Get countries from database
$db->query("SELECT `id`,`country_name_ru` FROM `country_`  ORDER BY IF(id >= 214, 0, 1) ASC, id ASC");
while(list($id,$name)=$db->fetchRow()) $countries_list[$id]=$name;
$smarty->assign("countries_list",$countries_list);

// Get legal statuses from database
$db->query("SELECT `id`,`status_name` FROM `legal_statuses`");
while(list($id,$name)=$db->fetchRow()) $legal_statuses_list[$id]=$name;
$smarty->assign("legal_statuses_list",$legal_statuses_list);

?>
