<?php

if(!isset($_SESSION['user']))
{
	header("Location:index.php");
	exit();
}

if(isset($_GET['action']) && $_GET['action']=="delete" && isset($_GET['id']))
{
	$id=intval($_GET['id']);
	//echo "DELETE FROM `cargo` WHERE `id`='$id'";
	$db->query("DELETE FROM `cargo` WHERE `id`='$id'"); //make sure triggers are installed to delete origins and destinations from separate table
	header("Location:index.php?page=mycargo");
	exit();
}

$db2=new DataBase($db);
$db->query("SELECT `id`,`name` FROM `transport_types`");
while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
$db->query("SELECT `id`,`name` FROM `transport_modes`");
while(list($id,$name)=$db->fetchRow()) $transportss[$id]=$name;

$db->query("SELECT `id`, `account_id`, `load_from`, `load_due`, `cargo_name`, `transport_required`, `transport_modes`, `canconsolidate`, `additional_info`,  `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price` FROM `cargo` WHERE `account_id`='{$_SESSION['user']['id']}' ORDER BY `load_due` DESC");
if($db->numRows()==0)
{
	$smarty->assign("nocargo","true");
}
$cargo_arr=array();
while(list($id, $account_id, $load_from, $load_due, $cargo_name, $transport_type, $transport_modes, $canconsolidate, $info,  $weight, $volume, $vehicles_number, $length, $width, $height, $price)=$db->fetchRow())
{
	$cargo_arr[$id]['load_from']=$load_from;
	$cargo_arr[$id]['load_due']=$load_due;
	$cargo_arr[$id]['cargo_name']=$cargo_name;
	$cargo_arr[$id]['transport_type']=$transports[$transport_type];
	$cargo_arr[$id]['transport_modes']=$transportss[$transport_modes];
	$cargo_arr[$id]['canconsolidate']=($canconsolidate==1)?'on':'off';
	$cargo_arr[$id]['weight']=$weight;
	$cargo_arr[$id]['volume']=$volume;
	$cargo_arr[$id]['vehicles_number']=$vehicles_number;
	$cargo_arr[$id]['length']=$length;
	$cargo_arr[$id]['width']=$width;
	$cargo_arr[$id]['height']=$height;
	$cargo_arr[$id]['info']=$info;
	if($price) $cargo_arr[$id]['price']=number_format($price, 0, ',', " ");
	//if($price) $cargo_arr[$id]['price']=number_format($price, 0, ',', " ");
	$db2->query("SELECT `city_name_ru` FROM `cargo_origins`,`city_` WHERE `cargo_origins`.`cargo_id`='$id' AND `cargo_origins`.`origin`=`city_`.`id` ORDER BY `cargo_origins`.`id`");
	while(list($o)=$db2->fetchRow()) $cargo_arr[$id]['origins'][]=$o;
	$db2->query("SELECT `city_name_ru` FROM `cargo_destinations`,`city_` WHERE `cargo_destinations`.`cargo_id`='$id' AND `cargo_destinations`.`destination`=`city_`.`id` ORDER BY `cargo_destinations`.`id`");
	while(list($d)=$db2->fetchRow()) $cargo_arr[$id]['destinations'][]=$d;
	$db2->query("SELECT `name`,`email`,`contact_text` FROM `accounts`,`contacts` WHERE `accounts`.`id`=`contacts`.`account_id` AND `accounts`.`id`='$account_id'");
	while(list($name,$email,$contact)=$db2->fetchRow())
	{
		$cargo_arr[$id]['contact_name']=$name;
		$cargo_arr[$id]['contact_email']=$email;
		$cargo_arr[$id]['contact_phones'][]=$contact;
	}	
}
/*
echo "<pre>";
print_r($cargo_arr);
echo "</pre>";
*/
$smarty->assign("cargos",$cargo_arr);

?>