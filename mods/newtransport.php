<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

if(!isset($_SESSION['user']))
{
	header("Location:index.php");
	exit();
}

require_once "libs/lib.php";

if(isset($_GET['action']) && $_GET['action']=="do")
{
	/*
	echo "<pre>";
	print_r($_POST);
	echo "</pre>";
	exit();
	*/
	//print_r($_POST); exit();
	if(!($date_from=dateToDB($_POST['date_from'])))
	{
		header("Location:index.php?page=newtransport&error=date");
		exit();
	}
	if(!($date_due=dateToDB($_POST['date_due'])))
	{
		header("Location:index.php?page=newtransport&error=date");
		exit();
	}
	$origin_raw=$_POST['origin'];
	foreach($origin_raw as $o)
	{
		if(intval($o)>0) $origin[]=intval($o);
	}
	if(count($origin)==0)
	{
		header("Location:index.php?page=newtransport&error=noorigin");
		exit();
	}

	$destination_raw=$_POST['destination'];
	foreach($destination_raw as $d)
	{
		if(intval($d)>0) $destination[]=intval($d);
	}
	if(count($destination)==0)
	{
		header("Location:index.php?page=newtransport&error=nodestination");
		exit();
	}

	$transport_type=intval($_POST['transports']);
	$transport_modes=intval($_POST['transports_modes']);
	$car_amount=intval($_POST['car_amount']);
	$weight=floatval($_POST['weight']);
	$volume=floatval($_POST['volume']);
	$length=floatval($_POST['length']);
	$width=floatval($_POST['width']);
	$height=floatval($_POST['height']);
	$price=floatval($_POST['price']);
	$db->startTransaction();
	if(!$db->query("
	INSERT INTO `transport`
	(`id`, `account_id`, `load_from`, `load_due`, `transport_required`, `transport_modes`, `weight`, `volume`, `vehicles_number`, `length`, `width`, `height`, `price`)
	VALUES
	(NULL, '{$_SESSION['user']['id']}', '$date_from', '$date_due', '$transport_type', '$transport_modes', '$weight', '$volume', '$car_amount', '$length', '$width', '$height', '$price')"))
	{
		$db->rollback();
		header("Location:index.php?page=newtransport&error=db");
		exit();
	}

	$brand=htmlspecialchars(trim($_POST['brand']),ENT_QUOTES);
	$capacity=intval($_POST['capacity']);
	$seats=htmlspecialchars(trim($_POST['seats']),ENT_QUOTES);
	$information=htmlspecialchars(trim($_POST['information']),ENT_QUOTES);
	$freight_id=$db->insertId();

	if(!$db->query("
		INSERT INTO `transport_characteristics` 
		(`id`, `brand`, `capacity`, `information`, `seats`, `transport_id`) 
		VALUES 
		(NULL, '$brand', '$capacity', '$information', '$seats','$freight_id')"))
	{
		$db->rollback();
		echo "<h1>".$freight_id."</h1>";
		echo "<h1>".$brand."</h1>";
		echo "<h1>".$capacity."</h1>";
		echo "<h1>".$seats."</h1>";
		echo "<h1>".$information."</h1>";
		//header("Location:index.php?page=newtransport&error=transport_characteristics");
		exit();
	}

	foreach($origin as $o)
	{
		if(!$db->query("INSERT INTO `transport_origins` (`id`,`transport_id`,`origin`) VALUES('0','$freight_id','$o')"))
		{
			$db->rollback();
			header("Location:index.php?page=newtransport&error=db&2");
			exit();
		}
	}
	foreach($destination as $d)
	{
		if(!$db->query("INSERT INTO `transport_destinations` (`id`,`transport_id`,`destination`) VALUES('0','$freight_id','$d')"))
		{
			$db->rollback();
			header("Location:index.php?page=newtransport&error=db&1");
			exit();
		}
	}
	$db->commit();
	header("Location:index.php?page=mytransport");
}

// Get countries from database
$db->query("SELECT `id`, `country_name_ru` FROM `country_` ORDER BY IF(id >= 214, 0, 1) DESC, id>222 ASC");
while(list($id,$name)=$db->fetchRow()) $countries_list[$id]=$name;
$smarty->assign("from_country",$countries_list);

$city_list[-1]="любой";
// Get kazakhstan cities from database. I used DISTINCT coz apparently there are duplicate names for cities in city_ table so far. for instance Baikonur is mentioned 3 times, two of which for Kazakhstan
$db->query("SELECT `id`, `city_name_ru` FROM `city_` WHERE `id_country`='222' GROUP BY `city_name_ru` ORDER BY `city_name_ru`");
while(list($id,$name)=$db->fetchRow()) $city_list[$id]=$name;
$smarty->assign("from_city",$city_list);

$smarty->assign("current_date",date("d.m.Y"));
$smarty->assign("due_date",date("d.m.Y",time()+86400 * 93));

// Get transport types from database
$db->query("SELECT `id`,`name` FROM `transport_modes`");
while(list($id,$name)=$db->fetchRow()) $transport_modes[$id]=$name;
$smarty->assign("transport_modes",$transport_modes);

// Get transport types from database
$db->query("SELECT `id`,`name` FROM `transport_types`");
while(list($id,$name)=$db->fetchRow()) $transports[$id]=$name;
$smarty->assign("transports",$transports);

?>
