<?php

class DataBase
{
    var $host;
    var $user;
    var $name;
    var $pass;

    var $link_id=0;
    var $query_id;
    var $record=array();
    var $row;

    function DataBase($dhost_or_dbobject,$duser=null,$dname=null,$dpass=null)
    {
	if(is_object($dhost_or_dbobject) && $duser== null && $dname==null && $dpass==null)
	{
	    //trying to instanciate from existing object
	    $this->host=$dhost_or_dbobject->host;
	    $this->user=$dhost_or_dbobject->user;
	    $this->name=$dhost_or_dbobject->name;
	    $this->pass=$dhost_or_dbobject->pass;
	}
	else
	{
	    $this->host=$dhost_or_dbobject;
	    $this->user=$duser;
	    $this->name=$dname;
	    $this->pass=$dpass;
	}
    }
    public function setEncoding($enc)
    {
	if(!mysql_set_charset($enc,$this->link_id)) return false;
    }
    function connect()
    {
	if($this->link_id==0)
	    $this->link_id=@mysql_connect($this->host,$this->user,$this->pass);
	if(!$this->link_id) return false;
	$this->setEncoding('utf8');
	$SelectResult=mysql_select_db($this->name, $this->link_id);
	if(!$SelectResult) return false;
	return true;
    }
    function showTableStatus()
    {
	$this->query("SHOW TABLE STATUS");
	$this->record=mysql_fetch_array($this->query_id);
	return $this->record;
    }
    function query($sql)
    {
	$this->connect();
	$this->query_id=mysql_query($sql,$this->link_id);
	$this->row=0;
	if (!$this->query_id) return false;
	return $this->query_id;
    }
    function nextRecord()
    {
	$this->record=mysql_fetch_array($this->query_id);
	$this->row++;
	$stat=is_array($this->record);
	if(!$stat)
	{
	    mysql_free_result($this->query_id);
	    $this->query_id=0;
	}
	return $this->record;
    }
    function gotoRow($r)
    {
	mysql_data_seek($this->query_id,$r);
    }
    function numRows()
    {
	return mysql_num_rows($this->query_id);
    }
    public function insert($table,$values_array)
    {
		$field_names=array_keys($values_array);

	    foreach($field_names as $field)
		{
		    $sql_fields.="`$field`,";
		    if($values_array[$field]!="NOW()") $sql_values.="'{$values_array[$field]}',"; else $sql_values.="NOW()";
		}
		$sql_fields=substr($sql_fields,'',-1); //remove last comma
		$sql_values=substr($sql_values,'',-1);

		$sql="INSERT INTO `$table` ($sql_fields) VALUES($sql_values)";
		return $this->query($sql);
    }

    public function select($table,$fields_array=null,$conditions_array=null,$orderbyfieldname=null,$limit=null)
    {
        if($fields_array!=null)
        {
            foreach($fields_array as $field)
            {
                $sql_fields.="`$field`,";
            }
            $sql_fields=substr($sql_fields,'',-1); //remove last comma
        }
        else
        {
            $sql_fields="*";
        }

        $sql="SELECT $sql_fields FROM `$table`";
        if($conditions_array!=null)
        {
            foreach($conditions_array as $condition)
            {
                $sql_conditions.=" AND $condition ";
            }
            $sql.=" WHERE 1=1 ";
        }
        if($orderbyfieldname!=null) $sql.=" ORDER BY `$orderbyfieldname`";
        if($limit!=null) $sql.=" LIMIT `$limit`";

        return $this->query($sql);
    }

    function update($table,$fields_array,$condition)
    {
        $sql="UPDATE `$table` SET ";
        $field_names=array_keys($fields_array);

        foreach($field_names as $field)
        {
            $sql.="`$field`='{$field_array[$field]},";
        }
        $sql=substr($sql,'',-1);// remove last comma

        $sql.=" WHERE $condition";

        return $this->query($sql);
    }
    function affectedRows()
    {
	return mysql_affected_rows($this->link_id);
    }
    function insertId()
    {
	return mysql_insert_id($this->link_id);
    }
    function fetchRow()
    {
	return mysql_fetch_row($this->query_id);
    }
    function startTransaction()
    {
		$this->query("BEGIN");
    }
    function commit()
    {
		$this->query("COMMIT");
    }
    function rollback()
    {
		$this->query("ROLLBACK");
    }
    function killResults()
    {
	if($this->query_id!=0) mysql_freeresult($this->query_id);
    }
    function kill()
    {
	if($this->link_id!=0) mysql_close($this->link_id);
    }
}

?>
